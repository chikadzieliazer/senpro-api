<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GtPosition extends Model
{
    use HasFactory;
    protected $table = 'tblposition_gt';
    protected $primarKey = 'position_id';
    public function unit()
    {
        return $this->hasOne(Unit::class, 'unit_id', 'unit_id');
    }
    public function fm()
    {
        return $this->hasOne(Asset::class, 'gt', 'unit_i');
    }
}
