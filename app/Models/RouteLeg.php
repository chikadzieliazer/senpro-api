<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class RouteLeg extends Model
{
    use HasFactory;
    protected $table = 'tbl_routeleg';
    protected $primarykey = 'legid';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'groupid',
        'leg',
        'code',
        'distance',
        'duration_empty',
        'duration_loaded',
        'created'       
    ];

    public function fmGroup(): BelongsTo
    {
        return $this->belongsTo(FmGroup::class, 'groupid');
    }
}
