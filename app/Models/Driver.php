<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Driver extends Model
{
    use HasFactory;

    protected $table = 'tbldriver';
    protected $primarKey = 'driverid';
    
    public function fmGroup(): BelongsTo
    {
        return $this->belongsTo(FmGroup::class, 'groupid');
    }

    public function trip(): BelongsTo
    {
        return $this->belongsTo(Trip::class, 'driverid', 'driverid');
    }
    public function position()
    {
        return $this->hasMany(FmPosition::class, 'driverid', 'driverid');
    }
}
