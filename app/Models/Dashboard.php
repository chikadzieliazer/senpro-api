<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dashboard extends Model
{
    use HasFactory;
    protected $table = 'tbltripsheet';
    protected $primarKey = 'tripsheetid';

    public function asset()
    {
        return $this->hasOne(Asset::class, 'assetid', 'assetid');
    }
}
