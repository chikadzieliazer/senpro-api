<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GtGroup extends Model
{
    use HasFactory;
    protected $table = 'tblclient_gt';
    protected $primaryKey = 'client_id';
    
}
