<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tollgate extends Model
{
    use HasFactory;
    protected $table = 'tblgrouplocation';
}
