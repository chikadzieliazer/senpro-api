<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class   Trip extends Model
{
    use HasFactory;
    protected $table = 'tbltrip';
    protected $primarKey = 'tripid';

    public function asset()
    {
        return $this->belongsTo(Asset::class, 'assetid', 'assetid');
    }
    public function group()
    {
        return $this->belongsTo(FmGroup::class, 'groupid', 'groupid');
    }
    public function driver()
    {
        return $this->belongsTo(Driver::class, 'driverid', 'driverid');
    }
}