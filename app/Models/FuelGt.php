<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FuelGt extends Model
{
    use HasFactory;
    protected $table = 'tblfuel_gt';
}
