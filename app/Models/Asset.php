<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    use HasFactory;
    protected $table = 'tblasset';
    protected $primarykey = 'assetid';

    public function tripsheet()
    {
        return $this->hasMany(Dashboard::class, 'assetid', 'assetid');
    }
    public function trip()
    {
        return $this->hasMany(Trip::class, 'assetid', 'assetid');
    }
    public function position()
    {
        return $this->hasMany(FmPosition::class, 'assetid', 'assetid');
    }
}