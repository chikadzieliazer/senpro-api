<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CurrentPositionGt extends Model
{
    use HasFactory;
    protected $table = 'tblgtposition_curr';
    protected $primarykey = 'positionid';
}
