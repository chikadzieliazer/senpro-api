<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class FmGroup extends Model
{
    use HasFactory;

    protected $table = 'tblgroup';
    protected $primaryKey = 'groupid';

    public function assets(): HasMany
    {
        return $this->hasMany(Asset::class, 'groupid');
    }
    public function customers(): HasMany
    {
        return $this->hasMany(Customer::class, 'groupid');
    }
    public function trailers()
    {
        return $this->hasMany(Trailer::class, 'groupid');
    }
    public function routeLegs()
    {
        return $this->hasMany(RouteLeg::class, 'groupid');
    }
    public function sites()
    {
        return $this->hasMany(Depot::class, 'groupid');
    }
    public function transits()
    {
        return $this->hasMany(TransitTarget::class, 'groupid');
    }
}
