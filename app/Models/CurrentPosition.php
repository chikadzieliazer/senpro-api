<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CurrentPosition extends Model
{
    use HasFactory;
    protected $table = 'tblassetposition_curr';
    protected $primarykey = 'positionid';
}

