<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Product extends Model
{
    use HasFactory;
    protected $table = 'tbl_product';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'groupid',
        'name',
        'description',
        'density',
        'units',
        'type',
        'created'       
    ];

    public function fmGroup()
    {
        return $this->belongsTo(FmGroup::class, 'groupid');
    }
}
