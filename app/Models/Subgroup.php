<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subgroup extends Model
{
    use HasFactory;
    protected $table = 'tblsubgroup';
    protected $primaryKey = 'subgroupid';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'groupid',
        'parentid',
        'name',
        'type'      
    ];

    public function fmGroups()
    {
        return $this->belongsTo(FmGroup::class, 'groupid');
    }
}
