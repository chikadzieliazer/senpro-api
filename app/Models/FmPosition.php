<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FmPosition extends Model
{
    use HasFactory;
    protected $table = 'tblposition';
    protected $primarKey = 'positionid';

    public function asset()
    {
        return $this->belongsTo(Asset::class, 'assetid', 'assetid');
    }

    public function group()
    {
        return $this->belongsTo(FmGroup::class, 'groupid', 'groupid');
    }
    
    public function driver()
    {
        return $this->belongsTo(Driver::class, 'driverid', 'driverid');
    }
}
