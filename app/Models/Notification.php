<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;
    protected $table = 'tbl_notifications';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'message',
        'notification_date',
        'is_read',
    ];
}
