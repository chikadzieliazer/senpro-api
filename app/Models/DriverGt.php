<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DriverGt extends Model
{
    use HasFactory;
    protected $table = 'tbldriver_gt';
    protected $primarykey = 'driverid';

    public function gtGroup(): BelongsTo
    {
        return $this->belongsTo(GtGroup::class, 'client_id');
    }
    
}
