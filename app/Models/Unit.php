<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use HasFactory;
    protected $table = 'tblunit_gt';
    protected $primarKey = 'unit_id';

    public function positions()
    {
        return $this->hasMany(GtPosition::class, 'unit_id', 'unit_id');
    }
}
