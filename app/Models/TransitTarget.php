<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TransitTarget extends Model
{
    use HasFactory;
    protected $table = 'tbl_transittarget';
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'groupid',  
        'position_a',
        'position_b',
        'distance',
        'duration',
        'created'       
    ];

    public function fmGroup(): BelongsTo
    {
        return $this->belongsTo(FmGroup::class, 'groupid');
    }
}
