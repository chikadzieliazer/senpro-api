<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tripsheet extends Model
{
    use HasFactory;
    protected $table = 'tbltripsheet';
    protected $primarykey = 'tripsheetid';

    public function asset()
    {
        return $this->belongsTo(Asset::class, 'assetid', 'assetid');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'userid');
    }
}