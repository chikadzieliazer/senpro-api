<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    protected $table = 'tbl_customer';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'groupid',
        'name',
        'description',
        'created'       
    ];

    public function fmGroup()
    {
        return $this->belongsTo(FmGroup::class, 'groupid');
    }
}
