<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Trailer extends Model
{
    use HasFactory;
    protected $table = 'tbl_trailer';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'old_id',
        'groupid',
        'name',
        'type',
        'status',
        'created',
        'description',
        'mileage',
        'make',
        'model',
        'registration'        
    ];

    public function fmGroup(): BelongsTo
    {
        return $this->belongsTo(FmGroup::class, 'groupid');
    }
}
