<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

     protected $table = 'tbl_user';
     public $timestamps = false;

    protected $fillable = [
        'name',
        'username',
        'password',
        'type',
        'email',
        'created',
        'notifylist',
        'lastlogin',
        'status',
        'fm',
        'gt',
        'showload',
        'notification',
        'power_bi_url',
        'department'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
    ];

    public function activity()
    {
        return $this->hasOne(Activity::class, 'userid', 'id')->latestOfMany();
    }
    public function gtgroup()
    {
        return $this->belongsTo(GtGroup::class, 'gt', 'client_id')->select(['client_id','client_account']);
    }
    public function fmgroup()
    {
        return $this->belongsTo(FmGroup::class, 'fm', 'groupid')->select(['groupid','name']);
    }
    public function tripsheets()
    {
        return $this->hasMany(Tripsheet::class, 'userid', 'id');
    }
}
