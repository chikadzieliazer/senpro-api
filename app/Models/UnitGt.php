<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UnitGt extends Model
{
    use HasFactory;
    protected $table = 'tblunit_gt';
    protected $primaryKey = 'unit_id';
    
    public function gtGroup()
    {
        return $this->belongsTo(GtGroup::class, 'client_id');
    }
}
