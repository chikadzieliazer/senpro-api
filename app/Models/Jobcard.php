<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jobcard extends Model
{
    use HasFactory;
    protected $table = 'tbl_jobcard';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'userid',
        'groupid',
        'assetid',
        'technicianid',
        'unittype',
        'serial',
        'fault',
        'work',
        'comments',
        'created',
        'jobcard',
        'travel',
        'labour',
        'parts',
        'ordernum'
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'userid', 'id');
    }

    public function fmGroups()
    {
        return $this->belongsTo(FmGroup::class, 'groupid');
    }

    public function assets()
    {
        return $this->belongsTo(Asset::class, 'assetid', 'assetid');
    }
}
