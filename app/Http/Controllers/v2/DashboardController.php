<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\GtPosition;
use App\Models\RouteLeg;
use App\Models\Trip;
use App\Models\Tripsheet;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    private function query($id, $start, $end, $site, $useDate){
        return Tripsheet::where('status', ['ACTIVE', 'OPEN'])
                ->when($useDate == 'close', function ($query)  use ($start, $end){
                    $query->whereBetween('end', [$start, $end]);
                })
                ->when($useDate == 'create', function ($query)  use ($start, $end){
                    $query->whereBetween('created', [$start, $end]);
                })
                ->when($id != 0, function ($query)  use ($id){
                        $query->where('groupid', $id);
                })
                ->when($site != 0, function ($query)  use ($site){
                        $query->whereHas('asset', function ($q) use ($site) {
                        $q->where('siteid', $site);
                    });
                })
                ->select('tripsheet', 'assetid', 'groupid', 'tripsheetid', 'distance', 'scores', 'fuel', 'targets', 'trip', 'driver', 'products', 'allowances')
                ->distinct('tripsheet')
                ->orderBy('tripsheet')
                ->orderBy('tripsheetid', 'desc');
    }

    public function index(Request $request, $id){
        $start    = $request->input('start', Carbon::now()->startOfMonth());
        $end      = $request->input('end', Carbon::now());
        $site     = $request->has('site') ? $request->input('site') : 0;
        $useDate  = $request->has('usedate') ? $request->input('usedate') : 'close';

        $carbonStart = new Carbon($start);
        $carbonEnd   = new Carbon($end);
        $diff        = $carbonStart->diffInMonths($carbonEnd) != 0 ? $carbonStart->diffInMonths($carbonEnd) : 1;
        $prevStart   = $carbonStart->subMonths($diff);
        $prevEnd     = $carbonEnd->subMonths($diff);

        $totalDist = $totalFuel = $count = $totalShuntDist = $totalEmptyDist = $totalLoadDist = $fmScore = $fuelScore = $customScore = $transitScore = $sendemScore = $prevTotalDist = $prevTotalFuel = $totalFuelVar = $prevFuelVarTotal = $prevTotalProbeFuel = $totalProbeFuel = $totalProductTonnage = $prevTotalProductTonnage = $totalFmFuel = $prevTotalFmFuel = $totalActualDist = $prevTotalActualDist = 0;
        $topDistArr = array();

        $ts     =  $this->query($id, $start, $end, $site, $useDate)->get();
        $prevTs =  $this->query($id, $prevStart, $prevEnd, $site, $useDate)->get();

        
        
        if($ts->isEmpty()){
            $data = [
                'bottom_consumption' => 0,
                'top_consumption' =>  0,
                'top_distance' => 0,
                'bottom_transits' => 0 ,
                'empty_distance' => 0,
                'shunt_distance' => 0,
                'load_distance' => 0,
                'FM Score' =>  0,
                'Custom Score' => 0,
                'Sendem Score' => 0,
                'Fuel Score' => 0,
                'Transit Score' => 0,
                'tripsheet_closed' => [
                    'current_period' => 0,
                    'last_period' => 0
                ],
                'tripsheet_distance' => [
                    'current_period' => 0,
                    'last_period' => 0
                ],
                'fuel_consumption' => [
                    'current_period' => 0,
                    'last_period' => 0
                ],
                'fuel_variance' => [
                    'current_period' => 0,
                    'last_period' => 0
                ],
                'product_tonnage' => [
                    'current_period' => 0,
                    'last_period' => 0
                ],
            ];
    
            return response()->json($data);
        }
        
        foreach ($ts as $item) {
            $totalDist += isset(json_decode(stripslashes($item['distance']))->fm_edit) && json_decode(stripslashes($item['distance']))->fm_edit > 0 ? json_decode(stripslashes($item['distance']))->fm_edit : (isset(json_decode(stripslashes($item['distance']))->fm) ? json_decode(stripslashes($item['distance']))->fm : 0);
            $totalActualDist += isset(json_decode(stripslashes($item['distance']))->actual) ? json_decode(stripslashes($item['distance']))->actual : 0;
            $totalFuel += isset(json_decode(stripslashes($item['fuel']))->actual) ? (int)json_decode(stripslashes($item['fuel']))->actual : 0;
            $totalFmFuel += isset(json_decode(stripslashes($item['fuel']))->fm) ? (int)json_decode(stripslashes($item['fuel']))->fm : 0;
            $fmScore += isset(json_decode(stripslashes($item['scores']))->overall) ?  json_decode(stripslashes($item['scores']))->overall : 0;
            $fuelScore += isset(json_decode(stripslashes($item['scores']))->fuel) ?  json_decode(stripslashes($item['scores']))->fuel : 0;
            $customScore += isset(json_decode(stripslashes($item['scores']))->advanced) ?  json_decode(stripslashes($item['scores']))->advanced : 0;
            $transitScore += isset(json_decode(stripslashes($item['scores']))->transits) ?  json_decode(stripslashes($item['scores']))->transits : 0;
            $sendemScore += isset(json_decode(stripslashes($item['scores']))->sendem) ?  json_decode(stripslashes($item['scores']))->sendem : 0;
            $totalProbeFuel += isset(json_decode(stripslashes($item['fuel']))->probe) ? (int)json_decode(stripslashes($item['fuel']))->probe : 0;
            $probeFuel = isset(json_decode(stripslashes($item['fuel']))->probe) ? (int)json_decode(stripslashes($item['fuel']))->probe : 0;
            $count += isset(json_decode(stripslashes($item['scores']))->overall) ? 1 : 0;
            $targetDist = isset(json_decode(stripslashes($item['targets']))->distance) ? json_decode(stripslashes($item['targets']))->distance : 0;
            $targetFuel = isset(json_decode(stripslashes($item['targets']))->fuel) ? json_decode(stripslashes($item['targets']))->fuel : 0;
            $targetCons = isset(json_decode(stripslashes($item['targets']))->consumption) ? json_decode(stripslashes($item['targets']))->consumption : 0;
            $dynamicDist =  isset(json_decode(stripslashes($item['distance']))->fm_edit) && json_decode(stripslashes($item['distance']))->fm_edit > 0 ? json_decode(stripslashes($item['distance']))->fm_edit : (isset(json_decode(stripslashes($item['distance']))->fm) ? json_decode(stripslashes($item['distance']))->fm : 0);
            $shunt  = $item['groupid'] == -5382047893814814814 || $item['groupid'] == -5511162494231442644 ? json_decode(stripslashes($item['distance']))->shunt :($dynamicDist>json_decode(stripslashes($item['targets']))->distance? $dynamicDist-json_decode(stripslashes($item['targets']))->distance : 0);
            $trip = json_decode(stripslashes($item['trip']))->long;
            $actualFuel = isset(json_decode(stripslashes($item['fuel']))->actual) ? (int)json_decode(stripslashes($item['fuel']))->actual : 0;
            $fmFuel = isset(json_decode(stripslashes($item['fuel']))->fm) ? (int)json_decode(stripslashes($item['fuel']))->fm : 0;
            $volvoFuel = isset(json_decode(stripslashes($item['fuel']))->volvo_connect) ? (int)json_decode(stripslashes($item['fuel']))->volvo_connect : 0;
            $transits = isset(json_decode(stripslashes($item['scores']))->transits) ? json_decode(stripslashes($item['scores']))->transits : 0;
            $productTonnage = isset(json_decode(stripslashes($item['products']))->loads) ? json_decode(stripslashes($item['products']))->loads : 0;
            $allowances = json_decode(stripslashes($item['allowances']), true);
            $trailers = isset(json_decode(stripslashes($item['products']))->trailers) ? json_decode(stripslashes($item['products']))->trailers : 0;
            $asset = Asset::where('assetid', $item['assetid'])
                ->select('emptyconsumption', 'targetfuelconsumption', 'loadconsumption')
                ->orderBy('groupid')
                ->orderBy('description')
                ->get();

            $consE =  $asset[0]['emptyconsumption'];
            $consL =  $asset[0]['loadconsumption'];

            $shuntFuelE = $consE>0 ? $shunt/(2 * $consE) : 0;
            $shuntFuelL = $consL>0 ? $shunt/(2 * $consL) : 0;

            if($item['groupid'] == -5511162494231442644){
                $totShuntFuel     = $shunt/0.5;
            }else if(!$asset[0]['emptyconsumption'] && !$asset[0]['emptyconsumption']){
                $totShuntFuel     = $asset[0]['targetfuelconsumption'] ? $shunt / $asset[0]['targetfuelconsumption'] : 0;
            } else {
                $totShuntFuel     = $shuntFuelE + $shuntFuelL;
            }
            // Calculate Product Tonnage
            foreach ($productTonnage as $key => $value) {
                $totalProductTonnage += $value->quantity;
            }

            //Calculate fuel targets    
            if ($item['groupid'] == -3273034392599683215) {
                $fuelTarget  = $targetCons > 0 ? $dynamicDist/$targetCons : 0;
            } else if($item['groupid'] == -5511162494231442644){
                $fuelTarget = $targetFuel + ($shunt/0.5);
            } else if($item['groupid'] == -202715777619230739 && $volvoFuel > 0){
                $fuelTarget = $volvoFuel;
            }
            else if($item['groupid'] == -202715777619230739){
                $fuelTarget = $targetFuel + $totShuntFuel;
            }
            else{
                if($totShuntFuel == 0){
                    $shuntFuelE = $shunt/2;
                } 
                $fuelTarget = $targetFuel + (($id !== 1853956352811964940 || $id !== -3273034392599683215)  ? ($shuntFuelE + $shuntFuelL) : 0);
            }

        
             // Calculate allowance
             if (isset($allowances['allowances']) && count($allowances['allowances'])>0) {
                foreach ($allowances['allowances'] as $a) {
                    if (str_contains($a['name'], "FUEL-ALLOWANCE")) {
                        $fuelAllowance = is_numeric($a['rate']) ? $a['rate']*$a['days'] : $a['days'];
                        $fuelTarget = $fuelTarget + $fuelAllowance;
                    }
                }
            }
            
            // Calculate Variance
            if($item['groupid'] == 1853956352811964940){
                $totalFuelVar += $fmFuel - $actualFuel;
           } elseif($item['groupid'] == -202715777619230739){
                $totalFuelVar += $volvoFuel > 0 ? $volvoFuel- $actualFuel : $fuelTarget - $actualFuel;
                $a = $volvoFuel > 0 ? $volvoFuel- $actualFuel : $fuelTarget - $actualFuel;
           }
           elseif($item['groupid'] == 7547074992352639280){
               $totalFuelVar += $fuelTarget - $probeFuel;
           }else {
               $totalFuelVar += $fuelTarget - $actualFuel;
           } 
            
            // Calculate ShuntDistance
            if($dynamicDist > $targetDist){
                $shunt = $dynamicDist - $targetDist;
                $totalShuntDist += $shunt;
            }

            // Calculate Load Distance
            if($trip != 'LOCAL(L)'){
                $shunt = $dynamicDist > $targetDist ? $dynamicDist - $targetDist : 0;
                $loadPc = $this->calc_load_percent($id, $trip);
                $totalLoadDist += $targetDist*($loadPc/100);
                $totalEmptyDist += $targetDist*(100-$loadPc)/100;
            }    
            
            //Top Consumption
            $consumption = $actualFuel != 0 ? ($item['groupid'] == 7547074992352639280 && $probeFuel != 0 ? number_format($dynamicDist / $probeFuel, 2) : number_format($dynamicDist / $actualFuel, 2)) : 0;
            $driverConsArr[$item->driver] = $consumption;
            $transitsArr[ $item->driver] = number_format($transits,2);
            $topDistArr[$item->driver] = array_key_exists($item['driver'],$topDistArr) ? (int)$topDistArr[$item->driver]+$dynamicDist : $dynamicDist;
        }
       
        foreach ($prevTs as $item) {          
            $prevTotalDist += isset(json_decode(stripslashes($item['distance']))->fm_edit) && json_decode(stripslashes($item['distance']))->fm_edit > 0 ? json_decode(stripslashes($item['distance']))->fm_edit : (isset(json_decode(stripslashes($item['distance']))->fm) ? json_decode(stripslashes($item['distance']))->fm : 0);
            $prevTotalActualDist += isset(json_decode(stripslashes($item['distance']))->actual) ? json_decode(stripslashes($item['distance']))->actual : 0;
            $prevTotalFuel += isset(json_decode(stripslashes($item['fuel']))->actual) ? (float)json_decode(stripslashes($item['fuel']))->actual : 0;
            $prevTotalProbeFuel += isset(json_decode(stripslashes($item['fuel']))->probe) ? (int)json_decode(stripslashes($item['fuel']))->probe : 0;
            $prevTotalFmFuel += isset(json_decode(stripslashes($item['fuel']))->fm) ? (int)json_decode(stripslashes($item['fuel']))->fm : 0;
            $prevProbeFuel = isset(json_decode(stripslashes($item['fuel']))->probe) ? (int)json_decode(stripslashes($item['fuel']))->probe : 0;
            $dynamicDist = isset(json_decode(stripslashes($item['distance']))->fm_edit) && json_decode(stripslashes($item['distance']))->fm_edit > 0 ? json_decode(stripslashes($item['distance']))->fm_edit : (isset(json_decode(stripslashes($item['distance']))->fm) ? json_decode(stripslashes($item['distance']))->fm : 0);
            $shunt = $dynamicDist>json_decode(stripslashes($item['targets']))->distance? $dynamicDist-json_decode(stripslashes($item['targets']))->distance : 0;
            $targetDist = isset(json_decode(stripslashes($item['targets']))->distance) ? json_decode(stripslashes($item['targets']))->distance : 0;
            $targetFuel = isset(json_decode(stripslashes($item['targets']))->fuel) ? json_decode(stripslashes($item['targets']))->fuel : 0;
            $targetCons = isset(json_decode(stripslashes($item['targets']))->consumption) ? json_decode(stripslashes($item['targets']))->consumption : 0;
            $actualFuel = isset(json_decode(stripslashes($item['fuel']))->actual) ? (int)json_decode(stripslashes($item['fuel']))->actual : 0;
            $fmFuel = isset(json_decode(stripslashes($item['fuel']))->fm) ? (int)json_decode(stripslashes($item['fuel']))->fm : 0;
            $volvoFuel = isset(json_decode(stripslashes($item['fuel']))->volvo_connect) ? (int)json_decode(stripslashes($item['fuel']))->volvo_connect : 0;
            $prevProductTonnage = isset(json_decode(stripslashes($item['products']))->loads) ? json_decode(stripslashes($item['products']))->loads : 0;
            $allowances = json_decode(stripslashes($item['allowances']), true);
            $trailers = isset(json_decode(stripslashes($item['products']))->trailers) ? json_decode(stripslashes($item['products']))->trailers : 0;
            
            // Calculate Product Tonnage
            foreach ($prevProductTonnage as $key => $value) {
                $prevTotalProductTonnage += $value->quantity;
            }

            // Calculate fuel targets
            if ($item['groupid']  == -3273034392599683215) {
                $fuelTarget = $targetCons > 0? $dynamicDist/$targetCons : 0;
            } else if($item['groupid'] == -5511162494231442644){
                $fuelTarget = $targetFuel + (($id !== 1853956352811964940 || $id !== -3273034392599683215)  ? ($shunt/0.5) : 0);
            }else if($item['groupid'] == -202715777619230739 && $volvoFuel > 0){
                $fuelTarget = $volvoFuel;
            }
            else{
                $fuelTarget = $targetFuel + (($id !== 1853956352811964940 || $id !== -3273034392599683215)  ? ($shunt*0.5) : 0);
            }

            // Calculate allowance
            if (isset($allowances['allowances']) && count($allowances['allowances'])>0) {
                foreach ($allowances['allowances'] as $a) {
                    if (str_contains($a['name'], "FUEL-ALLOWANCE")) {
                        $fuelAllowance = is_numeric($a['rate']) ? $a['rate']*$a['days'] : $a['days'];
                        $fuelTarget = $fuelTarget + $fuelAllowance;
                    }
                }
            }
            // Calculate Variance
            if($item['groupid'] == 1853956352811964940){
                $prevFuelVarTotal += $fmFuel - $actualFuel;
           } elseif($item['groupid'] == -202715777619230739){
                $prevFuelVarTotal += $volvoFuel > 0 ? $volvoFuel- $actualFuel : $fuelTarget - $actualFuel;
           }
           elseif($item['groupid'] == 7547074992352639280){
               $prevFuelVarTotal += $fuelTarget - $probeFuel;
           }else {
               $prevFuelVarTotal += $fuelTarget - $actualFuel;
           } 
        }

        // Closed tripsheets
        $closedTs = count($ts);
        $closedTsDiff = count($prevTs) == 0 ? 0 : ((count($ts) - count($prevTs))/count($prevTs)) * 100;  

        // Fuel Consumption
        if($id == 7547074992352639280){
            $cons =  $totalFuel !== 0 ?  $totalDist / $totalProbeFuel : 0;
        } else {
            $cons =  $totalFuel !== 0 ?  $totalActualDist / $totalFuel : 0;
        }

        if($id == 7547074992352639280){
            $prevCons = $prevTotalFuel !== 0 ?  $prevTotalDist / $prevTotalProbeFuel : 0;
        } else {
            $prevCons = $prevTotalFuel !== 0 ?  $prevTotalActualDist / $prevTotalFuel : 0;
        }

        $consDiff = !empty($prevCons) ? (($cons - $prevCons)/$prevCons) * 100 : 0;

        //Fuel Variance
        $fuelVarDiff = !empty($prevFuelVarTotal) ?  (($totalFuelVar - $prevFuelVarTotal)/($prevFuelVarTotal < 0 ? $prevFuelVarTotal*-1 : $prevFuelVarTotal* 1)) * 100 : 0;

        // Tripsheet Distance
        $totalDistDiff = $prevTotalDist == 0 ? 0 : (($totalDist - $prevTotalDist)/$prevTotalDist) * 100;  

        //Sort Drivers Consumption 
        arsort($driverConsArr);

        //Sort Transits Drivers
        asort($transitsArr);

        // Top Distance Driver
        arsort($topDistArr);
        $topDist = array_slice($topDistArr, 0, 5);
        foreach ($topDist as $key => $value) {
            $topDist[$key] = number_format($value, 2);
        }
        
        //Product Tonnage 
        $productTonnageDiff = $prevTotalProductTonnage == 0 ? 0 : (($totalProductTonnage - $prevTotalProductTonnage)/$prevTotalProductTonnage) * 100;  


        $data = [
            'bottom_consumption' => array_reverse(array_slice($driverConsArr, -5, 5), true),
            'top_consumption' =>  array_slice($driverConsArr, 0, 5),
            'top_distance' => $topDist,
            'bottom_transits' => $id!=5432009351795875573 ? array_slice($transitsArr, 0, 5) : 0,
            'empty_distance' => number_format($totalEmptyDist, 2),
            'shunt_distance' => number_format($totalShuntDist, 2),
            'load_distance' => number_format($totalLoadDist, 2),
            'FM Score' =>  number_format($fmScore / $count, 2),
            'Custom Score' => number_format($customScore / $count, 2),
            'Sendem Score' => number_format($sendemScore / $count, 2),
            'Fuel Score' => number_format($fuelScore / $count, 2),
            'Transit Score' => $id!=5432009351795875573 ? number_format($transitScore / $count, 2) : 0,
            'tripsheet_closed' => [
                'current_period' => $closedTs,
                'last_period' => number_format($closedTsDiff, 2)
            ],
            'tripsheet_distance' => [
                'current_period' => number_format($totalDist, 2),
                'last_period' => number_format($totalDistDiff, 2)
            ],
            'fuel_consumption' => [
                'current_period' => number_format($cons, 2),
                'last_period' => number_format($consDiff, 2)
            ],
            'fuel_variance' => [
                'current_period' => number_format($totalFuelVar, 2),
                'last_period' => number_format($fuelVarDiff, 2)
            ],
            'product_tonnage' => [
                'current_period' => number_format($totalProductTonnage/1000, 2),
                'last_period' => number_format($productTonnageDiff, 2)
            ],
        ];

        return response()->json($data);
       
    }

    public function fmDistance(Request $request, $id){        
        $start = $request->input('start', Carbon::now()->startOfMonth());
        $end   = $request->input('end', Carbon::now());
        $site  = $request->has('site') ? $request->input('site') : 0;

        $carbonStart = new Carbon($start);
        $carbonEnd   = new Carbon($end);
        $diff        = $carbonStart->diffInMonths($carbonEnd) != 0 ? $carbonStart->diffInMonths($carbonEnd) : 1;
        $prevStart   = $carbonStart->subMonths($diff);
        $prevEnd     = $carbonEnd->subMonths($diff);

        $ts =   Trip::when($id != 0, function ($query)  use ($id){
                $query->where('groupid', $id);
            })
            ->whereHas('asset', function ($q)  {
                $q->where('assettypeid', '!=', 2);
            })
            ->when($site != 0, function ($query)  use ($site){
                        $query->whereHas('asset', function ($q) use ($site) {
                        $q->where('siteid', $site);
                    });
                })
            ->whereBetween('tripstart', [$start, $end])
            ->sum('distancekilometers');
            
        $prevTs = Trip::when($id != 0, function ($query)  use ($id){
                $query->where('groupid', $id);
            })
            
            ->when($site != 0, function ($query)  use ($site){
                        $query->whereHas('asset', function ($q) use ($site) {
                        $q->where('siteid', $site);
                    });
                })
            ->whereBetween('tripstart', [$prevStart, $prevEnd])
            ->sum('distancekilometers');

        $percentage = $ts == 0 || $prevTs == 0 ? 0 : (($ts - $prevTs)/$prevTs) * 100;
        $data = [
            'live_distance' => number_format($ts,2),
            'difference' => number_format($percentage,1)
        ];
        return response()->json($data);
    }

    public function gtDistance(Request $request,$id){
        $total = 0;
        $prevTotal = 0;

        $start = $request->input('start', Carbon::now()->startOfMonth());
        $end   = $request->input('end', Carbon::now());
        $site = $request->input('site');

        $carbonStart = new Carbon($start);
        $carbonEnd   = new Carbon($end);
        $diff        = $carbonStart->diffInMonths($carbonEnd) != 0 ? $carbonStart->diffInMonths($carbonEnd) : 1;
        $prevStart   = $carbonStart->subMonths($diff);
        $prevEnd     = $carbonEnd->subMonths($diff);


        $ts = GtPosition::when($id == 1926, function ($query){
                    $query->join('tblunit_gt', 'tblposition_gt.unit_id', '=', 'tblunit_gt.unit_id')
                          ->join('tblasset', 'tblunit_gt.asset_id', '=', 'tblasset.gt');
                })
                ->whereBetween('tblposition_gt.datetime', [$start, $end])
                ->where('tblposition_gt.client_id', $id)
                ->when($site != 0, function ($query)  use ($site){
                    $query->where('tblasset.siteid', $site);
                })
                ->groupBy('tblposition_gt.unit_name')
                ->select('tblposition_gt.unit_name', DB::raw('max(tblposition_gt.odometer) - min(tblposition_gt.odometer) AS distance'))
                ->orderBy('tblposition_gt.unit_name', 'ASC')
                ->get();

        $prevTs = GtPosition::when($id == 1926, function ($query){
                    $query->join('tblunit_gt', 'tblposition_gt.unit_id', '=', 'tblunit_gt.unit_id')
                        ->join('tblasset', 'tblunit_gt.asset_id', '=', 'tblasset.gt');
                })
                ->whereBetween('tblposition_gt.datetime', [$prevStart, $prevEnd])
                ->where('tblposition_gt.client_id', $id)
                ->when($site != 0, function ($query)  use ($site){
                    $query->where('tblasset.siteid', $site);
                })
                ->groupBy('tblposition_gt.unit_name')
                ->select('tblposition_gt.unit_name', DB::raw('max(tblposition_gt.odometer) - min(tblposition_gt.odometer) AS distance'))
                ->orderBy('tblposition_gt.unit_name', 'ASC')
                ->get();

        foreach ($ts as $item) {
            if($item['distance'] <= 20000000){
                $total += $item['distance'];
            }
         }
        foreach ($prevTs as $item) {
             if($item['distance'] <= 20000000){
                $prevTotal += $item['distance'];
            }
         }
         
        $percentage = $prevTotal == 0 ? 0 : (($total - $prevTotal)/$prevTotal) * 100;
        $data = [
            'live_distance' => number_format($total/1000,2),
            'difference' => number_format($percentage,1)
        ];
        return response()->json($data);
    }

    public function fleetUtilisation(Request $request, $id){ 
        $start = $request->input('start', Carbon::now()->startOfMonth());
        $end   = $request->input('end', Carbon::now());
        $minDistance    = Carbon::parse($start)->diffInDays(Carbon::parse($end)) * 10;
        $utilisedVehicles     = 0;

        $fleetTotal = Asset::where('groupid', $id)
            ->where('userstate', 'Available')
            ->count();     
        
        $distCovered = Trip::where('groupid', $id)
            ->whereBetween('tripend', [$start, $end])
            ->select('assetid')
            ->selectRaw('SUM(distancekilometers) as total_distance')
            ->groupBy('assetid')
            ->get();

        $distCovered = json_decode($distCovered, true);
        foreach ($distCovered as  $d) {
            if((float)($d['total_distance']) > $minDistance ){
                $utilisedVehicles++;
            }
        }

        $data = [
            'fleet_total' => $fleetTotal,
            'utilised_vehicles' => $utilisedVehicles,
            'underutilised_vehicles' => $fleetTotal-$utilisedVehicles,
        ];

        return response()->json($data);
    }

    public function calc_load_percent($id, $trip){

        $tripArr = explode("-", str_replace("(L)","",str_replace("(E)","",$trip)));
        $arr = [];
        if ($tripArr) {
            $tmp = "";
            foreach($tripArr AS $t){
                if ($tmp == "") {
                    $tmp = rtrim(ltrim($t));
                }
                else{
                    $tripStr = $tmp."-".$t;       
                    array_push($arr, $tripStr);
                    $tmp = rtrim(ltrim($t));
                }
            }
        }
        if (count($arr) > 0) {
            $totDist = 0;
            $totLoad = 0;
            if($id != 0){
                $rows = RouteLeg::where('groupid', $id)
                    ->whereIn('code', $arr)
                    ->get();
            } else {
                $rows = RouteLeg::whereIn('code', $arr)
                    ->get();
            }
            
            $tripArr = explode("-", $trip);
            $tmp 	 = "";
            foreach($tripArr AS $t){
                if ($tmp == "") {
                    $tmp = rtrim(ltrim($t));
                }
                else{
                    $leg = $tmp."-".$t;
                    $tmp = rtrim(ltrim(str_replace("(L)", "", str_replace("(E)", "",$t))));

                    foreach($rows AS $r){
                        if($leg ==$r['code']."(L)") {
                            $totDist += $r['distance'];
                            $totLoad += $r['distance'];
                            continue;
                        }
                        elseif($leg == $r['code']."(E)") {
                            $totDist += $r['distance'];
                            continue;
                        }
                    }
                    
                }
            }
            return $totDist>0 ? (100*$totLoad/$totDist) : 0;
        }

        return "0";
    }
}
