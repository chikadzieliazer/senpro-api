<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\DepotRequest;
use App\Http\Resources\v1\DepotResource;
use App\Models\Depot;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DepotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $depot = Depot::paginate();
        return DepotResource::collection($depot);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepotRequest $request)
    {
        $depot = new Depot;
        $depot->groupid = $request->groupid;
        $depot->name = $request->name;
        $depot->type = $request->type;
        $depot->city = $request->city;
        $depot->lat = $request->lat;
        $depot->lng = $request->lng;
        $depot->created = Carbon::now()->toDateTimeString();
        $depot->save();
        return (new DepotResource($depot))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Depot $depot)
    {
        return new DepotResource($depot);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $depot =  Depot::findOrFail($id);
        $depot->update($request->all());
        return (new DepotResource($depot))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $depot =  Depot::findOrFail($id);
        $depot->delete();
        return (new DepotResource($depot))
            ->response()
            ->setStatusCode(204);
    }
}
