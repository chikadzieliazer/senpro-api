<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\TollgateResource;
use App\Models\InterestLocation;
use App\Models\Tollgate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TollgateController extends Controller
{
    public function index (Request $request, $groupId){
           $start = $request->input('start', Carbon::now()->startOfMonth());
           $end = $request->input('end', Carbon::now());
           $res = array();

           if($request->location){
                foreach ($request->location as $locationId) {
                    $mergedData = array();
                    $locs = InterestLocation::selectRaw('name, amount, currency, public.ST_AsText(shapewkt) as shapewkt')->where('id', $locationId )->get();
                    $shapeWkt = $locs[0]['shapewkt'];

                    $query = DB::table('tblgrouplocation as t')
                        ->select('t3.description', 't3.registrationnumber', 't2.timestamp', 't2.speedkilometresperhour', 't2.odometerkilometres')
                        ->selectRaw("st_distance(ST_GeographyFromText('$shapeWkt'), t2.longlat) as distance_meters")
                        ->addSelect('t2.longlat')
                        ->join('tblposition as t2', 't.groupid', '=', 't2.groupid')
                        ->join('tblasset as t3', 't2.assetid', '=', 't3.assetid')
                        ->whereRaw("st_dwithin(ST_GeographyFromText('$shapeWkt'), t2.longlat, 150)")
                        ->whereBetween('t2.timestamp', [ $start,  $end])
                        ->where('t2.groupid', $groupId)
                        ->orderBy('t3.description', 'asc')
                        ->orderBy('t2.timestamp', 'asc')
                        ->distinct()
                        ->get();
                    if($query->isNotEmpty()){
                        foreach ($query as $location) {
                            $location->name = $locs[0]['name'];
                            $location->amount = $locs[0]['amount'];
                            $location->currency = $locs[0]['currency'];
                        }   
                        $location = json_decode($query, true);
                        $left  = 0;
                        $right = 1;
                        $mergedData[$left] = $location[$left];
                        $mergedData[$left]["dateIn"] = $location[$left]["timestamp"];
                        $mergedData[$left]["dateOut"] = $location[$left]["timestamp"]; 
                    
                        while($right < count($location)){
                            $leftTimestamp  = strtotime($mergedData[$left]["timestamp"]);
                            $rightTimestamp = strtotime($location[$right]["timestamp"]);
                            if ($rightTimestamp - $leftTimestamp <= 3600 && $mergedData[$left]["description"] == $location[$right]['description']) {
                                $mergedData[$left]["odometerkilometres"] = max($mergedData[$left]["odometerkilometres"], $location[$right]["odometerkilometres"]);
                                $mergedData[$left]["speedkilometresperhour"] = max($mergedData[$left]["speedkilometresperhour"], $location[$right]["speedkilometresperhour"]);
                                $mergedData[$left]["distance_meters"] = max($mergedData[$left]["distance_meters"], $location[$right]["distance_meters"]);       
                                $mergedData[$left]["dateIn"] = min($mergedData[$left]["dateIn"], $location[$right]["timestamp"]);
                                $mergedData[$left]["dateOut"] = max($mergedData[$left]["dateOut"], $location[$right]["timestamp"]);
                                $right++;
                            } else {
                                $left = $right;
                                $right++;
                                $mergedData[$left] = $location[$left];
                                $mergedData[$left]["dateIn"] = $location[$left]["timestamp"];
                                $mergedData[$left]["dateOut"] = $location[$left]["timestamp"]; 
                            };
                        }
                        $res = array_merge($res, $mergedData);
                    }
                }
            } else {
                return response()->json(['success' => false, 'message' => 'Location ID does not exist.']);;
            } 
            $resourceData = TollgateResource::collection($res);
            return response()->json($resourceData);
    }
}
