<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\TripsheetDistanceResource;
use App\Models\RouteLeg;
use App\Models\Tripsheet;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TripsheetDistanceController extends Controller
{
    public function index($id, $site, Request $request){
        $totalShuntDistance = 0;
        $totalemptyDistance = 0;
        $totalLoadDistance = 0;
        $startDate = $request->input('startdate', Carbon::now()->startOfMonth());
        $endDate   = $request->input('enddate', Carbon::now());
        $ts = Tripsheet::with('asset')
            ->where('status', 'ACTIVE')
            ->whereBetween('end', [$startDate, $endDate])
            ->when($id != 0, function ($query)  use ($id){
                    $query->where('groupid', $id);
            })
            ->when($site != 0, function ($query)  use ($site){
                    $query->whereHas('asset', function ($q) use ($site) {
                    $q->where('siteid', $site);
                });
            })
            ->select('tripsheet', 'groupid', 'tripsheetid', 'distance', 'targets', 'trip')
            ->distinct('tripsheet')
            ->orderBy('tripsheet')
            ->orderBy('tripsheetid', 'desc')
            ->get();
        if($ts->isEmpty()){
            return new TripsheetDistanceResource([
                'shuntDistance' => 0,
                'emptyDistance' => 0,
                'loadDistance' => 0,
            ]);
        }
        foreach ($ts as $item) {
            $trip = json_decode(stripslashes($item['trip']))->long; 
            $target = isset(json_decode(stripslashes($item['targets']))->distance) ? json_decode(stripslashes($item['targets']))->distance : 0;
            $distance = isset(json_decode(stripslashes($item['distance']))->fm_edit) && json_decode(stripslashes($item['distance']))->fm_edit > 0 ? json_decode(stripslashes($item['distance']))->fm_edit :  (isset(json_decode(stripslashes($item['distance']))->fm) ? json_decode(stripslashes($item['distance']))->fm: 0);
            //Shunt Distance
            if($distance > $target){
                $shunt = $distance - $target;
                $totalShuntDistance += $shunt;
            }
            // Loaded and Empty 
            if($trip != 'LOCAL(L)'){
                $shunt = $distance > $target ? $distance - $target : 0;
                $loadPc = $this->calc_load_percent($id, $trip);
                $totalemptyDistance += ($target*(100-$loadPc)/100)+($shunt*0.5);
                $totalLoadDistance += ($target*($loadPc/100))+($shunt*0.5);
            }  
        }  
        return new TripsheetDistanceResource([
            'shuntDistance' => $totalShuntDistance,
            'emptyDistance' => $totalemptyDistance,
            'loadDistance' => $totalLoadDistance,
        ]);
    }
    function calc_load_percent($id, $trip){

        $tripArr = explode("-", str_replace("(L)","",str_replace("(E)","",$trip)));
        $arr = [];
        if ($tripArr) {
            $tmp = "";
            foreach($tripArr AS $t){
                if ($tmp == "") {
                    $tmp = rtrim(ltrim($t));
                }
                else{
                    $tripStr = $tmp."-".$t;       
                    array_push($arr, $tripStr);
                    $tmp = rtrim(ltrim($t));
                }
            }
        }
        if (count($arr) > 0) {
            $totDist = 0;
            $totLoad = 0;
            if($id != 0){
                $rows = RouteLeg::where('groupid', $id)
                    ->whereIn('code', $arr)
                    ->get();
            } else {
                $rows = RouteLeg::whereIn('code', $arr)
                    ->get();
            }
            $tripArr = explode("-", $trip);
            $tmp 	 = "";
            foreach($tripArr AS $t){
                if ($tmp == "") {
                    $tmp = rtrim(ltrim($t));
                }
                else{
                    $leg = $tmp."-".$t;
                    $tmp = rtrim(ltrim(str_replace("(L)", "", str_replace("(E)", "",$t))));

                    foreach($rows AS $r){
                        if($leg ==$r['code']."(L)") {
                            $totDist += $r['distance'];
                            $totLoad += $r['distance'];
                            continue;
                        }
                        elseif($leg == $r['code']."(E)") {
                            $totDist += $r['distance'];
                            continue;
                        }
                    }
                    
                }
            }
            return $totDist>0 ? (100*$totLoad/$totDist) : 0;
        }

        return "0";
    }

}
