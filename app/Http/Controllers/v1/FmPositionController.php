<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\FmPositionResource;
use App\Models\FmPosition;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FmPositionController extends Controller
{
    public function index(Request $request, $id){
        $startDate = $request->input('startdate', Carbon::now()->startOfMonth());
        $endDate   = $request->input('enddate', Carbon::now());
        return FmPositionResource::collection(
            FmPosition::where('groupid', $id)
            ->whereBetween('timestamp', [$startDate, $endDate])
            ->paginate(10000)
        );
    }
    public function show($id){
        $pos = FmPosition::where('positionid',$id)
        ->first();

        if (!$pos) {
            return response()->json(['success' => false, 'message' => 'Position does not exist.']);
        }
        return (new FmPositionResource($pos))
            ->response()
            ->setStatusCode(200);
        }
}
