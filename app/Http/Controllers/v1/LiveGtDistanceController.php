<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\LiveGtDistanceResource;
use App\Models\GtPosition;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LiveGtDistanceController extends Controller
{
    public function index($id){
        $total = 0;
        $ts = GtPosition::selectRaw('(max(odometer) - min(odometer)) /1000 AS distance')
                ->whereBetween('datetime', [Carbon::now()->startOfMonth(), Carbon::now()])
                ->distinct('unit_name')
                ->where('client_id', $id)
                ->groupBy('unit_name')
                ->get();
        foreach ($ts as $item) {
            $total += $item['distance'];
        }
        
        return new LiveGtDistanceResource($total);
    }

    public function monthlyDistance($id){
        $totalDistanceByMonth = [];
        $ts = GtPosition::selectRaw('MAX(odometer) as max_odometer, MIN(odometer) as min_odometer')
                // ->selectRaw("TO_CHAR(datetime, 'Mon') as month")
                ->whereBetween('datetime', [Carbon::now()->subMonths(11)->startOfMonth(), Carbon::now()->endOfMonth()])
                ->distinct('unit_name')
                ->where('client_id', $id)
                ->groupBy('unit_name')
                ->get();
        // var_dump();
        // foreach ($ts as $position) {
        //     $distance = $position['distance'];
        //     $month = str_replace(' ', '', $position['month']);
        //     if (!isset($totalDistanceByMonth[$month])) {
        //         $totalDistanceByMonth[$month] = 0;
        //     }
        //     if($distance <= 20000000){
        //         $totalDistanceByMonth[$month] += $distance;
        //     }
            
        // }
        return $ts;
        // return response()->json($totalDistanceByMonth);
    }   
}
