<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\NotificationResource;
use App\Models\Notification;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index()
    {
        $notifications = Notification::all();
        return (NotificationResource::collection($notifications))
            ->response()
            ->setStatusCode(200);
    }

    public function store(Request $request)
    {
        $notification = new Notification;
        $notification->message = $request->message;
        $notification->groupid = $request->groupid;
        $notification->notification_date = Carbon::now()->toDateTimeString();
        if ($notification->save()) {
            User::query()->update(['notification' => 'PENDING']);
            return (new NotificationResource($notification))
                ->response()
                ->setStatusCode(201);
        } else {
            return response()->json(['error' => 'Failed to save notification'], 500);
        }
    }

    public function show($id)
    {
        $notification = Notification::find($id);
        return new NotificationResource($notification);
    }

    public function update(Request $request, $id)
    {
        $notification = Notification::findOrFail($id);
        $notification->update($request->all());
        return (new NotificationResource($notification))
            ->response()
            ->setStatusCode(200);
    }

    public function destroy($id)
    {
        $notification = Notification::findOrFail($id);
        $notification->delete();
        return (new NotificationResource($notification))
            ->response()
            ->setStatusCode(204);
    }
}
