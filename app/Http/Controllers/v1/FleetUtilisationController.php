<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\FleetUtilisationResource;
use App\Http\Resources\v1\GtTripResource;
use App\Models\Asset;
use App\Models\GtPosition;
use App\Models\Trip;
use App\Models\Unit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FleetUtilisationController extends Controller
{

    public function fmTotal($id){
        $fleetCount =   Asset:: when($id != 0, function ($query)  use ($id){
                $query->where('groupid', $id);
            })
            ->where('userstate', 'Available')
            ->count();
        return $fleetCount;
    }

    public function utilisedAssets($id, Request $request){ 
        $fleetUtilKms = 10;
   
        $groupConfigs = config('constants.GRP_CONFIGS');
        foreach ($groupConfigs as $key => $value) {
            if($value['id'] == $id){
                $fleetUtilKms = $value['fleetUtilKms'];
            }
        }
        $startDate = $request->input('startdate', Carbon::now()->startOfMonth());
        $endDate   = $request->input('enddate', Carbon::now());
        $minDist   = Carbon::parse(Carbon::now())->diffInDays(Carbon::parse(Carbon::now()->startOfMonth())) * $fleetUtilKms;
        $utilised = Trip:: when($id != 0, function ($query)  use ($id){
                        $query->where('tbltrip.groupid', $id);
                })
            ->whereBetween('tripend', [$startDate, $endDate])
            ->select('tbltrip.assetid')
            ->join('tblasset', 'tbltrip.assetid', '=', 'tblasset.assetid')
            ->select('tbltrip.assetid', 'tblasset.description', 'model', 'emptyconsumption', 'loadconsumption',  )
            ->selectRaw('SUM(distancekilometers) as total_distance')
            ->havingRaw('SUM(distancekilometers) > '.$minDist)
            ->groupBy('tbltrip.assetid', 'tblasset.description', 'model', 'emptyconsumption', 'loadconsumption', )
            ->paginate(25);
        return $utilised;
    }


    public function underutilisedAssets($id, Request $request){ 

        $fleetUtilKms = 10;
   
        $groupConfigs = config('constants.GRP_CONFIGS');
        foreach ($groupConfigs as $key => $value) {
            if($value['id'] == $id){
                $fleetUtilKms = $value['fleetUtilKms'];
            }
        }
        $startDate = $request->input('startdate', Carbon::now()->startOfMonth());
        $endDate   = $request->input('enddate', Carbon::now());
        $maxDist   = Carbon::parse(Carbon::now())->diffInDays(Carbon::parse(Carbon::now()->startOfMonth())) * $fleetUtilKms;

        $underutilised = Asset::join('tbltrip', 'tblasset.assetid', '=', 'tbltrip.assetid')
            ->select('assetid')
            ->when($id != 0, function ($query)  use ($id){
                $query->where('tblasset.groupid', $id);
            })
            ->where('userstate', 'Available')
            ->whereBetween('tbltrip.tripend', [$startDate, $endDate])
            ->select('tbltrip.assetid', 'tblasset.description', 'model', 'emptyconsumption', 'loadconsumption',  )
            ->selectRaw('SUM(distancekilometers) as total_distance')
            ->havingRaw('SUM(distancekilometers) < '.$maxDist)
            ->groupBy('tbltrip.assetid', 'tblasset.description', 'model', 'emptyconsumption', 'loadconsumption', )
            ->paginate(25);
        
        return $underutilised;
    }

    public function gtTotal($id){
        $fleetCount =  Unit::where(function ($query) {
                $query->where('client_id', 3000)
                    ->orWhere('client_id', 1926);
            })
            ->distinct('unit_name')
            ->count();
        return $fleetCount;
    }


    public function utilisedUnits($id, Request $request){ 
        $startDate = $request->input('startdate', Carbon::now()->startOfMonth());
        $endDate   = $request->input('enddate', Carbon::now());

        $dist   = Carbon::parse(Carbon::now())->diffInDays(Carbon::parse(Carbon::now()->startOfMonth())) * 1000;
        $units = Unit::selectRaw('DISTINCT ON (tblunit_gt.unit_name) tblunit_gt.unit_name')
            ->selectRaw("MAX(tblposition_gt.odometer) as odometer")
            ->selectRaw("MAX(tblposition_gt.speed) as speed")
            ->selectRaw('MAX(tblposition_gt.odometer) - MIN(tblposition_gt.odometer) as "distance"')
            ->leftJoin('tblposition_gt', 'tblunit_gt.unit_id', '=', 'tblposition_gt.unit_id')
            ->where(function ($query) {
                $query->where('tblunit_gt.client_id', 3000)
                    ->orWhere('tblunit_gt.client_id', 1926);
            })
            ->whereBetween('tblposition_gt.datetime', [$startDate, $endDate])
            ->groupBy('tblunit_gt.unit_name')
            ->havingRaw('MAX(tblposition_gt.odometer) - MIN(tblposition_gt.odometer) > '.$dist)
            ->paginate(2);
        return GtTripResource::collection($units);
    }


    public function underutilisedUnits($id, Request $request){ 
   
        $startDate = $request->input('startdate', Carbon::now()->startOfMonth());
        $endDate   = $request->input('enddate', Carbon::now());

        $dist   = Carbon::parse(Carbon::now())->diffInDays(Carbon::parse(Carbon::now()->startOfMonth())) * 1000;

        $units = Unit::selectRaw('DISTINCT ON (tblunit_gt.unit_name) tblunit_gt.unit_name')
            ->selectRaw('MAX(tblposition_gt.odometer) - MIN(tblposition_gt.odometer) as "distance"')
            ->selectRaw("MAX(tblposition_gt.odometer) as odometer")
            ->selectRaw("MAX(tblposition_gt.speed) as speed")
            ->leftJoin('tblposition_gt', 'tblunit_gt.unit_id', '=', 'tblposition_gt.unit_id')
            ->where(function ($query) {
                $query->where('tblunit_gt.client_id', 3000)
                    ->orWhere('tblunit_gt.client_id', 1926);
            })
            ->whereBetween('tblposition_gt.datetime', [$startDate, $endDate])
            ->groupBy('tblunit_gt.unit_name')
            ->havingRaw('MAX(tblposition_gt.odometer) - MIN(tblposition_gt.odometer) < '.$dist)
            ->paginate(2);
        return GtTripResource::collection($units);
    }

}