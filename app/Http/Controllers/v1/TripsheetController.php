<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\OpenTripsheetResource;
use App\Http\Resources\v1\TripsheetResource;
use App\Http\Resources\v2\TripsheetResource as V2TripsheetResource;
use App\Models\Tripsheet;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TripsheetController extends Controller
{
    
    public function index($gid)
    {
        $ts = Tripsheet::where('status', 'ACTIVE')
            ->where('groupid', $gid)
            ->distinct('tripsheet')
            ->orderBy('tripsheet')
            ->orderBy('tripsheetid', 'desc')
            ->simplePaginate(10);
        return (TripsheetResource::collection($ts))
            ->response()
            ->setStatusCode(200);  
    }

    public function show($gid, $tid)
    {
        $ts = Tripsheet::where('status', 'ACTIVE')
            ->where('groupid',$gid)
            ->where('tripsheet',$tid)
            ->distinct('tripsheet')
            ->orderBy('tripsheet')
            ->orderBy('tripsheetid', 'desc')
            ->first();
        if (!$ts) {
            return response()->json(['success' => false, 'message' => 'Tripsheet does not exist.']);
        }
        return (new TripsheetResource($ts))
            ->response()
            ->setStatusCode(200);
    }

    public function v1index($gid, $sid, Request $request)
    {
        $startDate = $request->input('startdate', Carbon::now()->startOfMonth());
        $endDate   = $request->input('enddate', Carbon::now());
        $status   = $request->input('status', 'ACTIVE');

        $ts = Tripsheet::join('tbl_user', 'tbltripsheet.userid', '=', 'tbl_user.id')
            ->select('tbltripsheet.*', 'tbl_user.name')
            ->where('tbltripsheet.status', $status)
            ->when($status == 'ACTIVE', function ($query)  use ($startDate, $endDate){
                $query->whereBetween('end', [$startDate, $endDate]);
            })
            ->when($status == 'OPEN', function ($query)  use ($startDate, $endDate){
                $query->where(function ($query) use ($startDate, $endDate) {
                    $query->whereBetween('end', [$startDate, $endDate])
                    ->orWhereBetween('start', [$startDate, $endDate]);
                });
            })
            ->when($gid != 0, function ($query)  use ($gid){
                    $query->where('groupid', $gid);
            })
            ->when($sid != 0, function ($query)  use ($sid){
                    $query->whereHas('asset', function ($q) use ($sid) {
                    $q->where('siteid', $sid);
                });
            })
            ->distinct('tripsheet')
            ->orderBy('tripsheet')
            ->orderBy('tripsheetid', 'desc')
            ->paginate(25);
        return(V2TripsheetResource::collection($ts))
            ->response()
            ->setStatusCode(200); 
    }

    public function v1show($tid)
    {
        $ts = Tripsheet::where('tripsheet',$tid)
            ->distinct('tripsheet')
            ->orderBy('tripsheet')
            ->orderBy('tripsheetid', 'desc')
            ->first();

        if (!$ts) {
            return response()->json(['success' => false, 'message' => 'Tripsheet does not exist.']);
        }
        return (new V2TripsheetResource($ts))
            ->response()
            ->setStatusCode(200);
    }
}
