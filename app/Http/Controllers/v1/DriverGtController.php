<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\DriverGtRequest;
use App\Http\Resources\v1\DriverGtResource;
use App\Models\DriverGt;
use Illuminate\Http\Request;

class DriverGtController extends Controller
{

    public function group($id)
    {
        $driver = DriverGt::where('client_id', $id)->get();
        return (DriverGtResource::collection($driver))
            ->response()
            ->setStatusCode(200);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
    }

    public function index()
    {
        $driver = DriverGt::all();
        return (DriverGtResource::collection($driver))
            ->response()
            ->setStatusCode(200); 
    }

    public function store(DriverGtRequest $request)
    {
        $driver = new DriverGt;
        $driver->client_id = $request->client_id;
        $driver->name = $request->name;
        $driver->employeenumber =  $request->employeenumber;
        $driver->country = $request->country;
        $driver->save();
        return (new DriverGtResource($driver))
            ->response()
            ->setStatusCode(201);
    }


    public function show(DriverGt $driver)
    {
        return new DriverGtResource($driver);
    }

    public function update(DriverGtRequest $request, $id)
    {
        $driver = DriverGt::findOrFail($id);
        $driver->update($request->all());
        return (new DriverGtResource($driver))
            ->response()
            ->setStatusCode(200);
    }

    public function destroy($id)
    {
        $driver = DriverGt::findOrFail($id);
        $driver->delete();
        return (new DriverGtResource($driver))
            ->response(null)
            ->setStatusCode(204);
    }
}
