<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\FmGroupResource;
use App\Models\FmGroup;

class FmGroupController extends Controller
{
 

    public function index()
    {
        
        $group = FmGroup::paginate(10);
        return FmGroupResource::collection($group);
    }

    public function show($id)
    {
        $group = FmGroup::findOrFail($id);
        if (!$group) {
            return response()->json(['success' => false, 'message' => 'Group does not exist.']);
        }
    
        return (new FmGroupResource($group))
            ->response()
            ->setStatusCode(200);
    }

}
