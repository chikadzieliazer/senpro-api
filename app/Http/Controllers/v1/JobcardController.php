<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\JobcardResource;
use App\Models\Jobcard;
use Carbon\Carbon;
use Illuminate\Http\Request;

class JobcardController extends Controller
{
    public function group($id)
    {
        $jc = Jobcard::where('groupid', $id)->get();
        return (JobcardResource::collection($jc))
            ->response()
            ->setStatusCode(200);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jc = Jobcard::paginate();
        return JobcardResource::collection($jc);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jc = new Jobcard;
        $jc->groupid = $request->groupid;
        $jc->name = $request->name;
        $jc->description = $request->description;
        $jc->type = $request->type;
        $jc->density = $request->density;
        $jc->units = $request->units;
        $jc->created = Carbon::now()->toDateTimeString();
        $jc->save();
        return (new JobcardResource($jc))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Jobcard $jobcard)
    {
        return new JobcardResource($jobcard);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $jc = Jobcard::findOrFail($id);
        $jc->update($request->all());
        return (new JobcardResource($jc))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jc = Jobcard::findOrFail($id);
        $jc->delete();
        return (new JobcardResource($jc))
            ->response(null)
            ->setStatusCode(204);
    }
}
