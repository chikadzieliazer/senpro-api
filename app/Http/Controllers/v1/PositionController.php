<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\PositionResource;
use App\Models\GtPosition;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    public function index($id, Request $request){
        $startDate = $request->input('startdate', Carbon::now()->startOfMonth());
        $endDate   = $request->input('enddate', Carbon::now());
        return PositionResource::collection(
            GtPosition::where('client_id', $id)
            ->whereBetween('datetime', [$startDate, $endDate])
            ->paginate(500)
        );
    }

    public function show($id){

        $position = GtPosition::where('position_id',$id)
            ->first();
        if (!$position) {
            return response()->json(['success' => false, 'message' => 'Position does not exist.']);
        }
        return (new PositionResource($position))
            ->response()
            ->setStatusCode(200);
        }
}
