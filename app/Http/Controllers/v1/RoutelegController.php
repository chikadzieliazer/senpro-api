<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\RouteLegRequest;
use App\Http\Resources\v1\RouteLegResource;
use App\Models\RouteLeg;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RoutelegController extends Controller
{

    public function group($id)
    {
        $customer = RouteLeg::where('groupid', $id)->get();
        return (RouteLegResource::collection($customer))
            ->response()
            ->setStatusCode(200);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routeleg = RouteLeg::paginate(4);
        return RouteLegResource::collection($routeleg);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RouteLegRequest $request)
    {
        $routeleg = new RouteLeg;
        $routeleg->groupid = $request->groupid;
        $routeleg->leg = $request->leg;
        $routeleg->code = $request->code;
        $routeleg->distance = $request->distance;
        $routeleg->duration_empty = $request->duration_empty;
        $routeleg->duration_loaded = $request->duration_loaded;
        $routeleg->created = Carbon::now()->toDateTimeString();
        $routeleg->save();
        return (new RouteLegResource($routeleg))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(RouteLeg $routeleg)
    {
        return new RouteLegResource($routeleg);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $routeleg = RouteLeg::findOrFail($id);
        $routeleg->update($request->all());
        return (new RouteLegResource($routeleg))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $routeleg = RouteLeg::findOrFail($id);
        $routeleg->delete();
        return (new RouteLegResource($routeleg))
            ->response()
            ->setStatusCode(204);
    }
}
