<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\TripResource;
use App\Models\Trip;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TripController extends Controller
{
    public function index($id, Request $request){
        $startDate = $request->input('startdate', Carbon::now()->startOfMonth());
        $endDate   = $request->input('enddate', Carbon::now());
        return TripResource::collection(
            Trip::with(['driver:driverid,name', 'asset:assetid,description'])
                ->where('groupid', $id)
                ->whereBetween('tripend', [$startDate, $endDate])
                ->paginate(500)
        );
    }

    public function show($id)
    {
        $trip = Trip::where('tripid',$id)
            ->first();
        if (!$trip) {
            return response()->json(['success' => false, 'message' => 'Trip does not exist.']);
        }
        return (new TripResource($trip))
            ->response()
            ->setStatusCode(200);
    }
}
