<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\TrailerRequest;
use App\Http\Resources\v1\TrailerResource;
use App\Models\Trailer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TrailerController extends Controller
{
    public function group($id)
    {
        $customer = Trailer::where('groupid', $id)->get();
        return (TrailerResource::collection($customer))
            ->response()
            ->setStatusCode(200);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trailer = Trailer::paginate();
        return TrailerResource::collection($trailer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TrailerRequest $request)
    {
        $trailer = new Trailer;
        $trailer->old_id = $request->old_id;
        $trailer->groupid = $request->groupid;
        $trailer->name = $request->name;
        $trailer->type = $request->type;
        $trailer->status = $request->status;
        $trailer->description = $request->description;
        $trailer->units = $request->units;
        $trailer->mileage = $request->mileage;
        $trailer->make = $request->make;
        $trailer->model = $request->model;
        $trailer->registration = $request->registration;
        $trailer->created = Carbon::now()->toDateTimeString();
        $trailer->save();
        return (new TrailerResource($trailer))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Trailer $trailer)
    {
        return new TrailerResource($trailer);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $trailer = Trailer::findOrFail($id);
        $trailer->update($request->all());
        return (new TrailerResource($trailer))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trailer = Trailer::findOrFail($id);
        $trailer->delete();
        return (new TrailerResource($trailer))
            ->response(null)
            ->setStatusCode(204);
    }
}
