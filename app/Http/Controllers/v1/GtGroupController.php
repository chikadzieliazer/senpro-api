<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\GtGroupResource;
use App\Models\GtGroup;
use Illuminate\Http\Request;

class GtGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $group = GtGroup::paginate(10);
        return GtGroupResource::collection($group);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function show(GtGroup $gtgroup)
    {
        return new GtGroupResource($gtgroup);
    }

}
