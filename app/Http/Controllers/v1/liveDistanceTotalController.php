<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\liveDistanceTotalGtResource;
use App\Http\Resources\v1\liveDistanceTotalResource;
use App\Models\GtPosition;
use App\Models\Trip;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class liveDistanceTotalController extends Controller
{
    public function fmTotals(Request $request){
        $groupId  = $request->has('group') ? $request->input('group') : 0;    
        $siteId   = $request->has('site') ? $request->input('site') : 0;       
        $start    = $request->input('start', Carbon::now()->startOfMonth());
        $end      = $request->input('end', Carbon::now());   
        $ts = Trip::selectRaw("SUM(distancekilometers) as distance")
                ->selectRaw("AVG((drivingtime)::integer) as drivingtime")
                ->selectRaw("AVG((standingtime)::integer) as standing_time")
                ->selectRaw("AVG(maxspeedkilometersperhour) as avg_speed")
                ->selectRaw("SUM(fuelusedlitres) as fm_fuel")
                ->whereBetween('tripstart', [$start, $end])
                ->when($groupId != 0, function ($query)  use ($groupId){
                    $query->where('groupid', $groupId);
                    })  
                    ->when($siteId != 0, function ($query)  use ($siteId){
                            $query->whereHas('asset', function ($q) use ($siteId) {
                            $q->where('siteid', $siteId);
                        });
                    })
                    ->whereHas('asset', function ($q)  {
                        $q->where('assettypeid', '!=', 2);
                    })
                ->get(); 
                return liveDistanceTotalResource::collection($ts);
    }

    public function gtTotals(Request $request){ 
        $total = 0;
        $count = 0;
        $avg   = 0;
        $clientId  = $request->has('unit') ? $request->input('unit') : 0;          
        $start    = $request->input('start', Carbon::now()->startOfMonth());
        $end      = $request->input('end', Carbon::now());

        $ts = GtPosition::select(DB::raw('max(odometer) - min(odometer) AS distance'))
                ->distinct('unit_name')
                ->selectRaw("AVG(speed) as avg_speed")
                ->where('client_id', $clientId)
                ->whereBetween('datetime', [$start, $end])
                ->groupBy('unit_name')
                ->get();
        foreach ($ts as $item) {
            if($item['distance'] <= 20000000){
                $total += $item['distance'];
                $avg += $item['avg_speed'];
            }
            $count++;
        }
        return response()->json([
            "data" => [
                'type' => 'Total Gt Asset Trip',
                'attributes' => [
                    'distance' => number_format($total/1000, 2),
                    'average speed' => number_format($avg/$count, 2)
                ]
            ]
        ],200);
     }
}

