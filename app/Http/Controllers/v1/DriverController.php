<?php


namespace App\Http\Controllers\v1;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\DriverResource;
use App\Models\Driver;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    public function group($id)
    {
        $driver = Driver::where('groupid', $id)->get();
        return (DriverResource::collection($driver))
            ->response()
            ->setStatusCode(200);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $driver = Driver::paginate(25);
        return (DriverResource::collection($driver))
            ->response()
            ->setStatusCode(200); 
    }

    public function show(Driver $driver)
    {
        return new DriverResource($driver);
    }

}
