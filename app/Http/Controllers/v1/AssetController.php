<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\AssetResource;
use App\Models\Asset;
use Illuminate\Http\Request;

class AssetController extends Controller
{
    public function group($id)
    {
        $customer = Asset::where('groupid', $id)->get();
        return (AssetResource::collection($customer))
            ->response()
            ->setStatusCode(200);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
    }
    
    public function index()
    {
        $asset = Asset::paginate(5);
        return AssetResource::collection($asset);
    }

    public function show($id)
    {
        $asset = Asset::where('assetid',$id)->first();
        if (!$asset) {
            return response()->json(['success' => false, 'message' => 'Asset does not exist.']);
        }
        return (new AssetResource($asset))
            ->response()
            ->setStatusCode(200);
    }
}
