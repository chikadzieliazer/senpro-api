<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\DecomissionRequest;
use App\Http\Resources\v1\DecomissionResource;
use App\Models\Decomission;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Google\Cloud\Storage\StorageClient;
use SplFileInfo;

class DecomissionController extends Controller
{

    public function index(Request $request)
    {
        $startDate = $request->input('start');
        $endDate = $request->input('end');
        
        if(is_null($startDate)){
            $decom = Decomission::all();
        } else {
            $decom = Decomission::whereBetween('created', [$startDate, $endDate])->get();
        }
        return DecomissionResource::collection($decom);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DecomissionRequest $request)
    {
        $googleConfigFile = file_get_contents(config_path('sendemplatform.json'));
        $storage = new StorageClient([
            'keyFile' => json_decode($googleConfigFile, true)
        ]);
        $storageBucketName = config('cloudstorage.storage_bucket');
        $bucket = $storage->bucket($storageBucketName);
        $filesArray = json_decode($request->decomdocument, true);
        $decomFileNames = array();
        if($filesArray){
            for($i=0; $i < count($filesArray); $i++){
                $filePath = $filesArray[$i]['name'];
                $fileName = time().'-'.$filesArray[$i]['postname'];
                $decomFileNames[$i] = $fileName;
                $info = new SplFileInfo($filePath);
                $DecomFilePath = $info->getRealPath();
                $fileSource = fopen($DecomFilePath, 'r');
                $bucket->upload($fileSource, [
                    'predefinedAcl' => 'publicRead',
                    'name' => $fileName
                ]);
            }
        }
        $encodeFiles = implode(",",$decomFileNames);
        $decom = new Decomission;
        $decom->userid = $request->userid;
        $decom->groupname = $request->groupname;
        $decom->username = $request->username;
        $decom->grouptype = $request->grouptype;
        $decom->asset = $request->asset;
        $decom->peripherals = $request->peripherals;
        $decom->decomdocument = $encodeFiles;
        $decom->ticketurl = $request->ticketurl;
        $decom->terminationnumber = $request->terminationnumber;
        $decom->decomcomment = $request->decomcomment;
        $decom->created = Carbon::now()->toDateTimeString();
        $decom->save();
        return (new DecomissionResource($decom))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Decomission $decomission)
    {
        return new DecomissionResource($decomission);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  
     * $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $decom = Decomission::findOrFail($id);
        $decom->update($request->all());
        return (new DecomissionResource($decom))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decom = Decomission::findOrFail($id);
        $decom->delete();
        return response()->json([
            'success' => 'Decomission Deleted Successfully!'
        ]);
    }
}
