<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\Dashboard;
use App\Models\Event;
use App\Models\FuelGt;
use App\Models\GtPosition;
use App\Models\RouteLeg;
use App\Models\Trip;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;

class DashboardController extends Controller
{

    public $defaultStart;
    public $defaultEnd;
    public $requestStart;
    public $requestEnd;
    public $prevStart;
    public $prevEnd;
    public $site;

    public function __construct(Request $request){
        $this->requestStart = $request->input('startdate', Carbon::now()->startOfMonth());
        $this->requestEnd = $request->input('enddate', Carbon::now());
        $this->site = $request->has('site') ? $request->input('site') : 0;
        $carbonStart = new Carbon($this->requestStart);
        $carbonEnd = new Carbon($this->requestEnd);
        $diff = $carbonStart->diffInMonths($carbonEnd) != 0 ? $carbonStart->diffInMonths($carbonEnd) : 1;
        $this->prevStart = $carbonStart->subMonths($diff);
        $this->prevEnd = $carbonEnd->subMonths($diff);
    }
       
    public function dashboard($id){
        return response()->json([
            'Bottom Consumption' => $this->bottomConsumption($id),
            'Top Consumption' => $this->topConsumption($id),
            'Top Distance' => $this->topDistance($id),
            'Bottom Transits' => $this->bottomTransits($id),
            'Empty Distance' => $this->emptyDistance($id),
            'Load Distance' => $this->loadDistance($id),
            'Shunt Distance' => $this->shuntDistance($id),
            'FM Score' => $this->fmScore($id),
            'Custom Score' => $this->customScore($id),
            'Sendem Score' => $this->sendemScore($id),
            'Fuel Score' => $this->fuelScore($id),
            'Transit Score' => $this->transitScore($id),
            'Mixed Chart' => $this->monthlyDistanceAverages($id),
            'Tripsheet Distance' => $this->tripsheetsDistance($id),
            'Tripsheets Closed' => $this->tripsheetsClosed($id),
            'Fuel Consumption' => $this->fuelConsumption($id),
            'Fuel Variance' => $this->fuelVariance($id),
            'FM Distance' => $this->fmDistance($id),
            'Utilisation' => $this->fleetUtilisation($id)
        ], 200);
    }


    public function gtDistance($id){
        $total = 0;
        $prevTotal = 0;
        $obj = new stdClass();

        $ts = GtPosition::select(DB::raw('(max(odometer) - min(odometer)) /1000 AS distance'))
                ->distinct('unit_name')
                ->where('client_id', $id)
                ->whereBetween('datetime', [$this->requestStart, $this->requestEnd])
                ->groupBy('unit_name')
                ->get();
        
        $prevTs = GtPosition::select(DB::raw('(max(odometer) - min(odometer)) /1000 AS distance'))
                ->distinct('unit_name')
                ->where('client_id', $id)
                ->whereBetween('datetime', [$this->prevStart, $this->prevEnd])
                ->groupBy('unit_name')
                ->get();
        foreach ($ts as $item) {
            $total += $item['distance'];
         }
        foreach ($prevTs as $item) {
            $prevTotal += $item['distance'];
         }
        $percentage = $prevTotal == 0 ? 0 : (($total - $prevTotal)/$prevTotal) * 100;
        $obj->{'GT Distance'} = number_format($total, 2);
        $obj->Difference = number_format($percentage,1);
        
        return $obj;
    }

    public function fmDistance($id){        
        $obj = new stdClass();
        $site = $this->site;
        $ts =   Trip::when($id != 0, function ($query)  use ($id){
                $query->where('groupid', $id);
            })
            ->whereHas('asset', function ($q)  {
                $q->where('assettypeid', '!=', 2);
            })
            ->when($site != 0, function ($query)  use ($site){
                        $query->whereHas('asset', function ($q) use ($site) {
                        $q->where('siteid', $site);
                    });
                })
            ->whereBetween('tripstart', [$this->requestStart, $this->requestEnd])
            ->sum('distancekilometers');
            
        $prevTs = Trip::when($id != 0, function ($query)  use ($id){
                $query->where('groupid', $id);
            })
            
            ->when($site != 0, function ($query)  use ($site){
                        $query->whereHas('asset', function ($q) use ($site) {
                        $q->where('siteid', $site);
                    });
                })
            ->whereBetween('tripstart', [$this->prevStart, $this->prevEnd])
            ->sum('distancekilometers');

        $percentage = $ts == 0 || $prevTs == 0 ? 0 : (($ts - $prevTs)/$prevTs) * 100;
        $obj->{'FM Distance'} = number_format($ts,2);
        $obj->Difference = number_format($percentage,1);
        return $obj;
    }

    public function fleetUtilisation($id){ 
        if($id != 5432009351795875573 && $id != -3273034392599683215 
        && $id != -8493556079849164556
        && $id != -5382047893814814814 && $id != -4871886099058137262
        ) {
            return 0;
        }     
        $obj            = new stdClass();
        $startDate      = $this->requestStart;
        $endDate        = $this->requestEnd;
        $minDistance    = Carbon::parse($startDate)->diffInDays(Carbon::parse($endDate)) * 10;
        $utilisedVehicles     = 0;
        
        $assetDistance = Trip::where('groupid', $id)
            ->whereBetween('tripend', [$startDate, $endDate])
            ->select('assetid')
            ->selectRaw('SUM(distancekilometers) as total_distance')
            ->groupBy('assetid')
            ->get();

        $fleetCount = Asset::where('groupid', $id)
            ->where('userstate', 'Available')
            ->count();

        $assetDistance = json_decode($assetDistance, true);
        foreach ($assetDistance as  $d) {
            if((float)($d['total_distance']) > $minDistance ){
                $utilisedVehicles++;
            }
        }
        
        $percentageUtilised = ($utilisedVehicles/$fleetCount)*100;
        $obj->{'Utilized Vehicles'} = $utilisedVehicles;
        $obj->{'Underutilized Vehicles'} = $fleetCount - $utilisedVehicles;
        $obj->{'Utilized Percentage'} = number_format($percentageUtilised, 2);
        $obj->{'Underutilized Percentage'} = number_format(100 - $percentageUtilised,2);
        return $obj;
    }


    public function tripsheetsClosed($id){
        $ts = $this->query($this->requestStart, $this->requestEnd, $this->site, $id)->count();
        $prevTs = $this->query($this->prevStart, $this->prevEnd, $this->site, $id)->count();
        $percentage = $prevTs == 0 ? 0 : (($ts - $prevTs)/$prevTs) * 100;  
        $obj = new stdClass();
        $obj->{'Tripsheets Closed'} = $ts;
        $obj->Difference = number_format($percentage,1);
        return $obj;
    }

    public function fuelConsumption($id){
        $distance = $fuel = $prevDistance = $prevFuel = 0; 
        $obj = new stdClass();
        $ts = $this->query($this->requestStart, $this->requestEnd, $this->site, $id)->get();
        $prevTs = $this->query($this->prevStart, $this->prevEnd, $this->site, $id)->get();
        if($ts->isEmpty() && $prevTs->isEmpty()){
            $obj->{'Fuel Consumption'} = 0;
            $obj->Difference = 0;
            return $obj;
        }
        foreach ($ts as $item) {
            $distance += isset(json_decode(stripslashes($item['distance']))->fm_edit) && json_decode(stripslashes($item['distance']))->fm_edit > 0 ? json_decode(stripslashes($item['distance']))->fm_edit : json_decode(stripslashes($item['distance']))->fm;
            $fuel += isset(json_decode(stripslashes($item['fuel']))->actual) ? (int)json_decode(stripslashes($item['fuel']))->actual : 0;
        }
        foreach ($prevTs as $item) {
            $prevDistance += json_decode(stripslashes($item['distance']))->fm_edit > 0 ? json_decode(stripslashes($item['distance']))->fm_edit : json_decode(stripslashes($item['distance']))->fm;
            $prevFuel += isset(json_decode(stripslashes($item['fuel']))->actual) ? (float)json_decode(stripslashes($item['fuel']))->actual : 0;
        }
        $prevConsumption = $prevFuel !== 0 ?  $prevDistance / $prevFuel : 0;
        $consumption =  $fuel !== 0 ?  $distance / $fuel : 0;
        $percentage = !empty($prevConsumption) ? (($consumption - $prevConsumption)/$prevConsumption) * 100 : 0;
        $obj->{'Fuel Consumption'} = number_format($consumption,2);
        $obj->Difference = number_format($percentage,1);
        return $obj;
    }
    
    public function fuelVariance($id){
        $total = 0;
        $prevTotal =  $fuelTarget = 0; 
        $obj = new stdClass();
        $ts = $this->query($this->requestStart, $this->requestEnd, $this->site, $id)->get();
        $prevTs = $this->query($this->prevStart, $this->prevEnd, $this->site, $id)->get();
        if($ts->isEmpty() && $prevTs->isEmpty()){
              $obj->{'Fuel Variance'} = 0;
                $obj->Difference = 0; 
                return $obj;
        }
        foreach ($ts as $item) {
            $dynamicDist =  isset(json_decode(stripslashes($item['distance']))->fm_edit) && json_decode(stripslashes($item['distance']))->fm_edit > 0 ? json_decode(stripslashes($item['distance']))->fm_edit : json_decode(stripslashes($item['distance']))->fm;
            $shunt  = $dynamicDist>json_decode(stripslashes($item['targets']))->distance? $dynamicDist-json_decode(stripslashes($item['targets']))->distance : 0;

            if ($item['groupid']  == -3273034392599683215) {
                $fuelTarget 	 = (json_decode(stripslashes($item['targets']))->consumption>0) ? $dynamicDist/json_decode(stripslashes($item['targets']))->consumption : 0;
            } else if($item['groupid']  == -5511162494231442644){
                $fuelTarget = json_decode(stripslashes($item['targets']))->fuel + (($id !== 1853956352811964940 || $id !== -3273034392599683215)  ? ($shunt/0.5) : 0);
            }
            else{
                $fuelTarget = json_decode(stripslashes($item['targets']))->fuel + (($id !== 1853956352811964940 || $id !== -3273034392599683215)  ? ($shunt*0.5) : 0);
            }

            if($item['groupid'] == 1853956352811964940){
                $fmFuel = isset(json_decode(stripslashes($item['fuel']))->fm) ? floatval(json_decode(stripslashes($item['fuel']))->fm) : 0;
                
                $actualFuel = isset(json_decode(stripslashes($item['fuel']))->actual) ? floatval(json_decode(stripslashes($item['fuel']))->actual) : 0;
                 $total += $fmFuel - $actualFuel;
            } else {
                $actualFuel = isset(json_decode(stripslashes($item['fuel']))->actual) ? floatval(json_decode(stripslashes($item['fuel']))->actual) : 0;
                $total += $fuelTarget - $actualFuel;
            }
        }

        foreach ($prevTs as $item) {
            $dynamicDist =  isset(json_decode(stripslashes($item['distance']))->fm_edit) && json_decode(stripslashes($item['distance']))->fm_edit > 0 ? json_decode(stripslashes($item['distance']))->fm_edit : json_decode(stripslashes($item['distance']))->fm;
            $shunt  = isset(json_decode(stripslashes($item['targets']))->distance) && $dynamicDist > json_decode(stripslashes($item['targets']))->distance ? $dynamicDist-json_decode(stripslashes($item['targets']))->distance : 0;
            
            if ($item['groupid']  == -3273034392599683215) {
                $fuelTarget 	 = (json_decode(stripslashes($item['targets']))->consumption>0) ? $dynamicDist/json_decode(stripslashes($item['targets']))->consumption : 0;
            }else if($item['groupid']  == -5511162494231442644){
                $fuelTarget = json_decode(stripslashes($item['targets']))->fuel + (($id !== 1853956352811964940 || $id !== -3273034392599683215)  ? ($shunt/0.5) : 0);
            }else{
                $fuelTarget 	 = isset(json_decode(stripslashes($item['targets']))->fuel) ? json_decode(stripslashes($item['targets']))->fuel : 0;
            }

            if($item['groupid'] == 1853956352811964940){
                $actualFuel = isset(json_decode(stripslashes($item['fuel']))->actual) ? (float)json_decode(stripslashes($item['fuel']))->actual : 0;
                $fmFuel = (isset(json_decode(stripslashes($item['fuel']))->fm) ? floatval(json_decode(stripslashes($item['fuel']))->fm) : 0);
                 $prevTotal += $fmFuel - $actualFuel;
            } else {
                $actualFuel = isset(json_decode(stripslashes($item['fuel']))->actual) ? (float)json_decode(stripslashes($item['fuel']))->actual : 0;
                $prevTotal += $fuelTarget - $actualFuel;
            }
        }
        $percentage = !empty($prevTotal) ?  (($total - $prevTotal)/($prevTotal < 0 ? $prevTotal*-1 : $prevTotal* 1)) * 100 : 0;
        
        $obj->{'Fuel Variance'} = number_format($total, 2);
        $obj->Difference = number_format($percentage,1);
        return $obj;
    }

    public function tripsheetsDistance($id){
        $obj = new stdClass();
        $total= 0; 
        $prevTotal= 0;
         $ts = $this->query($this->requestStart, $this->requestEnd, $this->site, $id)->get();
        $prevTs = $this->query($this->prevStart, $this->prevEnd, $this->site, $id)->get();
        if($ts->isEmpty() && $prevTs->isEmpty()){
            $obj->{'Tripsheet Distance'} = 0;
            $obj->Difference = 0;
            return $obj;
        }
        foreach ($ts as $item) {  
            
            $total += isset(json_decode(stripslashes($item['distance']))->fm_edit) && json_decode(stripslashes($item['distance']))->fm_edit > 0 ? json_decode(stripslashes($item['distance']))->fm_edit : json_decode(stripslashes($item['distance']))->fm;
        }   
        foreach ($prevTs as $item) {   
            $prevTotal += json_decode(stripslashes($item['distance']))->fm_edit > 0 ? json_decode(stripslashes($item['distance']))->fm_edit : json_decode(stripslashes($item['distance']))->fm;
        }
        $percentage = $prevTotal == 0 ? 0 : (($total - $prevTotal)/$prevTotal) * 100;  
        $obj->{'Tripsheet Distance'} = number_format($total);
        $obj->Difference = number_format($percentage,1);
        return $obj;
    }

    public function fmScore($id){
        $totalScore = $count = 0; 
        $ts = $this->query($this->requestStart, $this->requestEnd,$this->site, $id)->get();
        if($ts->isEmpty()){
            return 0;
        }
        foreach ($ts as $item) {
            if (isset(json_decode(stripslashes($item['scores']))->overall)) {
                $totalScore += json_decode(stripslashes($item['scores']))->overall;
                $count++;
            } 
        }
        $avgScore = number_format($totalScore / $count, 2);
       return $avgScore;
    }

    public function customScore($id){
        $scoreCustom = $count = 0; 
        $ts = $this->query($this->requestStart, $this->requestEnd,$this->site, $id)->get();
        if($ts->isEmpty()){
            return 0;
        }
        foreach ($ts as $item) {
            if (isset(json_decode(stripslashes($item['scores']))->advanced)) {
               $scoreCustom += json_decode(stripslashes($item['scores']))->advanced;
               $count++;
            }
        }
        $avgScore = number_format($scoreCustom / $count, 2);
        return $avgScore;
    }


    public function fuelScore($id){
        $fs = $count = 0; 
        $ts = $this->query($this->requestStart, $this->requestEnd,$this->site, $id)->get();
        if($ts->isEmpty()){
            return 0;
        }
        foreach ($ts as $item) {
            if(isset(json_decode(stripslashes($item['scores']))->fuel)){
                $fs += floatval(json_decode(stripslashes($item['scores']))->fuel);
                $count++;
            }
        }
        $avgScore = number_format($fs / $count, 2);
        return $avgScore;
    }


    public function transitScore($id){

        $transitScore = $count = 0; 
        $ts = $this->query($this->requestStart, $this->requestEnd,$this->site, $id)->get();

        if($ts->isEmpty() || $id==5432009351795875573){
            return 0;
        }
        foreach ($ts as $item) {
            if(isset(json_decode(stripslashes($item['scores']))->transits)){
                $transitScore += json_decode(stripslashes($item['scores']))->transits;
                $count++;
            }
        }
        $avgScore = number_format($transitScore / $count, 2);
        return $avgScore;
    }


    public function sendemScore($id){
        $sendemScore = $count = 0;
        $ts = $this->query($this->requestStart, $this->requestEnd,$this->site, $id)->get();
        if($ts->isEmpty()){
            return 0;
        }  
        foreach ($ts as $item) {
            if(isset(json_decode(stripslashes($item['scores']))->sendem)){
                $sendemScore += json_decode(stripslashes($item['scores']))->sendem;
                $count++;
            }
        }
        $avgScore = number_format($sendemScore / $count, 2);
        return $avgScore;
    }

    public function shuntDistance($id){;
        $total= 0;
        $ts = $this->query($this->requestStart, $this->requestEnd,$this->site, $id)->get();
        if($ts->isEmpty()){
            return 0;
        }
        foreach ($ts as $item) {
            $target = isset(json_decode(stripslashes($item['targets']))->distance) ? json_decode(stripslashes($item['targets']))->distance : 0;
            $distance = isset(json_decode(stripslashes($item['distance']))->fm_edit) && json_decode(stripslashes($item['distance']))->fm_edit > 0 ? json_decode(stripslashes($item['distance']))->fm_edit : json_decode(stripslashes($item['distance']))->fm;
            if($distance > $target){
                $shunt = $distance - $target;
                $total += $shunt;
            }
        }  
        return number_format($total,2);
    }

    public function emptyDistance($id){
        $total= 0;      
        $ts = $this->query($this->requestStart, $this->requestEnd,$this->site, $id)->get();
        if($ts->isEmpty()){
            return 0;
        }
        foreach ($ts as $item) {
            $trip = json_decode(stripslashes($item['trip']))->long;
            
            if($trip != 'LOCAL(L)'){
                $trunk = isset(json_decode(stripslashes($item['targets']))->distance) ? json_decode(stripslashes($item['targets']))->distance : 0;
                $distance = isset(json_decode(stripslashes($item['distance']))->fm_edit) && json_decode(stripslashes($item['distance']))->fm_edit > 0 ? json_decode(stripslashes($item['distance']))->fm_edit : json_decode(stripslashes($item['distance']))->fm;
                $shunt = $distance > $trunk ? $distance - $trunk : 0;
                $loadPc = $this->calc_load_percent($id, $trip);
                $total += ($trunk*(100-$loadPc)/100)+($shunt*0.5);
            }  
        }
        return number_format($total,2);
    }


    public function loadDistance($id){
        $total= 0; 
        $ts = $this->query($this->requestStart, $this->requestEnd,$this->site, $id)->get();
        if($ts->isEmpty()){
            return 0;
        }
        foreach ($ts as $item) {
            $trip = json_decode(stripslashes($item['trip']))->long;
            if($trip != 'LOCAL(L)'){
                $trunk = isset(json_decode(stripslashes($item['targets']))->distance) ? json_decode(stripslashes($item['targets']))->distance : 0;
                $distance = isset(json_decode(stripslashes($item['distance']))->fm_edit) && json_decode(stripslashes($item['distance']))->fm_edit > 0 ? json_decode(stripslashes($item['distance']))->fm_edit : json_decode(stripslashes($item['distance']))->fm;
                $shunt = $distance > $trunk ? $distance - $trunk : 0;
                $loadPc = $this->calc_load_percent($id, $trip);
                $total += ($trunk*($loadPc/100))+($shunt*0.5);
            }
        }
        return number_format($total,2);
    }

    
    public function topConsumption($id){
        $ts = $this->query($this->requestStart, $this->requestEnd,$this->site, $id)->get();
        if($ts->isEmpty()){
            return 0;
        }
        foreach ($ts as $item) {
            $distance = isset(json_decode(stripslashes($item['distance']))->fm_edit) && json_decode(stripslashes($item['distance']))->fm_edit > 0 ? json_decode(stripslashes($item['distance']))->fm_edit : json_decode(stripslashes($item['distance']))->fm;
            $fuel = isset(json_decode(stripslashes($item['fuel']))->actual) ? (int)json_decode(stripslashes($item['fuel']))->actual : 0;
            $consumption = $fuel != 0  ? number_format($distance / $fuel, 2) : 0;
            $array[$item->driver] = $consumption;
        }
        arsort($array);
        return array_slice($array, 0, 5);
    }

    public function bottomConsumption($id){
        $ts = $this->query($this->requestStart, $this->requestEnd,$this->site, $id)->get();
        if($ts->isEmpty()){
            return 0;
        }
        foreach ($ts as $item) { 
            $distance = isset(json_decode(stripslashes($item['distance']))->fm_edit) && json_decode(stripslashes($item['distance']))->fm_edit > 0 ? json_decode(stripslashes($item['distance']))->fm_edit : json_decode(stripslashes($item['distance']))->fm;
            $fuel = isset(json_decode(stripslashes($item['fuel']))->actual) ? (int)json_decode(stripslashes($item['fuel']))->actual : 0;
            $consumption = $fuel != 0  ? number_format($distance / $fuel, 2) : 0;
            $array[$item->driver] = $consumption;
        }
        asort($array);
        return array_slice($array, 0, 5);
    }

    public function bottomTransits($id){
        $ts = $this->query($this->requestStart, $this->requestEnd,$this->site, $id)->get();
        if($ts->isEmpty() || $id==5432009351795875573){
            return 0;
        }
        foreach ($ts as $item) { 
            if (isset(json_decode(stripslashes($item['scores']))->transits)) {
                $transits = json_decode(stripslashes($item['scores']))->transits;
            }
            $array[ $item->driver] = number_format($transits,2);
        }

        asort($array);
        return array_slice($array, 0, 5);
    }

    public function topDistance($id){
        $array = array();
        $ts = $this->query($this->requestStart, $this->requestEnd,$this->site, $id)->get();

        if($ts->isEmpty()){
            return 0;
        }
        foreach ($ts as $item) {
            $distance = isset(json_decode(stripslashes($item['distance']))->fm_edit) && json_decode(stripslashes($item['distance']))->fm_edit > 0 ? json_decode(stripslashes($item['distance']))->fm_edit : json_decode(stripslashes($item['distance']))->fm; 
            $array[$item->driver] = array_key_exists($item['driver'],$array) ? $array[$item->driver]+$distance : $distance;
        }
        arsort($array);
        $res = array_slice($array, 0, 5);
        foreach ($res as $key => $value) {
            $res[$key] = number_format($value, 2);
        }
        return $res;
     }
 
    public function monthlyDistanceAverages($id){
        $average = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0);
        $distance = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0);
        $site = $this->site;

        $ts = Trip::select(DB::raw("sum(distancekilometers) as distance, sum(distancekilometers) / count(distinct assetid) as average, to_char(tripstart,'fmMM') as month"))
            ->when($id != 0, function ($query)  use ($id){
                        $query->where('groupid', $id);
                })
            
            ->where('distancekilometers', '>', 0)
            ->when($site != 0, function ($query)  use ($site){
                        $query->whereHas('asset', function ($q) use ($site) {
                        $q->where('siteid', $site);
                    });
                })
            ->whereBetween('tripstart', [Carbon::now()->startOfYear(), Carbon::now()])
            ->groupBy('month')
            ->get();
        foreach($ts as $item){
            $index = $item->month - 1;
            $average[$index] = number_format($item->average, 2, ".", "");
            $distance[$index] = number_format($item->distance, 2, ".", "");
        }
        $average = array_slice($average, 0, date('m'));
        $distance = array_slice($distance, 0, date('m'));
        $obj = new stdClass();
        $obj->{'Monthly Total Distance'} = $distance;
        $obj->{'Monthly Average Distance'} =  $average;
        return $obj;
    }

    public function query($startDate, $endDate, $site, $id){
        return Dashboard::where('status', 'ACTIVE')
                ->whereBetween('end', [$startDate, $endDate])
                ->when($id != 0, function ($query)  use ($id){
                        $query->where('groupid', $id);
                })
                ->when($site != 0, function ($query)  use ($site){
                        $query->whereHas('asset', function ($q) use ($site) {
                        $q->where('siteid', $site);
                    });
                })
                ->select('tripsheet', 'groupid', 'tripsheetid', 'distance', 'scores', 'fuel', 'targets', 'trip', 'driver')
                ->distinct('tripsheet')
                ->orderBy('tripsheet')
                ->orderBy('tripsheetid', 'desc');
    }

    function calc_load_percent($id, $trip){

        $tripArr = explode("-", str_replace("(L)","",str_replace("(E)","",$trip)));
        $arr = [];
        if ($tripArr) {
            $tmp = "";
            foreach($tripArr AS $t){
                if ($tmp == "") {
                    $tmp = rtrim(ltrim($t));
                }
                else{
                    $tripStr = $tmp."-".$t;       
                    array_push($arr, $tripStr);
                    $tmp = rtrim(ltrim($t));
                }
            }
        }
        if (count($arr) > 0) {
            $totDist = 0;
            $totLoad = 0;
            if($id != 0){
                $rows = RouteLeg::where('groupid', $id)
                    ->whereIn('code', $arr)
                    ->get();
            } else {
                $rows = RouteLeg::whereIn('code', $arr)
                    ->get();
            }
            
            $tripArr = explode("-", $trip);
            $tmp 	 = "";
            foreach($tripArr AS $t){
                if ($tmp == "") {
                    $tmp = rtrim(ltrim($t));
                }
                else{
                    $leg = $tmp."-".$t;
                    $tmp = rtrim(ltrim(str_replace("(L)", "", str_replace("(E)", "",$t))));

                    foreach($rows AS $r){
                        if($leg ==$r['code']."(L)") {
                            $totDist += $r['distance'];
                            $totLoad += $r['distance'];
                            continue;
                        }
                        elseif($leg == $r['code']."(E)") {
                            $totDist += $r['distance'];
                            continue;
                        }
                    }
                    
                }
            }
            return $totDist>0 ? (100*$totLoad/$totDist) : 0;
        }

        return "0";
    }

}