<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\GtTripResource;
use App\Models\GtPosition;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GtTripController extends Controller
{
    public function gtTrips(Request $request){ 
        $clientId  = $request->has('unit') ? $request->input('unit') : 0;          
        $start    = $request->input('start', Carbon::now()->startOfMonth());
        $end      = $request->input('end', Carbon::now());

        $ts = GtPosition::select('unit_name') 
            ->selectRaw("MAX(odometer) - MIN(odometer) AS distance")
            ->selectRaw("MAX(odometer) as odometer")
            ->selectRaw("MAX(speed) as speed")
            ->where('client_id', $clientId)
            ->whereBetween('datetime', [$start, $end])
            ->groupBy( 'unit_name')
            ->get();
        return GtTripResource::collection($ts);
     }
}