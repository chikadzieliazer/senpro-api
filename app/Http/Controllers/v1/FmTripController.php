<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\FmTripResource;
use App\Models\Trip;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FmTripController extends Controller
{
    public function fmTrips(Request $request){ 
        $groupId  = $request->has('group') ? $request->input('group') : 0;    
        $siteId   = $request->has('site') ? $request->input('site') : 0;       
        $start    = $request->input('start', Carbon::now()->startOfMonth());
        $end      = $request->input('end', Carbon::now());

        $ts = Trip::select('assetid') 
                ->selectRaw("SUM(distancekilometers) as distance")
                ->selectRaw("MAX(endodometerkilometers) as odometer")
                ->selectRaw("MAX(maxrpm) as RPM")
                ->selectRaw("SUM((drivingtime)::integer) as drivingtime")
                ->selectRaw("SUM((standingtime)::integer) as standing_time")
                ->selectRaw("MAX(maxspeedkilometersperhour) as max_speed")
                ->selectRaw("SUM(fuelusedlitres) as fm_fuel")
                ->with([
                    'asset' => function($q)
                        {   
                            $q->select('assetid', 'description');
                        },
                    ])
                ->whereBetween('tripstart', [$start, $end])
                ->when($groupId != 0, function ($query)  use ($groupId){
                    $query->where('groupid', $groupId);
                    })  
                    ->when($siteId != 0, function ($query)  use ($siteId){
                            $query->whereHas('asset', function ($q) use ($siteId) {
                            $q->where('siteid', $siteId);
                        });
                    })
                    ->whereHas('asset', function ($q)  {
                        $q->where('assettypeid', '!=', 2);
                    })
                ->groupBy('assetid')
                ->get();
            // return $ts;
        return FmTripResource::collection($ts);
     }
}
