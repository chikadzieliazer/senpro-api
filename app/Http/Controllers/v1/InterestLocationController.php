<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\InterestLocationResource;
use App\Models\InterestLocation;
use Illuminate\Http\Request;

class InterestLocationController extends Controller
{
    public function index ($groupId){
        $location = InterestLocation::where('groupid', $groupId)
                        ->where('islocationinterest', true)
                        ->paginate(25);
        return InterestLocationResource::collection($location);
    }
    public function show($id){
        $location = InterestLocation::where('id',$id)
        ->first();

        if (!$location) {
            return response()->json(['success' => false, 'message' => 'Tollgate does not exist.']);
        }
        return (new InterestLocationResource($location))
            ->response()
            ->setStatusCode(200);
    }
}
