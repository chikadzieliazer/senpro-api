<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\TransitTargetRequest;
use App\Http\Resources\v1\TransitTargetResource;
use App\Models\TransitTarget;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TransitTargetController extends Controller
{

    public function group($id)
    {
        $customer = TransitTarget::where('groupid', $id)->get();
        return (TransitTargetResource::collection($customer))
            ->response()
            ->setStatusCode(200);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transitTarget = TransitTarget::paginate();
        return TransitTargetResource::collection($transitTarget);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransitTargetRequest $request)
    {
        $transitTarget = new TransitTarget;
        $transitTarget->groupid = $request->groupid;
        $transitTarget->position_a = $request->position_a;
        $transitTarget->position_b = $request->position_b;
        $transitTarget->distance = $request->distance;
        $transitTarget->duration = $request->duration;
        $transitTarget->created = Carbon::now()->toDateTimeString();
        $transitTarget->save();
        return (new TransitTargetResource($transitTarget))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(TransitTarget $transitTarget)
    {
        return new TransitTargetResource($transitTarget);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transitTarget = TransitTarget::findOrFail($id);
        $transitTarget->update($request->all());
        return (new TransitTargetResource($transitTarget))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transitTarget = TransitTarget::findOrFail($id);
        $transitTarget->delete();
        return (new TransitTargetResource($transitTarget))
            ->response()
            ->setStatusCode(201);
    }
}
