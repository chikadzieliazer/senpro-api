<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\SubgroupResource;
use App\Models\Subgroup;
use Illuminate\Http\Request;

class SubgroupController extends Controller
{

    public function group($id)
    {
        $subgroup = Subgroup::where('groupid',$id)->get();
        return (SubgroupResource::collection($subgroup))
            ->response()
            ->setStatusCode(200);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $product = Subgroup::paginate();
        return SubgroupResource::collection($product);
    }


    

    public function store(Request $request)
    {
        $subgroup = new Subgroup;
        $subgroup->groupid = $request->groupid;
        $subgroup->parentid = $request->parentid;
        $subgroup->name = $request->name;
        $subgroup->type = $request->type;
        $subgroup->save();
        return (new SubgroupResource($subgroup))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Subgroup $subgroup)
    {
        return new SubgroupResource($subgroup);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Subgroup::findOrFail($id);
        $product->update($request->all());
        return (new SubgroupResource($product))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subgroup = Subgroup::findOrFail($id);
        $subgroup->delete();
        return (new SubgroupResource($subgroup))
            ->response(null)
            ->setStatusCode(204);
    }

}
