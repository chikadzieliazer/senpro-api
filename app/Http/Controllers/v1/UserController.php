<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\UserLoginRequest;
use App\Http\Requests\v1\UserRegisterRequest;
use App\Http\Resources\v1\UserResource;
use App\Models\Config;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        return UserResource::collection(
            User::where('status', 'ACTIVE')
                ->with(['fmgroup', 'gtgroup', 'activity'])
                ->get()
        );
    }

    public function login(UserLoginRequest $request)
    {
        $user = User::where('username', $request->username)->first();
        if(isset($user)){
            if($user->password == md5($request->password)){
                $user->password =  bcrypt($request->password);
                $user->save();
            }
        }
        $credentials = $request->only(['username', 'password']);
        if (!auth()->attempt($credentials)) {
            return response()->json(['Error' => 'Incorrect Details. Please try again'], 401);
        }
        $tokenData = auth()->user()->createToken('MyApiToken');
        $token = $tokenData->accessToken;
        $expiration = $tokenData->token->expires_at->diffInSeconds(Carbon::now());

        return response()->json([   
            'Access Token' => $token,
            'Token Type' => 'Bearer',
            'Expires In' => $expiration
        ], 201);

    }

    public function internalLogin(UserLoginRequest $request)
    {
        $user = User::where('username', $request->username)->first();
        if(isset($user)){
            if($user->password == md5($request->password)){
                $user->password =  bcrypt($request->password);
                $user->save();
            }
        }
        $credentials = $request->only(['username', 'password']);
        if (!auth()->attempt($credentials)) {
            return response()->json(['Error' => 'Incorrect Details. Please try again'], 401);
        }
        $tokenData = auth()->user()->createToken('MyApiToken');
        $token = $tokenData->accessToken;
        $expiration = $tokenData->token->expires_at->diffInSeconds(Carbon::now());
        $user = auth()->user();

        $configResults = Config::where('groupid', $user->fm)->first();

        return response()->json([   
            'User' => $user,
            'Config' => $configResults,
            'Access Token' => $token,
            'Token Type' => 'Bearer',
            'Expires In' => $expiration
        ], 201);

    }

    public function store(Request $request)
    {
        try {
            $date = Carbon::now()->toDateTimeString();
            $pass = bcrypt("sendempass");
            $user = new User;
            $user->name = $request->name;
            $user->username = $request->username;
            $user->password = $pass;
            $user->type = $request->type;
            $user->email = $request->email;
            $user->notifylist = $request->notifylist;
            $user->status = "ACTIVE";
            $user->fm = $request->fm;
            $user->gt = $request->gt;
            $user->department = $request->department;
            $user->disableload = $request->disableload;
            $user->created = $date;
            $user->save();
            return response()->json([
                'message' => 'User Added.',
                "user" => $user
            ], 201);
        } catch (\Throwable $th) {
            return response($th->getMessage(), 400);
        }
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        if (!$user) {
            return response()->json(['success' => false, 'message' => 'User does not exist']);
        }
    
        return response()->json($user, 200);
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        if($request->has('password')){
            if($request->has('oldpwd') && $request->input('oldpwd') == 'sendempass'){
                $data = array('password'=>bcrypt($request->input('password')));
                $user->update($data);
                return response()->json([
                    'success' => true
                ]);   
            } else if($request->has('oldpwd') && $request->input('oldpwd') != 'sendempass'){
                return response()->json([
                    'success' => false
                ]); 
            }
            $data = array('password'=>bcrypt('sendempass'));
            
        } else {
            $data = $request->all();
        }
        $user->update($data);
        return response()->json([
            "user" => $user
        ], 201);

    }

     public function destroy(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->status = 'DELETED';
        $user->save();
        return response()->json([
            'message' => 'User Deleted.'
        ], 200);
    }

    public function updatePassword(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->password = bcrypt("sendempass");
        $user->save();
        return response()->json([
            'message' => 'Password Reset.'
        ], 200);
    }

    public function logout()
    {
        $token = auth()->user()->token();

        $token->revoke();
        $token->delete();

        return response()->json([
            'message' => 'Logged out successfully'
        ], 200);
    }
}
