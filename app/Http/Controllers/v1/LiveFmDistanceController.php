<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\LiveFMDistanceMonthlyResource;
use App\Http\Resources\v1\LiveFMDistanceResource;
use App\Models\Trip;
use Carbon\Carbon;

class LiveFmDistanceController extends Controller
{
    public function index($id, $site){
        
    $ts = Trip::with('asset:assetid,assettypeid,siteid')
        ->when($id != 0, function ($query) use ($id) {
            $query->where('groupid', $id);
        })
        ->whereBetween('tripstart', [Carbon::now()->startOfMonth(), Carbon::now()])
        ->whereHas('asset', function ($q) {
            $q->where('assettypeid', '!=', 2);
        })
        ->when($site != 0, function ($query) use ($site) {
            $query->whereHas('asset', function ($q) use ($site) {
                $q->where('siteid', $site);
            });
        })
        ->sum('distancekilometers');
        return new LiveFMDistanceResource($ts);
    }
    public function monthlyDistance($id, $site){
        $ts = Trip::selectRaw("SUM(distancekilometers) as distance")
            ->selectRaw("TO_CHAR(tripstart, 'Mon') as month")
            ->when($id != 0, function ($query)  use ($id){
                        $query->where('groupid', $id);
                })
            ->where('distancekilometers', '>', 0)
            ->when($site != 0, function ($query)  use ($site){
                        $query->whereHas('asset', function ($q) use ($site) {
                        $q->where('siteid', $site);
                    });
                })
            ->whereBetween('tripstart', [Carbon::now()->subMonths(11)->startOfMonth(), Carbon::now()->endOfMonth()])
            ->groupBy('month')
            ->orderByRaw('MIN(tripstart)')
            ->paginate(25);
        return LiveFMDistanceMonthlyResource::collection($ts);
    }
    public function monthlyAverage($id, $site){
        $ts = Trip::selectRaw('SUM(distancekilometers) / COUNT(DISTINCT assetid) as distance')
            ->selectRaw("TO_CHAR(tripstart, 'Mon') as month")
            ->when($id != 0, function ($query)  use ($id){
                        $query->where('groupid', $id);
                })
            ->where('distancekilometers', '>', 0)
            ->when($site != 0, function ($query)  use ($site){
                        $query->whereHas('asset', function ($q) use ($site) {
                        $q->where('siteid', $site);
                    });
                })
            ->whereBetween('tripstart', [Carbon::now()->subMonths(11)->startOfMonth(), Carbon::now()->endOfMonth()])
            ->groupBy('month')
            ->orderByRaw('MIN(tripstart)')
            ->paginate(25);
        return LiveFMDistanceMonthlyResource::collection($ts);
    }
}