<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\UnitGtResource;
use App\Models\UnitGt;
use Illuminate\Http\Request;

class UnitGtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unit = UnitGt::paginate(10);
        return UnitGtResource::collection($unit);
    }

    public function show(UnitGt $unitgt)
    {
        return new UnitGtResource($unitgt);
    }

}
