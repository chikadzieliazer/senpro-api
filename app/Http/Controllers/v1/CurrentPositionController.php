<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\CurrentPositionResource;
use App\Models\CurrentPosition;
use App\Models\CurrentPositionGt;
use Illuminate\Http\Request;

class CurrentPositionController extends Controller
{
    public function group($id)
    {
        $positions = CurrentPosition::select('tblassetposition_curr.*', 'tblasset.description', 'tblasset.registrationnumber', 'tblasset.gt')
            ->join('tblasset', 'tblasset.assetid', '=', 'tblassetposition_curr.assetid')
            ->where('tblassetposition_curr.groupid', $id)
            ->get();
        $gtPositions = CurrentPositionGt::where('fm_id', $id)
            ->whereNotNull('asset_id')
            ->get();

        $latestPositions = [];
        foreach ($positions as $position) {
            foreach ($gtPositions as $gtPosition) {
                if ($position->gt == $gtPosition->asset_id) {
                    if ($position->lastupdate >= $gtPosition->lastupdate) {
                        $latestPositions[] = $position;
                    } else {
                        $latestPositions[] = $gtPosition;
                    }
                }
            }
        }
        // return response()->json($latestPositions);
        return (CurrentPositionResource::collection($latestPositions))
            ->response()
            ->setStatusCode(200);
    }   
}