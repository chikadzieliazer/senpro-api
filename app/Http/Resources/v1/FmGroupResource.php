<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FmGroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => 'Fm Group',
            'id' => $this->groupid,
            'attributes' => [
                'name' => $this->name,
                'type' => $this->type,
                'displaytimezone' => $this->displaytimezone,
            ],
            'relationship' => [
                'assets' => $this->assets,
                'customers' => $this->customers,
                'products' => $this->products,
            ],
            'links' => [
                'self' => route('fmgroups.show', $this->groupid)
            ]
        ];
    }
}
