<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class RouteLegResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => 'Route Legs',
            'id' => $this->id,
            'attributes' => [
                'leg' => $this->leg,
                'code' => $this->code,
                'distance' => $this->distance,
                'duration_empty' => $this->duration_empty,
                'duration_loaded' => $this->duration_loaded,
                'created' => $this->created,
            ],
            'relationship' => [
                'group' => $this->fmGroup,
            ],
            'links' => [
                'self' => route('routelegs.show', $this->legid)
            ]
        ];
    }
    public function with($request)
    {
        return [
            'status' => 'success',
            'version' => '1.0.0'
        ];

    }

    public function withResponse($request, $response)
    {
        $response->header('Accept', 'application/json');
    }
}
