<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class FmTripResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => 'FM Asset Trip',
            'id' => $this->assetid,
            'attributes' => [
                'asset name' => $this->asset->description,
                'distance' => number_format($this->distance, 2),
                'odometer' => number_format($this->odometer,2),
                'rpm' => $this->rpm,
                'driving time' => $this->seconds_to_time_min($this->drivingtime),
                'standing time' => $this->seconds_to_time_min($this->standing_time),
                'max speed' => $this->max_speed,
                'fm fuel' => number_format($this->fm_fuel, 2),
            ]
        ];
    }

    function seconds_to_time_min($seconds){
        $neg   = "";
        if ($seconds<0) {
            $neg 	 = "-";
            $seconds = $seconds*-1;
        }
        $hours = floor($seconds / 3600);
        $mins  = floor($seconds / 60 % 60);
        $secs  = floor($seconds % 60);
        
        // $days  = floor($hours/24);
        // $hours = ($hours%24);
    
        if($hours<10){ $hours = "0". $hours; }
        if($mins<10){  $mins  = "0". $mins;   }
        if($secs<10){  $secs  = "0". $secs;   }
    
        // if($days>0){
        //  	return $neg . $days ."d, $hours:$mins:$secs";
        // }
            
        return $neg ."$hours:$mins:$secs";
    }
}
