<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class DriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => 'Driver FM',
            'id' => $this->driverid,
            'attributes' => [
                'siteid' => $this->siteid,
                'name' => $this->name,
                'imageuri' => $this->imageuri,
                'fmdriverid' => $this->fmdriverid,
                'employeenumber' => $this->employeenumber,
                'issystemdriver' => $this->issystemdriver,
                'mobilenumber' => $this->mobilenumber,
                'email' => $this->email,
                'extendeddriverid' => $this->extendeddriverid,
                'extendeddriveridtype' => $this->extendeddriveridtype,
                'country' => $this->country,
            ],
            'relationship' => [
                'group' => $this->fmGroup,
            ],
            'links' => [
                'self' => route('fmdrivers.show', $this->driverid)
            ]
        ];
    }
}
