<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class PositionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => 'Gt Position',
            'attributes' => [
                'unit_name' => $this->unit_name,
                'account_name' => $this->account_name,
                'datetime' => $this->datetime,
                'hws' => $this->hws,
                'altitude' => $this->altitude,
                'speed' => $this->speed,
                'direction' => $this->direction,
                'latitude' => $this->latitude,
                'longitude' => $this->longitude,
                'position_text' => $this->position_text,
                'source' => $this->source,
                'odometer' => $this->odometer,
                'speed_limit' => $this->speedlimit,
                'distance' => $this->distance,
                'event' => $this->event,
            ],
            'links' => [
                'self' => route('position.show', $this->position_id)
            ]
        ];
    }
    public function with($request)
    {
        return [
            'status' => 'success',
            'version' => '1.0.0'
        ];

    }

    public function withResponse($request, $response)
    {
        $response->header('Accept', 'application/json');
    }
}

