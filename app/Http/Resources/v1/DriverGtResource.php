<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class DriverGtResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => 'Driver GT',
            'id' => $this->driverid,
            'attributes' => [
                'clientid' => $this->siteid,
                'name' => $this->name,
                'employeenumber' => $this->employeenumber,
                'country' => $this->country,
            ],
            'relationship' => [
                'group' => $this->gtGroup,
            ],
            'links' => [
                'self' => route('gtdrivers.show', $this->client_id)
            ]
        ];
    }
}
