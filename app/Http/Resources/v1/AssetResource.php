<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class AssetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */

       public function toArray($request)
    {
        return [
            'type' => 'asset',
            'assetid' => $this->assetid,
            'attributes' => [
                'groupid' => $this->groupid,
                'assettypeid' => $this->assettypeid,
                'description' => $this->description,
                'isconnectedtrailer' => $this->isconnectedtrailer,
                'registrationnumber' => $this->registrationnumber,
                'siteid' => $this->siteid,
                'targetfuelconsumption' => $this->targetfuelconsumption,
                'targetfuelconsumptionunits' => $this->targetfuelconsumptionunits,
                'targethourlyfuelconsumption' => $this->targethourlyfuelconsumption,
                'targethourlyfuelconsumptionunits' => $this->targethourlyfuelconsumptionunits,
                'fleetnumber' => $this->fleetnumber,
                'make' => $this->make,
                'model' => $this->model,
                'year' => $this->year,
                'vinnumber' => $this->vinnumber,
                'enginenumber' => $this->enginenumber,
                'fmvehicleid' => $this->fmvehicleid,
                'additionalmobiledevice' => $this->additionalmobiledevice,
                'notes' => $this->notes,
                'icon' => $this->icon,
                'iconcolour' => $this->iconcolour,
                'colour' => $this->colour,
                'assetimage' => $this->assetimage,
                'isdefaultimage' => $this->isdefaultimage,
                'assetimageurl' => $this->assetimageurl,
                'userstate' => $this->userstate,
                'createdby' => $this->createdby,
                'createddate' => $this->createddate,
                'odometer' => $this->odometer,
                'enginehours' => $this->enginehours,
                'country' => $this->country,
                'emptyconsumption' => $this->emptyconsumption,
                'loadconsumption' => $this->loadconsumption,
            ]
        ];
    }

    public function with($request)
    {
        return [
            'status' => 'success',
            'version' => '1.0.0'
        ];

    }

    public function withResponse($request, $response)
    {
        $response->header('Accept', 'application/json');
    }
}
