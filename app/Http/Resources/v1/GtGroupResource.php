<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class GtGroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->client_id,
            'attributes' => [
                'account_id' => $this->account_id,
                'fm_groupid' => $this->fm_groupid,
                'client_account' => $this->client_account,
                'account_name' => $this->account_name,
                'fm_name' => $this->fm_name,
            ],
            'links' => [
                'self' => route('gtclients.show', $this->client_id)
            ]
        ];
    }
}
