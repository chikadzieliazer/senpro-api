<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class SubgroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => 'Subgroup',
            'id' => $this->subgroupid,
            'attributes' => [
                'parentid' => $this->parentid,
                'name' => $this->name,
                'type' => $this->type,
            ],
            'relationship' => [
            'group' => $this->fmGroup,
            ],
            'links' => [
                'self' => route('subgroups.show', $this->subgroupid)
            ]
        ];
    }

    public function with($request)
    {
        return [
            'status' => 'success',
            'version' => '1.0.0'
        ];

    }

    public function withResponse($request, $response)
    {
        $response->header('Accept', 'application/json');
    }
}
