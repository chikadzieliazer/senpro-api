<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class GtTripResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
           return [
            'type' => 'GT Positions',
            'id' => $this->unit_id,
            'attributes' => [   
                'unit_name' => $this->unit_name ? $this->unit_name : $this->unit['unit_name'],
                'distance' => number_format(($this->distance)/1000, 2),
                'max_odometer' => number_format($this->odometer/1000,2),
                'max_speed' => $this->speed,
            ]
        ];
    }

    public function with($request)
    {
        return [
            'status' => 'success',
            'version' => '1.0.0'
        ];

    }

    public function withResponse($request, $response)
    {
        $response->header('Accept', 'application/json');
    }
}
