<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class FmPositionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => 'FM Position',
            'id' => $this->positionid,
            'attributes' => [
                'position_source_id' => $this->positionsourceid,
                'driver_id' => $this->driverid,
                'asset_id' => $this->assetid,
                'timestamp' => $this->timestamp,
                'latitude' => $this->latitude,
                'longitude' => $this->longitude,
                'speed_kilometres_per_hour' => $this->speedkilometresperhour,
                'altitude_metres' => $this->altitudemetres,
                'heading' => $this->heading,
                'number_of_satelites' => $this->numberofsatelites,
                'hdop' => $this->hdop,
                'vdop' => $this->vdop,
                'pdop' => $this->pdop,
                'age_of_reading_seconds' => $this->ageofreadingseconds,
                'distances_ince_reading_kilometres' => $this->distancesincereadingkilometres,
                'odometer_kilometres' => $this->odometerkilometres,
                'source' => $this->src,
                'is_available' => $this->isavl,
                'speed_limit' => $this->speedlimit,
                'longlat' => $this->longlat,
            ],
            'links' => [
                'self' => route('fmPosition.show', $this->positionid)
            ]
        ];
    }
    public function with($request)
    {
        return [
            'status' => 'success',
            'version' => '1.0.0'
        ];

    }

    public function withResponse($request, $response)
    {
        $response->header('Accept', 'application/json');
    }
}
