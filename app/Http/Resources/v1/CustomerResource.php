<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => 'customer',
            'id' => $this->id,
            'attributes' => [
                'name' => $this->name,
                'descrption' => $this->description,
                'created' => $this->created,
            ],
            'relationship' => [
            'group' => $this->fmGroup,
            ],
            'links' => [
                'self' => route('customers.show', $this->id)
            ]
        ];
    }

    public function with($request)
    {
        return [
            'status' => 'success',
            'version' => '1.0.0'
        ];

    }

    public function withResponse($request, $response)
    {
        $response->header('Accept', 'application/json');
    }
}


