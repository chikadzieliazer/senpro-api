<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class TollgateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => 'Tollgate',
            'attributes' => [
                'name' => $this['name'],
                'description' => $this['description'],
                'registrationnumber' => $this['registrationnumber'],
                'timestamp' => $this['timestamp'],
                'speedkilometresperhour' => $this['speedkilometresperhour'],
                'odometerkilometres' => $this['odometerkilometres'],
                'amount' => $this['amount'],
                'currency' => $this['currency'],
                'distance_meters' => $this['distance_meters'],
                'longlat' => $this['longlat'],
                'dateIn' => $this['dateIn'],
                'dateOut' => $this['dateOut'],
            ]
        ];
    }
    
    public function with($request)
    {
        return [
            'status' => 'success',
            'version' => '1.0.0'
        ];

    }
    public function withResponse($request, $response)
    {
        $response->header('Accept', 'application/json');
    }
}
