<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class TransitTargetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => 'Transit target',
            'id' => $this->id,
            'attributes' => [
                'position_a' => $this->position_a,
                'position_b' => $this->position_b,
                'distance' => $this->distance,
                'duration' => $this->duration,
                'units' => $this->units,
                'created' => $this->created,
            ],
            'relationship' => [
                'group' => $this->fmGroup,
            ],
            'links' => [
                'self' => route('transittargets.show', $this->id)
            ]
        ];
    }

    public function with($request)
    {
        return [
            'status' => 'success',
            'version' => '1.0.0'
        ];

    }

    public function withResponse($request, $response)
    {
        $response->header('Accept', 'application/json');
    }
}
