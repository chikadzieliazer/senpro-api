<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class JobcardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => 'Jobcard',
            'id' => $this->id,
            'attributes' => [
                'technicianid' => $this->technicianid,
                'unittype' => $this->unittype,
                'serial' => $this->serial,
                'fault' => $this->fault,
                'work' => $this->work,
                'comments' => $this->comments,
                'jobcard' => $this->jobcard,
                'travel' => $this->travel,
                'labour' => $this->labour,
                'parts' => $this->parts,
                'created' => $this->created,
            ],
            'relationship' => [
                'users' => $this->users,
                'group' => $this->fmGroups,
                'asset' => $this->assets,
            ],
            'links' => [
                'self' => route('jobcards.show', $this->id)
            ]
        ];
    }
}
