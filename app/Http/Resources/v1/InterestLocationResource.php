<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class InterestLocationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => 'Interest Location',
            'id' => $this->id,
            'attributes' => [
                'name' => $this->name,
                'address' => $this->address,
                'location_type' => $this->locationtype,
                'contact_name' => $this->contactname,
                'home_phone' => $this->homephone,
                'mobile_phone' => $this->mobilephone,
                'work_phone' => $this->workphone,
                'email' => $this->email,
                'currency' => $this->currency,
                'amount' => $this->amount,
            ],
            'links' => [
                'self' => route('interest-location.show', $this->id)
            ]
        ];
    }

    public function with($request)
    {
        return [
            'status' => 'success',
            'version' => '1.0.0'
        ];

    }
    public function withResponse($request, $response)
    {
        $response->header('Accept', 'application/json');
    }
}
