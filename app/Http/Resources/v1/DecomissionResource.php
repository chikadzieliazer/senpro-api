<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class DecomissionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => 'Decomission',
            'id' => $this->id,
            'attributes' => [
                'groupname' => $this->groupname,
                'username' => $this->username,
                'group type' => $this->grouptype,
                'asset' => $this->asset,
                'peripherals' => $this->peripherals,
                'termination number' => $this->terminationnumber,
                'ticket URL' => $this->ticketurl,
                'decomission document' => $this->decomdocument,
                'decomission comment' => $this->decomcomment,
                'created' => $this->created,
            ],
            'links' => [
                'self' => route('decomissions.show', $this->id)
            ]
        ];
    }
}
