<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class TripsheetDistanceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'shunt_distance' => number_format($this->resource['shuntDistance'], 2),
            'empty_distance' => number_format($this->resource['emptyDistance'], 2),
            'load_distance' => number_format($this->resource['loadDistance'], 2),
        ];
    }

    public function with($request)
    {
        return [
            'status' => 'success',
            'version' => '1.0.0'
        ];

    }

    public function withResponse($request, $response)
    {
        $response->header('Accept', 'application/json');
    }
}