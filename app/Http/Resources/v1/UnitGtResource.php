<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class UnitGtResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => 'GT Units',
            'id' => $this->unit_id,
            'attributes' => [
                'fm_groupid' => $this->fm_groupid,
                'fm_assetid' => $this->fm_assetid,
                'unit_name' => $this->unit_name,
                'fm_name' => $this->fm_name,
                'fm_description' => $this->fm_description,
                'targetemptyconsumption' => $this->targetemptyconsumption,
                'targetloadedconsumption' => $this->targetloadedconsumption,
                'asset_id' => $this->asset_id,
            ],
            'relationships' => [
                'group' => $this->gtGroup,
            ],
            'links' => [
                'self' => route('gtunits.show', $this->unit_id)
            ]
        ];
    }
}
