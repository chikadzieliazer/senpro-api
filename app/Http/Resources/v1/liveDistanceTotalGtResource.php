<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class liveDistanceTotalGtResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        print_r($this->collection);
        return [
            'type' => 'Total Gt Asset Trip',
            'attributes' => [
                'distance' => number_format($this->distance, 2),
                'average speed' => number_format($this->avg_speed, 2)
            ]
        ];
    }
}
