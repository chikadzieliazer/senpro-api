<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class CurrentPositionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => $this->groupid ? 'fmposition' : 'gtposition',
            'positionid' => $this->positionid ?? $this->position_id,
            'attributes' => [
                'groupid' => $this->groupid ?? $this->fm_id,
                "driverid" => $this->driverid,
                'assetid' => $this->assetid ?? $this->asset_id,
                'description' => $this->description ? $this->description.' - '.$this->registrationnumber : $this->unit_name,
                'latitude' => $this->latitude,
                'longitude' => $this->longitude,
                'firstupdate' => $this->firstupdate,
                'lastupdate' => $this->lastupdate,
                'odometer' => $this->odometer ?? $this->odometerkilometres,

            ],
        ];
    }
    public function with($request)
    {
        return [
            'status' => 'success',
            'version' => '1.0.0'
        ];

    }

    public function withResponse($request, $response)
    {
        $response->header('Accept', 'application/json');
    }
}
