<?php

namespace App\Http\Resources\v1;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // Convert the string date to a Carbon instance
        $carbonDate = Carbon::parse($this->notification_date);

        // Get the difference between the current date and the date in various intervals
        $minutesDiff = $carbonDate->diffInMinutes(Carbon::now());
        $hoursDiff = $carbonDate->diffInHours(Carbon::now());
        $daysDiff = $carbonDate->diffInDays(Carbon::now());
        $monthsDiff = $carbonDate->diffInMonths(Carbon::now());

        // Format the date accordingly
        if ($minutesDiff < 60) {
            // Less than 1 hour ago
            $formattedDate = $minutesDiff . " min ago";
        } elseif ($hoursDiff < 24) {
            // Less than 1 day ago
            $formattedDate = $hoursDiff . " hours ago";
        } elseif ($daysDiff < 30) {
            // Less than 30 days ago
            $formattedDate = $daysDiff . " days ago";
        } elseif ($monthsDiff < 12) {
            // Less than 12 months (1 year) ago
            $formattedDate = $monthsDiff . " months ago";
        } else {
            // Default to the full date if it's older than 1 year
            $formattedDate = $carbonDate->toFormattedDateString();
        }

        return [
            'message' => $this->message,
            'notification_date' => $formattedDate,
        ];
    }
    public function with($request)
    {
        return [
            'status' => 'success',
            'version' => '1.0.0'
        ];

    }

    public function withResponse($request, $response)
    {
        $response->header('Accept', 'application/json');
    }
       
}
