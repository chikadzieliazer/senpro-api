<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class TripResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => 'Trip',
            'attributes' => [
                'driver' => $this->driver['name'],
                'asset' => $this->asset['description'],
                'trip_start' => $this->tripstart,
                'trip_end' => $this->tripend,
                'first_depart' => $this->firstdepart,
                'first_depart' => $this->firstdepart,
                'last_halt' => $this->lasthalt,
                'driving_time' => $this->drivingtime,
                'standing_time' => $this->standingtime,
                'duration' => $this->duration,
                'distance_kilometers' => $this->distancekilometers,
                'start_odometer_kilometers' => $this->startodometerkilometers,
                'end_odometer_kilometers' => $this->endodometerkilometers,
                'fuel_used_litres' => $this->fuelusedlitres,
                'max_speed_kilometers_per_hour' => $this->maxspeedkilometersperhour,
                'max_acceleration_kilometers_per_hour_per_second' => $this->maxaccelerationkilometersperhourpersecond,
                'max_deceleration_kilometers_per_hour_per_second' => $this->maxdecelerationkilometersperhourpersecond,
                'max_rpm' => $this->maxrpm,
            ],
            'links' => [
                'self' => route('trip.show', $this->tripid)
            ]
        ];
    }
    public function with($request)
    {
        return [
            'status' => 'success',
            'version' => '1.0.0'
        ];

    }

    public function withResponse($request, $response)
    {
        $response->header('Accept', 'application/json');
    }
}
