<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class DepotResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => 'Depot',
            'id' => $this->id,
            'attributes' => [
                'name' => $this->name,
                'city' => $this->city,
                'type' => $this->type,
                'lat' => $this->lat,
                'lng' => $this->lng,
                'created' => $this->created,
            ],
            'relationship' => [
                'group' => $this->fmGroup,
            ],
            'links' => [
                'self' => route('depots.show', $this->id)
            ]
        ];
    }
    public function with($request)
    {
        return [
            'status' => 'success',
            'version' => '1.0.0'
        ];

    }

    public function withResponse($request, $response)
    {
        $response->header('Accept', 'application/json');
    }
}
