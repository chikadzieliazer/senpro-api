<?php

namespace App\Http\Resources\v2;

use Illuminate\Http\Resources\Json\JsonResource;

class TripsheetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */

     public function toArray($request)
     {
         $tripLong = json_decode(stripslashes($this->trip), true);
         $distance = $this->distance ? json_decode(stripslashes($this->distance), true): array("actual"=>0,"empty"=>0,"fm"=>0,"fm_edit"=>0,"gt"=>0,"gt_edit"=>0, "shunt"=>0, "volvo_connect"=>0);
         $fuel = $this->fuel ? json_decode(stripslashes($this->fuel), true) : array("actual"=>0,"probe"=>0,"fm"=>0,"gt"=>0, "volvo_connect"=>0);
         $duration = $this->duration ? json_decode(stripslashes($this->duration), true) : array("actual"=>0,"fm"=>0,"fm_driving"=>0,"gt"=>0,"transit"=>0);
         $targets = $this->targets ? json_decode(stripslashes($this->targets), true) : array("distance"=>0,"fuel"=>0,"duration"=>0,"consumption"=>0);
         $exceptions = json_decode(stripslashes($this->exceptions), true);
         $scores = $this->scores ? json_decode(stripslashes($this->scores), true) : array("transits"=>0,"advanced"=>0, "green"=>0, "sendem"=>0, "fuel"=>0, "overall"=>0, "overall_edit"=>0,
         "speed"=>0,"revv"=>0,"brake"=>0,"accl"=>0,"idle"=>0);
         $transits = json_decode(stripslashes($this->transits), true);
         $service = json_decode(stripslashes($this->service), true);
         $axisevents = json_decode(stripslashes($this->axisevents), true);
         $trailers = json_decode(stripslashes($this->trailers), true); 
         $tsCons = $fuel['actual']>0 ? $distance['actual']/$fuel['actual'] : 0;
         $fmCons = isset($distance['fm']) && $fuel['fm']>0 ? $distance['fm']/$fuel['fm'] : 0;
 
 
         // Calculate Distance Variance 
         $targetDist  = 0;
         $dynamicDist = isset($distance['fm_edit']) && $distance['fm_edit'] > 0 ? $distance['fm_edit'] :(isset($distance['fm']) ? $distance['fm']: 0);
         if ($this->groupid==-3273034392599683215 && $distance['fm']!=$distance['fm_edit'] && $distance['fm_edit']>0) { 
             $targetDist = $dynamicDist;
         } else {
             $targetDist = isset($targets['distance'] ) ? $targets['distance'] : 0;
         }
         if($this->groupid == 1853956352811964940){
             $distVar = $distance['actual']-$dynamicDist;
         }else{
             $distVar = $dynamicDist-$targetDist; 
         }
 
         // Calculate Fuel Variance 
         if($this->groupid == 1853956352811964940 || $this->groupid == 1425761779014311329){
             $showProbeFuel = true;
         } else {
             $showProbeFuel = false;
         }
 
         if ($showProbeFuel && isset($fuel['probe']) && $fuel['probe']>0) {
             $fuelVar = number_format($fuel['probe']-$fuel['actual'],1);
         }
         elseif($this->groupid == -5511162494231442644){
             $fuelVar = number_format($targets['fuel']-$fuel['actual'],1);
         } 
         else{
             if ($this->groupid != 1853956352811964940) {
                 $fuelVar = number_format($targets['fuel']-$fuel['actual'],1);
             }
             else{
                 $fuelVar = number_format($fuel['fm']-$fuel['actual'],1);
             }
         }
 
         return [
             'tripsheet_name' => $this->tripsheet,
             'tripsheet_id' => $this->tripsheetid,
             'attributes' => [
                 'created' => $this->created,
                 'asset' => $this->asset,
                 'driver' => $this->driver,
                 'asset' => $this->asset,
                 'start' => $this->start,
                 'end' => $this->end,
                 'user' => $this->name,
                 'approved' => $this->approved,
                 'version' => $this->version,
                 'status' => $this->status,
                 'verification_date' => $this->verificationdate,
                 'verification_user' => $this->verificationuser,
                 'type' => $this->type,
                'trip' => [
                     'long' => $tripLong['long'],
                     'short' => $tripLong['short'],
                 ],
                 'distance' => [
                     'actual' => $distance['actual'],
                     'empty' => isset($distance['empty']) ?  $distance['empty'] : 0,
                     'fm' => isset($distance['fm']) ? $distance['fm']: 0,
                     'fm_edit' => isset($distance['fm_edit']) ? $distance['fm_edit'] : 0,
                     'variance' => $distVar,
                     'gt' => isset($distance['gt']) ? $distance['gt']: 0,
                     'gt_edit' => isset($distance['gt_edit']) ? $distance['gt_edit'] : 0,
                 ],
                 'fuel' => [
                     'actual' => $fuel['actual'],
                     'fm' => $fuel['fm'],
                     'variance' =>  $fuelVar,
                     'gt' => isset($fuel['gt']) ? $fuel['gt'] : 0,
                 ],
                 'duration' => [
                     'actual' => $duration['actual'],
                     'fm' => $duration['fm'],
                     'fm_driving' => $duration['fm_driving'],
                     'gt' => $duration['gt'],
                     'transit' => $duration['transit'],
                 ],
                 'consumption' => [
                     'tripsheet' => $tsCons,
                     'fm' => $fmCons,
                     'target' => isset($targets['consumption'] ) ? $targets['consumption'] : 0,
                 ],
                 'targets' => [
                     'distance' => isset($targets['distance'] ) ? $targets['distance'] : 0,
                     'fuel' => isset($targets['fuel'] ) ? $targets['fuel'] : 0,
                     'duration' => isset($targets['duration'] ) ? $targets['duration'] : 0,
                 ],
                 'scores' => [
                     "transits"=> isset($scores["transits"]) ? $scores["transits"]  : 0,
                     "advanced"=> isset($scores["advanced"]) ? $scores["advanced"]  : 0,
                     "green"=> 100 - (isset($scores["green"]) ? $scores["green"] : 0),
                     "sendem"=> isset($scores["sendem"]) ? $scores["sendem"]  : 0,
                     "fuel"=> isset($scores["fuel"]) ? $scores["fuel"]  : 0,
                     "overall"=> isset($scores["overall"]) ? $scores["overall"]  : 0,
                     "overall_edit"=> isset($scores["overall_edit"]) ? $scores["overall_edit"]  : 0,
                     "speed"=> isset($scores["speed"]) ? $scores["speed"]  : 0,
                     "revv"=> isset($scores["revv"]) ? $scores["revv"]  : 0, 
                     "brake"=> isset($scores["brake"]) ? $scores["brake"]  : 0,
                     "accl"=> isset($scores["accl"]) ? $scores["accl"]  : 0, 
                     "idle"=> isset($scores["idle"]) ? $scores["idle"]  : 0, 
                 ],
                 'exceptions' => $exceptions,
                 'axisevents' => $axisevents,
                 'trailers' => $trailers
             ],
             'links' => [
                 'self' => route('tripsheet.v1show', $this->tripsheet)
             ]
         ];
     }
 
     public function with($request)
     {
         return [
             'status' => 'success',
             'version' => '1.0.0'
         ];
 
     }
 
     public function withResponse($request, $response)
     {
         $response->header('Accept', 'application/json');
     }
}
