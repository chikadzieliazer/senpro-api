<?php

namespace App\Http\Requests\v1;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class DecomissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'userid' => 'required|integer',
            'groupname' => 'required|string|max:30',
            'username' => 'required|string|max:50',
            'grouptype' => 'required|string|max:210',
            'asset' => 'required|string',
            'peripherals' => 'string|nullable',
            'terminationnumber' => 'nullable',
            'ticketurl' => 'required|string|max:240',
            'decomcomment' => 'required'
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success'   => false,
            'message'   => 'Validation errors',
            'data'      => $validator->errors()
        ]));
    }
}
