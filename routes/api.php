<?php

use App\Http\Controllers\v1\AssetController;
use App\Http\Controllers\v1\CurrentPositionController;
use App\Http\Controllers\v1\DashboardController;
use App\Http\Controllers\v1\DecomissionController;
use App\Http\Controllers\v1\DriverController;
use App\Http\Controllers\v1\DriverGtController;
use App\Http\Controllers\v1\FleetUtilisationController;
use App\Http\Controllers\v1\FmGroupController;
use App\Http\Controllers\v1\FmPositionController;
use App\Http\Controllers\v1\FmTripController;
use App\Http\Controllers\v1\GtGroupController;
use App\Http\Controllers\v1\GtTripController;
use App\Http\Controllers\v1\InterestLocationController;
use App\Http\Controllers\v1\JobcardController;
use App\Http\Controllers\v1\liveDistanceTotalController;
use App\Http\Controllers\v1\LiveFmDistanceController;
use App\Http\Controllers\v1\LiveGtDistanceController;
use App\Http\Controllers\v1\NotificationController;
use App\Http\Controllers\v1\PositionController;
use App\Http\Controllers\v1\ProductController;
use App\Http\Controllers\v1\RoutelegController;
use App\Http\Controllers\v1\SubgroupController;
use App\Http\Controllers\v1\TollgateController;
use App\Http\Controllers\v1\TrailerController;
use App\Http\Controllers\v1\TransitTargetController;
use App\Http\Controllers\v1\TripController;
use App\Http\Controllers\v1\TripsheetController;
use App\Http\Controllers\v1\TripsheetDistanceController;
use App\Http\Controllers\v1\UnitGtController;
use App\Http\Controllers\v1\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::fallback(function(){
    return response()->json([
        'status'    => false,
        'message'   => 'Page Not Found.',
    ], 404);
});


Route::post('v1/login',[UserController::class,'login']);
Route::post('v1/internallogin',[UserController::class,'internalLogin']);
Route::get('dashboard/{groupid}',[DashboardController::class,'dashboard']);
Route::get('dashboard/gt/{client_id}',[DashboardController::class,'gtDistance']);
Route::get('diagnostics',[DashboardController::class,'diagnosticsDashboard']);
Route::apiResource('decomissions', DecomissionController::class);
Route::get('dashboard/group/{groupid}',[\App\Http\Controllers\v2\DashboardController::class,'index']);
Route::get('unittrips',[GtTripController::class,'gtTrips']);

//Old Tripsheets
Route::get('tripsheet/{groupid}',[TripsheetController::class,'index']);
Route::get('tripsheet/{groupid}/{tripsheetid}',[TripsheetController::class,'show']);

Route::get('livegtdistance/{client_id}', [LiveGtDistanceController::class, 'index']);

Route::get('tripsheets/group/{groupid}/site/{site}',[TripsheetController::class,'v1index']);

Route::group(['prefix' => 'v1','middleware' => ['auth:api']], function() {
    
    Route::apiResource('gtclients', GtGroupController::class)->only([
        'index', 'show'
    ]); 

    Route::apiResource('gtunits', UnitGtController::class)->only([
        'index', 'show'
    ]);

    Route::apiResource('assets', AssetController::class)->only([
        'index', 'show'
    ]);

    Route::apiResource('fmdrivers', DriverController::class)->only([
        'index', 'show'
    ]);
    
    Route::get('assets/group/{groupid}',[AssetController::class,'group']);

    Route::get('fmdrivers/group/{groupid}',[DriverController::class,'group']);

    Route::get('livedistancetotals', [liveDistanceTotalController::class, 'fmTotals']);
    
    Route::get('assettrips',[FmTripController::class,'fmTrips']);

    

    Route::apiResource('gtdrivers', DriverGtController::class)->only([
        'index', 'show'
    ]);
    
    Route::get('gtdrivers/group/{groupid}',[DriverGtController::class,'group']);

    Route::apiResource('fmgroups', FmGroupController::class)->only([
        'index', 'show'
    ]);

    Route::apiResource('users', UserController::class);

    Route::apiResource('jobcards', JobcardController::class);

    Route::apiResource('products', ProductController::class);
    Route::get('products/group/{groupid}',[ProductController::class,'group']);

    Route::apiResource('trailers', TrailerController::class);
    Route::get('trailers/group/{groupid}',[TrailerController::class,'group']);

    Route::apiResource('routelegs', RoutelegController::class);
    Route::get('routelegs/group/{groupid}',[RoutelegController::class,'group']);

    Route::apiResource('subgroups', SubgroupController::class);
    Route::get('subgroups/group/{groupid}',[SubgroupController::class,'group']);

    Route::apiResource('transittargets', TransitTargetController::class);

    Route::apiResource('notifications', NotificationController::class);
    
    Route::get('transittargets/group/{groupid}',[TransitTargetController::class,'group']);

    Route::get('livedistancetotalsgt', [liveDistanceTotalController::class, 'gtTotals']);

    Route::get('logout',[UserController::class,'logout']);

    //Executive Dashboard Routes
    Route::get('livefmdistance/{groupid}/{site}', [LiveFmDistanceController::class, 'index']);

    //Tripsheets
    Route::get('tripsheets/group/{groupid}/site/{site}',[TripsheetController::class,'v1index']);
    Route::get('tripsheets/{tripsheetid}',[TripsheetController::class,'v1show'])->name('tripsheet.v1show');

    //FM Fleet Utilisation
    Route::get('fleet/group/{groupid}', [FleetUtilisationController::class, 'fmTotal']);
    Route::get('fleet/utilised/group/{groupid}', [FleetUtilisationController::class, 'utilisedAssets']);
    Route::get('fleet/underutilised/group/{groupid}', [FleetUtilisationController::class, 'underutilisedAssets']);

  

    //GT Fleet Utilisation
    Route::get('fleet/client/{client}', [FleetUtilisationController::class, 'gtTotal']);
    Route::get('fleet/utilised/client/{clientid}', [FleetUtilisationController::class, 'utilisedUnits']);
    Route::get('fleet/underutilised/client/{clientid}', [FleetUtilisationController::class, 'underutilisedUnits']);
 
    Route::get('livefmdistance/monthly/total/{groupid}/{site}', [LiveFmDistanceController::class, 'monthlyDistance']);
    Route::get('livefmdistance/monthly/average/{groupid}/{site}', [LiveFmDistanceController::class, 'monthlyAverage']);
    Route::get('distance/overview/{groupid}/{site}', [TripsheetDistanceController::class, 'index']);



    Route::get('trips/group/{groupid}', [TripController::class, 'index']);
    Route::get('trips/{tripid}', [TripController::class, 'show'])->name('trip.show');

    Route::get('positions/gt/client/{client}', [PositionController::class, 'index']);
    Route::get('positions/gt/{positionid}', [PositionController::class, 'show'])->name('position.show');

    Route::get('positions/fm/group/{groupid}', [FmPositionController::class, 'index']);
    Route::get('positions/fm/{positionid}', [FmPositionController::class, 'show'])->name('fmPosition.show');

    // Current dev
    Route::get('/interest-locations/groups/{groupId}', [InterestLocationController::class, 'index']);
    Route::get('/interest-locations/{locationId}', [InterestLocationController::class, 'show'])->name('interest-location.show');

    Route::post('/tollgates/groups/{groupId}', [TollgateController::class, 'index']);
    Route::get('/tollgates/{tollgateId}', [TollgateController::class, 'show'])->name('tollgates.show');

    //New Dashboard
    Route::get('dashboard/group/{groupid}',[\App\Http\Controllers\v2\DashboardController::class,'index']);
    Route::get('distance/fm/live/group/{groupid}',[\App\Http\Controllers\v2\DashboardController::class,'fmDistance']);
    Route::get('distance/gt/live/group/{unitid}',[\App\Http\Controllers\v2\DashboardController::class,'gtDistance']);
    Route::get('fleet/count/group/{groupid}',[\App\Http\Controllers\v2\DashboardController::class,'fleetUtilisation']);

    // GT Monthly Total Distance
    Route::get('livegtdistance/monthly/total/{unit}', [LiveGtDistanceController::class, 'monthlyDistance']);

    //Current Position
    Route::get('positions/current/{groupid}',[CurrentPositionController::class,'group']);
});
