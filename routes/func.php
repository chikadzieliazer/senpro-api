<?php

function record_sort($records, $field, $reverse=false)
{
    $hash = array();
   
    foreach($records as $key => $record)
    {
        $hash[$record[$field].$key] = $record;
    }
   
    ($reverse)? krsort($hash) : ksort($hash);
   
    $records = array();
   
    foreach($hash as $record)
    {
        $records []= $record;
    }
   
    return $records;
}

function get_info($type, $fmid, $gtid, $dbh, $dbn, $dbu, $dbp){
	if ($fmid==0) { $fmid=-1; }
	if ($gtid==0) { $gtid=-1; }
	$sql       = "";
	if($type  == "ADMINISTRATOR"){
		$sql   = "SELECT COUNT(g.groupid) as fmgroups, 
						(SELECT startdatetime FROM tblevent where startdatetime IS NOT NULL AND startdatetime >= DATE_TRUNC('month', CURRENT_DATE) ORDER BY startdatetime DESC LIMIT 1) AS lastevent, 
						(SELECT datetime FROM tblposition_gt where datetime IS NOT NULL AND datetime >= DATE_TRUNC('month', CURRENT_DATE) ORDER BY datetime DESC LIMIT 1) AS lastposition, 
						(SELECT end_time FROM tblfuel_gt where end_time IS NOT NULL AND end_time >= DATE_TRUNC('month', CURRENT_DATE) ORDER BY end_time DESC LIMIT 1) AS lastfuel, 
						(SELECT startdatetime FROM tblevent where startdatetime IS NOT null AND locationname LIKE 'CP %' AND startdatetime >= DATE_TRUNC('month', CURRENT_DATE) ORDER BY startdatetime DESC LIMIT 1) AS lastlocation,
						(SELECT COUNT(eventid) FROM tblevent e, tbllibraryevent l where e.eventtypeid=l.eventtypeid AND l.description='transit check' AND locationname NOT LIKE 'CP %' AND endlongitude IS NOT NULL AND endlatitude IS NOT NULL AND e.startdatetime >= DATE_TRUNC('month', CURRENT_DATE)) AS emptylocations,
						(SELECT COUNT(assetid) FROM tblasset WHERE userstate<>'Decommissioned') AS fmassets,
						(SELECT COUNT(DISTINCT unit_name) FROM tblunit_gt) AS gtunits,
						(SELECT COUNT(d.driverid) FROM tbldriver d, tblsubgroup s WHERE d.siteid=s.subgroupid AND s.name NOT LIKE '%Decommissioned%') AS fmdrivers,
						(SELECT COUNT(driverid) FROM tbldriver_gt) AS gtdrivers,
						(SELECT COUNT(account_id) FROM tblclient_gt) AS gtgroups,
						(SELECT COUNT(distinct tripsheet) FROM tbltripsheet t WHERE t.end >= DATE_TRUNC('month', CURRENT_DATE)) AS tripsheets,
						(SELECT COUNT(id) FROM tbl_customer) AS customers,
						(SELECT COUNT(legid) FROM tbl_routeleg) AS legs,
						(SELECT COUNT(id) FROM tbl_product) AS products,
						(SELECT COUNT(id) FROM tbl_transittarget) AS transit_targets,
						(SELECT COUNT(id) FROM tbl_depot) AS depots,
						(SELECT COUNT(id) FROM tbl_jobcard WHERE created >= DATE_TRUNC('month', CURRENT_DATE)) AS jobcards
					FROM tblgroup g ";
	}
	else{
		$sql   	  = "SELECT COUNT(distinct t.tripsheet) as tripsheets,
						(SELECT COUNT(id) FROM tbl_customer c WHERE c.groupid=$fmid OR c.groupid=$gtid) AS customers,
						(SELECT COUNT(legid) FROM tbl_routeleg lg WHERE lg.groupid=$fmid OR lg.groupid=$gtid) AS legs,
						(SELECT COUNT(id) FROM tbl_product p WHERE p.groupid=$fmid OR p.groupid=$gtid) AS products,
						(SELECT COUNT(id) FROM tbl_depot d WHERE d.groupid=$fmid OR d.groupid=$gtid) AS depots,
						(SELECT COUNT(id) FROM tbl_jobcard jc WHERE jc.created >= DATE_TRUNC('month', CURRENT_DATE) AND (jc.groupid=$fmid OR jc.groupid=$gtid)) AS jobcards "; 

		if ($fmid != 0) {
			$sql .= ",(SELECT COUNT(assetid) FROM tblasset fa WHERE fa.groupid=$fmid AND fa.userstate<>'Decommissioned') AS fmassets,
					 (SELECT COUNT(da.driverid) FROM tbldriver da, tblsubgroup ds WHERE da.groupid=$fmid AND da.siteid=ds.subgroupid AND ds.name NOT LIKE '%Decommissioned%') AS fmdrivers,
					 (SELECT startdatetime FROM tblevent where startdatetime IS NOT NULL AND startdatetime >= DATE_TRUNC('month', CURRENT_DATE) ORDER BY startdatetime DESC LIMIT 1) AS lastevent,
					 (SELECT startdatetime FROM tblevent where startdatetime IS NOT null and locationname LIKE 'CP %' AND startdatetime >= DATE_TRUNC('month', CURRENT_DATE) ORDER BY startdatetime DESC LIMIT 1) AS lastlocation,
					 (SELECT COUNT(eventid) FROM tblevent e, tbllibraryevent l where e.eventtypeid=l.eventtypeid AND l.description='transit check' AND locationname NOT LIKE 'CP %' AND endlongitude IS NOT NULL AND endlatitude IS NOT NULL AND e.startdatetime >= DATE_TRUNC('month', CURRENT_DATE)) AS emptylocations,
					 (SELECT COUNT(id) FROM tbl_transittarget tt WHERE tt.groupid=$fmid) AS transit_targets ";
		}

		if ($gtid != 0) {
			$sql .= ",(SELECT COUNT(DISTINCT unit_name) FROM tblunit_gt gu WHERE gu.client_id=$gtid) AS gtunits,
					 (SELECT COUNT(driverid) FROM tbldriver_gt gd WHERE gd.client_id=$gtid) AS gtdrivers,
					 (SELECT datetime FROM tblposition_gt where datetime IS NOT NULL AND datetime >= DATE_TRUNC('month', CURRENT_DATE) ORDER BY datetime DESC LIMIT 1) AS lastposition,
					 (SELECT end_time FROM tblfuel_gt where end_time IS NOT NULL AND end_time >= DATE_TRUNC('month', CURRENT_DATE) ORDER BY end_time DESC LIMIT 1) AS lastfuel ";
		}

		$sql   	 .= "FROM tbltripsheet t WHERE t.end >= DATE_TRUNC('month', CURRENT_DATE) AND (t.groupid=$fmid OR t.groupid=$gtid)";
	}

	// echo $sql;
	// exit;

	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	if($stmt->execute()){
        return array('result'=>true,'info'=>$stmt->fetch());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}

function needs_fetch_loop($url){
	$loop_ids = ["driverId", "assetId", "journeyId", "messageId"];
	foreach($loop_ids as $id) {
		if (stripos($url, "{$id}") !== false) 
			return true;
	}
	return false;
}

function get_field_from_url($url){
	if (strpos($url, 'driverId') !== false) {
		return 'driverId';
	}
	if (strpos($url, 'assetId') !== false) {
		return 'assetId';
	}
	if (strpos($url, 'journeyId') !== false) {
		return 'journeyId';
	}
	if (strpos($url, 'messageId') !== false) {
		return 'messageId';
	}
	return null;
}

function get_table_from_field($field){
	switch ($field) {
		case "driverid":
			return "tbldriver";
			break;
		case 'assetid':
			return 'tblasset';
			break;
		case 'journeyid':
			return 'tbljourney';
			break;
		case 'messageid':
			return 'tblmessage';
			break;
	}
	return null;
}

function resolve_table_name($object_name){
	switch ($object_name) {
		case "RouteLocations":
			return "routelocation";
			break;
		case 'JourneyAssets':
			return 'journeyasset';
			break;
		case 'ParameterDefinitions':
			return 'parameterdefinition';
			break;
		case 'Intervals':
			return 'interval';
			break;
		case 'SubTrips':
			return 'subtrip';
			break;
		case 'MediaUrls':
			return 'mediaurl';
			break;
		case 'EventTypes':
			return 'eventtype';
			break;
	}
	return strtolower($object_name);
}

function array_to_XLS($data){
	$out = fopen("php://output", 'w');
	$i 	 = 0;

	foreach ($data as $values) {
    	if ($i<1) {
    		fputcsv($out, array_keys($values),"\t");
    	}
	    fputcsv($out, $values,"\t");
	    $i++;
	}
    $xls = fgets($out);
    fclose($out);

    return $xls;
}

function array_to_CSV($data){
    $out = fopen("php://output", 'w');
    $i 	 = 0;
    
    foreach ($data as $values) {
    	if ($i<1) {
    		fputcsv($out, array_keys($values), ',', '"');
    	}
	    fputcsv($out, $values, ',', '"');
	    $i++;
	}
    $csv = fgets($out);
    fclose($out);

    return $csv;
}

function array_to_CSV_multi($data){
    $out = fopen("php://output", 'w');

    // print_r($data);
    // exit;
    
    if (is_array($data)) {
    	for($i=0; $i<count($data); $i++) {
    		// $r = stripslashes($data[$i]);
	    	if ($i<1) {
	    		fputcsv($out, array_keys($data[$i]), ',', '"');
	    	}
		    fputcsv($out, $data[$i], ',', '"');
		}
    }
    
    $csv = fgets($out);
    fclose($out);

    return $csv;
}

function force_download_headers($filename) {
    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
}

function add_url_params($group_id, $query_string){
	if(isset($_GET['start'])){
		$query_string = str_replace("{start}", $_GET['start'], $query_string);
	}
	if(isset($_GET['end'])){
		$query_string = str_replace("{end}", $_GET['end'], $query_string);
	}
	$query_string = str_replace("{organisationId}", $group_id, $query_string);
	$query_string = str_replace("{groupId}", $group_id, $query_string);
	
	return $query_string;
}

function create_post_request_data($group_id, $table, $dbh, $dbn, $dbu, $dbp){
	$post_data      = '';
	$asset_ids      = get_id_array($group_id, "tblasset", "assetid", $dbh, $dbn, $dbu, $dbp);
	$driver_ids     = get_id_array($group_id, "tbldriver", "driverid", $dbh, $dbn, $dbu, $dbp);
	$ev_types       = get_id_array($group_id, "tbllibraryevent", "eventtypeid", $dbh, $dbn, $dbu, $dbp);
	$hos_ev_types   = get_id_array($group_id, "tblghoseventtypecategory", "id", $dbh, $dbn, $dbu, $dbp);

	if($table == 'tbltrip'){
		$post_data = str_replace('"', '', json_encode($asset_ids));
	}
	elseif($table == 'tblevent'){
		$post_data  = '{"EntityIds":'.  str_replace('"', '',json_encode($asset_ids)) .',';
		$post_data .= '"EventTypeIds":'.  str_replace('"', '',json_encode($ev_types)) .'}';
		echo $post_data	;
		exit;
	}
	elseif($table == 'tbldriverscore'){
		$post_data = str_replace('"', '', json_encode($driver_ids));
	}
	elseif($table == 'tblghos'){
		$post_data  = '{"EntityIds":'.  str_replace('"', '',json_encode($asset_ids)) .',';
		$post_data .= '"EventTypeIds":'.  str_replace('"', '',json_encode($hos_ev_types)) .'}';
	}
	return $post_data;
}

function fetch_post($conn, $groupid, $endpoint, $post_data){
	$url       = add_url_params($groupid, $endpoint);
	$response  = $conn->post($url, $post_data, array('Content-Type' => 'application/json'));

	return $response;
}

function fetch_get($conn, $groupid, $endpoint){
	$url       = add_url_params($groupid, $endpoint);
	$response  = $conn->get($url); 

	return $response;
}

function fetch_group_entities($rest_client, $mix_groups, $grp_table, $dbh, $dbn, $dbu, $dbp){
	$response  = fetch_get($rest_client, "", $mix_groups);
	return save_response($response, "", $grp_table, [], $dbh, $dbn, $dbu, $dbp);
}

function create_insert_string($group_id, $table_name, $data, $ignore_fields) {
	$query              = "INSERT INTO ". strtolower($table_name);
	if(is_array($data)){
		// print_r($data);
		$keys           = array(); 
		$maxKeysIndex   = 0;

		for ($i=0; $i < count($data); $i++) {
			$data_array = json_decode(json_encode($data[$i]), True);
			if(count(array_keys($data_array)) > $maxKeysIndex){
				$maxKeysIndex = $i;
			}           
		}
		foreach($data[$maxKeysIndex] as $k => $v) {
			if(!in_array($k, $ignore_fields)){  $keys[] = $k;  }
		}
		$query      = $query ." (". implode(', ', $keys);
		if(empty($group_id)){
			$query  = $query .") VALUES ";
		}
		else{
			$query  = $query .", groupid) VALUES ";
		}
			
		for ($i=0; $i < count($data); $i++) {
			if($i>0){ $query = $query .","; }
			$data_array      = json_decode(json_encode($data[$i]), True);
			$values          = array();
			
			for($j=0; $j<count($keys); $j++){
				if (array_key_exists($keys[$j], $data_array)) {
					$v        = $data_array[$keys[$j]];
					// print_r($v);
					// echo "<br/>";
					// print_r($keys);
					// echo "<br/>";
					if(!is_array($v)){
							$values[] = !empty($v) ? "'". str_replace("'","`",$v) ."'" : "null";
					}
					else{}
				}
				else{
					$values[] = "null";
				}
			}
			$query = $query ."(". implode(', ', $values);
			if(empty($group_id)){
				$query  = $query .")";
			}
			else{
				$query  = $query .", '$group_id')";
			}
		}
	}
	else{
		$keys   = array(); 
		$vals   = array(); 
		// echo($data);
		foreach($data as $k => $v) {
			if(!in_array($k, $ignore_fields)){  
				$keys[] = strtolower($k);
				$vals[] = !empty($v) ? "'". str_replace("'","`",$v) ."'" : "null";
			}
		}
		$query = $query ." (". implode(', ', $keys) .") VALUES (". implode(', ', $vals) .")";
	}
	return $query .";";
}

function create_sub_insert_string($group_id, $table_name, $data, $target_objects){
	$query                  = "";
	for ($i=0; $i < count($data); $i++) {
		$full_sub_array = json_decode(json_encode($data[$i]), True);
		for ($a=0; $a < count($target_objects); $a++) {
			if (array_key_exists($target_objects[$a], $full_sub_array)) {
				$sub_array = $full_sub_array[$target_objects[$a]];

				// TODO: Handle array of arrays here
				// $decoded_arr = json_decode ($sub_array, false, 512, JSON_BIGINT_AS_STRING);
				// $query = create_insert_string("tbl".$target_objects[$a], $decoded_arr, array());
		
				$keys      = array(); 
				$vals      = array();
				if(count($sub_array)>0){
					foreach($sub_array as $k => $v) {
						$keys[] = strtolower($k);
						$vals[] = !empty($v) ? "'". str_replace("'","`",$v) ."'" : "null";
					}
					$sub_table = resolve_table_name($target_objects[$a]);
					$query .= "INSERT INTO tbl$sub_table (". implode(', ', $keys);
					if(!empty($group_id)){ $query .= ", groupid";}
					$query .= ") VALUES (". implode(', ', $vals);
					if(!empty($group_id)){ $query .= ", '$group_id'"; }
					$query .= ");";        
				}
			}
		}
	}
	return $query;
}

function save_response($response, $group_id, $table, $fields_to_ignore, $dbh, $dbn, $dbu, $dbp){
	$saved  = 0;
	if($response->info->http_code == 200){
		if (version_compare(PHP_VERSION, '5.4.0', '>=')) {
			$result_arr = json_decode ($response->response, false, 512, JSON_BIGINT_AS_STRING);
			if(is_array($result_arr)){
				if(count($result_arr)>1){
					$sql    = create_insert_string($group_id, $table, $result_arr, $fields_to_ignore);
					$saved += gcp_query($sql, $dbh, $dbn, $dbu, $dbp);
					if(count($fields_to_ignore) > 0){
					    $sql2   = create_sub_insert_string($group_id, $table, $result_arr, $fields_to_ignore);
					    $saved += gcp_multi_query($sql2, $dbh, $dbn, $dbu, $dbp);
					}
				}
				else{
					echo "\n - No data received from MIX [$table] - GRP [$group_id]. HTTP_". $response->info->http_code ." response.";
				}
			}
			else{
				$sql    = create_insert_string($group_id, $table, $result_arr, $fields_to_ignore);
				$saved += gcp_query($sql, $dbh, $dbn, $dbu, $dbp);
				if(count($fields_to_ignore) > 0){
				    $sql2   = create_sub_insert_string($group_id, $table, $result_arr, $fields_to_ignore);
				    $saved += gcp_multi_query($sql2, $dbh, $dbn, $dbu, $dbp);
				}
        	}
		}
	}
	else{
		echo "\n - No data received from MIX [$table] - GRP [$group_id]. HTTP_". $response->info->http_code ." response.";
	}
	return $saved;
}

function exists_in_db($table_name, $id_column, $value){
	$sql     = "SELECT $id_column FROM $table_name WHERE $id_column='$value'";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	return (pg_num_rows($result)>0);
}

// GRP SITES
function list_group_sites($groupid, $parentid, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT s.*, g.name AS group, 
					(SELECT COUNT(assetid) FROM tblasset WHERE siteid=s.subgroupid AND userstate<>'Decommissioned') AS assets,
					(SELECT COUNT(d.driverid) FROM tbldriver d WHERE d.siteid=s.subgroupid) AS drivers,    
					(SELECT p.name FROM tblsubgroup p WHERE p.subgroupid=s.parentid) AS parent,    
					(SELECT COUNT(subgroupid) FROM tblsubgroup c WHERE c.parentid=s.subgroupid) AS children    
				FROM tblsubgroup s, tblgroup g   
				WHERE s.groupid=g.groupid ";

	if ($groupid!="" && $groupid!=0) { $sql .= " AND s.groupid=$groupid "; }   
	if ($parentid!="" && $parentid!=0) { $sql .= " AND s.parentid=$parentid "; }   
				  
	$sql 	.= " ORDER BY name";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}

function list_specific_events($groupid, $driver_id, $eventDesception, $startDate, $endDate, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT e.startdatetime, e.enddatetime, e.endlatitude, e.endlongitude,  e.totaltimeseconds, e.mediaurls, l.description 
						FROM tblevent e, tbllibraryevent l 
						WHERE l.eventtypeid=e.eventtypeid 
						AND e.groupid = l.groupid 
						AND l.groupid = '$groupid' 
						AND e.driverid = '$driver_id' 
						AND l.description LIKE '$eventDesception' 
						AND e.startdatetime >= '$startDate' 
						AND e.enddatetime <= '$endDate'"; 
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);
	return $row;
}
function list_site_drivers($site_id, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT d.*, g.name AS customer FROM tbldriver d, tblgroup g WHERE d.groupid=g.groupid   "; 
	if ($site_id!=0) { $sql .= " AND d.siteid=$site_id "; } 
	$sql    .= " ORDER BY customer, name";
	
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
// --- END SITES ---

// PRODUCTS
function save_product($groupid,$name,$description,$density,$units,$type,$dbh,$dbn,$dbu,$dbp){

	$sql 	  = "INSERT INTO tbl_product (groupid,name,description,density,units,created,type) 
					VALUES(?, ?, ?, ?, ?, ?, ?);";
	
	$created  = Date('Y-M-d H:i:s');
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $name);
	$stmt     ->bindParam(3,  $description);
	$stmt     ->bindParam(4,  $density);
	$stmt     ->bindParam(5,  $units);
	$stmt     ->bindParam(6,  $created);
	$stmt     ->bindParam(7,  $type);

	if($stmt->execute()){
		$_id  = $db_conn->lastInsertId();
        return array('result'=>true,'id'=>$_id,'message'=>'Product has been created');
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function list_load_products($dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT p.*, 
					(SELECT f.name FROM tblgroup f WHERE f.groupid=p.groupid) AS fmgroup,
					(SELECT g.client_account FROM tblclient_gt g WHERE g.client_id=p.groupid) AS gtgroup
				  FROM tbl_product p ORDER BY name";

	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function list_group_load_products($groupid, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT p.*, 
					(SELECT f.name FROM tblgroup f WHERE f.groupid=p.groupid) AS fmgroup,
					(SELECT g.client_account FROM tblclient_gt g WHERE g.client_id=p.groupid) AS gtgroup
				  FROM tbl_product p WHERE p.groupid=$groupid   
				  ORDER BY name";

	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function update_product($id,$groupid,$name,$description,$density,$units,$type,$dbh,$dbn,$dbu,$dbp){

	$sql 	  = "UPDATE tbl_product SET groupid=?,name=?,description=?,density=?,units=?,type=? WHERE id=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $name);
	$stmt     ->bindParam(3,  $description);
	$stmt     ->bindParam(4,  $density);
	$stmt     ->bindParam(5,  $units);
	$stmt     ->bindParam(6,  $type);
	$stmt     ->bindParam(7,  $id);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function delete_product($prodid,$dbh,$dbn,$dbu,$dbp){

	$sql 	  = "DELETE FROM tbl_product WHERE id=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $prodid);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
// --- END PRODUCTS ---

// DRIVERS
function save_driver($groupid,$name,$emp_num,$country,$dbh,$dbn,$dbu,$dbp){

	$sql 	  = "INSERT INTO tbldriver_gt (client_id,name,employeenumber,country) 
					VALUES(?, ?, ?, ?);";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $name);
	$stmt     ->bindParam(3,  $emp_num);
	$stmt     ->bindParam(4,  $country);

	if($stmt->execute()){
        $_id  = $db_conn->lastInsertId();
        return array('result'=>true,'id'=>$_id,'message'=>'Driver has been created');
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function list_fm_drivers($dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT d.*, g.name AS customer    
					FROM tbldriver d, tblgroup g, tblsubgroup s   
					WHERE d.groupid=g.groupid AND d.siteid=s.subgroupid AND s.name NOT LIKE '%Decommissioned%'    
					ORDER BY customer, name";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function list_group_fm_drivers($group_id, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT d.*, g.name AS customer    
					FROM tbldriver d, tblgroup g, tblsubgroup s   
					WHERE d.groupid=$group_id AND d.groupid=g.groupid AND d.siteid=s.subgroupid AND s.name NOT LIKE '%Decommissioned%'    
					ORDER BY customer, name";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function list_gt_drivers($dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT d.*, c.client_account AS customer    
					FROM tbldriver_gt d, tblclient_gt c   
					WHERE d.client_id=c.client_id    
					ORDER BY customer, name";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function list_group_gt_drivers($group_id, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT d.*, c.client_account AS customer    
					FROM tbldriver_gt d, tblclient_gt c   
					WHERE d.client_id=$group_id AND d.client_id=c.client_id    
					ORDER BY customer, name";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function update_driver($id,$groupid,$name,$emp_num,$country,$dbh,$dbn,$dbu,$dbp){

	$sql 	  = "UPDATE tbldriver_gt SET client_id=?,name=?,employeenumber=?,country=?  WHERE driverid=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $name);
	$stmt     ->bindParam(3,  $emp_num);
	$stmt     ->bindParam(4,  $country);
	$stmt     ->bindParam(5,  $id);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function delete_gt_driver($drvid,$dbh,$dbn,$dbu,$dbp){

	$sql 	  = "DELETE FROM tbldriver_gt WHERE driverid=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $drvid);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
// --- END DRIVERS ---

// CUSTOMERS
function save_group_customer($groupid,$name,$description,$dbh,$dbn,$dbu,$dbp){

	$sql 	  = "INSERT INTO tbl_customer (groupid,name,description,created) 
					VALUES(?, ?, ?, ?);";
	
	$created  = gmdate('Y-M-d H:i:s');
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $name);
	$stmt     ->bindParam(3,  $description);
	$stmt     ->bindParam(4,  $created);

	if($stmt->execute()){
		$_id  = $db_conn->lastInsertId();
        return array('result'=>true,'id'=>$_id,'message'=>'Customer has been created');
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function list_customers($dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT c.*, 
					(SELECT f.name FROM tblgroup f WHERE f.groupid=c.groupid) AS fmgroup,
					(SELECT g.client_account FROM tblclient_gt g WHERE g.client_id=c.groupid) AS gtgroup
				  FROM tbl_customer c ORDER BY name";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function list_group_customers($group, $dbh, $dbn, $dbu, $dbp){
	$sql 	 = "SELECT c.*, 
					(SELECT f.name FROM tblgroup f WHERE f.groupid=c.groupid) AS fmgroup,
					(SELECT g.client_account FROM tblclient_gt g WHERE g.client_id=c.groupid) AS gtgroup
				  FROM tbl_customer c WHERE c.groupid=$group   
				  ORDER BY name";

	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function update_group_customer($id,$groupid,$name,$description,$dbh,$dbn,$dbu,$dbp){

	$sql 	  = "UPDATE tbl_customer SET groupid=?,name=?,description=? WHERE id=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $name);
	$stmt     ->bindParam(3,  $description);
	$stmt     ->bindParam(4,  $id);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function delete_group_customer($custid,$dbh,$dbn,$dbu,$dbp){

	$sql 	  = "DELETE FROM tbl_customer WHERE id=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $custid);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
// --- END CUSTOMERS ---

// PUMP STATIONS
function save_pump_station($groupid,$name,$pumps,$description,$dbh,$dbn,$dbu,$dbp){

	$sql 	  = "INSERT INTO tbl_fuel_station (groupid,name,pumps,description,status,created) 
					VALUES(?,?,?,?,?,?);";
	
	$created  = gmdate('Y-M-d H:i:s');
	$status   = "ACTIVE";

	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $name);
	$stmt     ->bindParam(3,  $pumps);
	$stmt     ->bindParam(4,  $description);
	$stmt     ->bindParam(5,  $status);
	$stmt     ->bindParam(6,  $created);

	if($stmt->execute()){
		$_id  = $db_conn->lastInsertId();
        return array('result'=>true,'id'=>$_id,'message'=>'Pump station has been created');
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function get_pump_station($id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT * FROM tbl_fuel_station WHERE id=$id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_assoc($db_result);

	return $row;
}
function list_pump_stations($dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT s.*, 
					(SELECT f.name FROM tblgroup f WHERE f.groupid=s.groupid) AS fmgroup,
					(SELECT g.client_account FROM tblclient_gt g WHERE g.client_id=s.groupid) AS gtgroup
				  FROM tbl_fuel_station s   
				  WHERE s.status='ACTIVE' ORDER BY name";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function list_group_pump_stations($group, $dbh, $dbn, $dbu, $dbp){
	$sql 	 = "SELECT s.*, 
					(SELECT f.name FROM tblgroup f WHERE f.groupid=s.groupid) AS fmgroup,
					(SELECT g.client_account FROM tblclient_gt g WHERE g.client_id=s.groupid) AS gtgroup
				  FROM tbl_fuel_station s WHERE s.groupid=$group AND s.status='ACTIVE'  
				  ORDER BY name";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function update_pump_station($id,$groupid,$name,$pumps,$description,$dbh,$dbn,$dbu,$dbp){

	$sql 	  = "UPDATE tbl_fuel_station SET groupid=?,name=?,pumps=?,description=? WHERE id=? AND status<>'DELETED';";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $name);
	$stmt     ->bindParam(3,  $pumps);
	$stmt     ->bindParam(4,  $description);
	$stmt     ->bindParam(5,  $id);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function delete_pump_station($id,$dbh,$dbn,$dbu,$dbp){

	$sql 	  = "UPDATE tbl_fuel_station SET status='DELETED' WHERE id=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $id);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
// --- END PUMP STATIONS ---

// DEPOTS
function save_depot($groupid,$name,$type,$city,$dbh,$dbn,$dbu,$dbp){

	$sql 	  = "INSERT INTO tbl_depot (groupid,name,type,city,created) 
					VALUES(?, ?, ?, ?, ?);";
	
	$created  = Date('Y-M-d H:i:s');
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $name);
	$stmt     ->bindParam(3,  $type);
	$stmt     ->bindParam(4,  $city);
	$stmt     ->bindParam(5,  $created);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function list_depots($dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT p.*, 
					(SELECT f.name FROM tblgroup f WHERE f.groupid=p.groupid) AS fmgroup,
					(SELECT g.client_account FROM tblclient_gt g WHERE g.client_id=p.groupid) AS gtgroup
				  FROM tbl_depot p ORDER BY name";

	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function list_group_depots($groupid, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT p.*, 
					(SELECT f.name FROM tblgroup f WHERE f.groupid=p.groupid) AS fmgroup,
					(SELECT g.client_account FROM tblclient_gt g WHERE g.client_id=p.groupid) AS gtgroup
				  FROM tbl_depot p WHERE p.groupid=$groupid   
				  ORDER BY name";

	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function update_depot($id,$groupid,$name,$type,$city,$dbh,$dbn,$dbu,$dbp){

	$sql 	  = "UPDATE tbl_depot SET groupid=?,name=?,type=?,city=? WHERE id=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $name);
	$stmt     ->bindParam(3,  $type);
	$stmt     ->bindParam(4,  $city);
	$stmt     ->bindParam(5,  $id);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function delete_depot($depotid,$dbh,$dbn,$dbu,$dbp){

	$sql 	  = "DELETE FROM tbl_depot WHERE id=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $depotid);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
// --- END DEPOTS ---


// ASSETS
function list_gt_assets($dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT DISTINCT ON (a.asset_id) asset_id, a.unit_id, a.client_id, a.unit_name, g.client_account AS group,  
					targetemptyconsumption, targetloadedconsumption       
				  FROM tblunit_gt a, tblclient_gt g    
				  WHERE a.client_id=g.client_id AND a.asset_id IS NOT NULL  
				  ORDER BY asset_id, unit_name";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function list_fm_assets($dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT a.*, g.name AS customer, 
						(SELECT u.unit_name FROM tblunit_gt u WHERE u.fm_assetid=a.assetid) AS unit_name    
					FROM tblasset a, tblgroup g   
					WHERE a.groupid=g.groupid AND a.userstate<>'Decommissioned'    
					ORDER BY customer, description";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function list_group_gt_assets($group_id, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT DISTINCT ON (a.asset_id) asset_id, a.unit_id, a.client_id, a.unit_name, g.client_account AS group, 
					targetemptyconsumption, targetloadedconsumption    
				  FROM tblunit_gt a, tblclient_gt g    
				  WHERE a.client_id=$group_id AND a.client_id=g.client_id AND a.asset_id IS NOT NULL  
				  ORDER BY asset_id, unit_name";

				  echo $sql;
				  exit; 
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function list_group_fm_assets($group_id, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT a.*, g.name AS customer, 
						(SELECT u.unit_name FROM tblunit_gt u WHERE u.fm_assetid=a.assetid) AS unit_name    
					FROM tblasset a, tblgroup g   
					WHERE a.groupid=g.groupid AND a.groupid=$group_id AND a.userstate<>'Decommissioned'     
					ORDER BY customer, description";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function list_vehicles($dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT a.*, g.name AS customer    
					FROM tblasset a, tblgroup g   
					WHERE a.groupid=g.groupid AND a.userstate<>'Decommissioned'    
					ORDER BY customer, description";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function update_asset($assetid,$loaded,$empty,$dbh,$dbn,$dbu,$dbp){
	$sql 	  = "UPDATE tblasset SET loadconsumption=?, emptyconsumption=? WHERE assetid=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $loaded);
	$stmt     ->bindParam(2,  $empty);
	$stmt     ->bindParam(3,  $assetid);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function link_assets($fmAssetid,$gtAssetid,$dbh,$dbn,$dbu,$dbp){
	$sql 	  = "UPDATE tblunit_gt SET fm_assetid=? WHERE unit_id=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $fmAssetid);
	$stmt     ->bindParam(2,  $gtAssetid);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function unlink_fm_asset($fmAssetid,$dbh,$dbn,$dbu,$dbp){
	$sql 	  = "UPDATE tblunit_gt SET fm_assetid=NULL WHERE fm_assetid=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $fmAssetid);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function update_gt_asset($assetid,$empty,$loaded,$dbh,$dbn,$dbu,$dbp){
	$sql 	  = "UPDATE tblunit_gt SET targetemptyconsumption=?, targetloadedconsumption=? WHERE unit_id=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $empty);
	$stmt     ->bindParam(2,  $loaded);
	$stmt     ->bindParam(3,  $assetid);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
// --- END ASSETS ---


// TRIP LEGS
function save_leg($groupid,$leg,$code,$distance,$loaded,$empty,$dbh,$dbn,$dbu,$dbp){
	$sql 	  = "INSERT INTO tbl_routeleg (groupid,leg,code,distance,duration_loaded,duration_empty,created) 
					VALUES(?, ?, ?, ?, ?, ?, ?);";
	
	$created  = Date('Y-M-d H:i:s');
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $leg);
	$stmt     ->bindParam(3,  $code);
	$stmt     ->bindParam(4,  $distance);
	$stmt     ->bindParam(5,  $loaded);
	$stmt     ->bindParam(6,  $empty);
	$stmt     ->bindParam(7,  $created);
	try {
		if($stmt->execute()){
	        return array('result'=>true,'rows'=>$stmt->rowCount());
	    }
	    else {
	        $errors = $stmt->errorInfo();
	        return array('result'=>false,'error'=>$errors[2]);
	    }
	}
	catch(Exception $e) {
		if (stristr($e->errorInfo[2], "duplicate key") ) {
			return array('result'=>false,'error'=>"Duplicate route code entry for group.");
		}
	}
}
function list_legs($dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT l.*, 
					(SELECT f.name FROM tblgroup f WHERE f.groupid=l.groupid) AS fmgroup,
					(SELECT g.client_account FROM tblclient_gt g WHERE g.client_id=l.groupid) AS gtgroup
	 			  FROM tbl_routeleg l ORDER BY leg";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}

function calc_load_percent($groupid, $trip, $dbh, $dbn, $dbu, $dbp){
	$l = substr_count($trip, '(L)');
	$e = substr_count($trip, '(E)');

	// return ($l+$e)>0 ? number_format(100*$l/($l+$e),1) : 0;


	$tripArr = explode("-", str_replace("(L)","",str_replace("(E)","",$trip)));
	$tripStr = "";
	if ($tripArr) {
		$tmp = "";
		foreach($tripArr AS $t){
			if ($tmp == "") {
				$tmp = rtrim(ltrim($t));
			}
			else{
				if($tripStr!=""){ $tripStr .= ","; }
				$tripStr .= "'".$tmp."-".$t."'";
				$tmp = rtrim(ltrim($t));
			}
		}
	}
	if ($tripStr!="") {
		$totDist = 0;
		$totLoad = 0;
		$sql     = "SELECT * FROM tbl_routeleg WHERE groupid=$groupid AND code IN ($tripStr) ";
		$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
		$result  = pg_query($db_conn, $sql);
		$rows    = pg_fetch_all($result);
		$tripArr = explode("-", $trip);
		$tmp 	 = "";
		foreach($tripArr AS $t){
			if ($tmp == "") {
				$tmp = rtrim(ltrim($t));
			}
			else{
				$legDist = 0;
				$leg = $tmp."-".$t;
				$tmp = rtrim(ltrim(str_replace("(L)", "", str_replace("(E)", "",$t))));

				foreach($rows AS $r){
					if($leg ==$r['code']."(L)") {
					    $totDist += $r['distance'];
						$totLoad += $r['distance'];
						continue;
					}
					elseif($leg == $r['code']."(E)") {
						$totDist += $r['distance'];
						continue;
					}
				}
				
			}
		}
		// echo $totDist." vs ".$totLoad;
		// exit;
		return $totDist>0 ? (100*$totLoad/$totDist) : 0;
	}

	return "0";
}

function calc_load_percent_new($groupid, $trip, $dbh, $dbn, $dbu, $dbp){
	$tripArr = explode("-", preg_replace("/\([0-9]+T\)/","",str_replace("(E)","",$trip)));
	$tripStr = "";
	if ($tripArr) {
		$tmp = "";
		foreach($tripArr AS $t){
			if ($tmp == "") {
				$tmp = rtrim(ltrim($t));
			}
			else{
				if($tripStr!=""){ $tripStr .= ","; }
				$tripStr .= "'".$tmp."-".$t."'";
				$tmp = rtrim(ltrim($t));
			}
		}
	}
	if ($tripStr!="") {
		$totDist = 0;
		$totLoad = 0;
		$sql     = "SELECT * FROM tbl_routeleg WHERE groupid=$groupid AND code IN ($tripStr) ";
		$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
		$result  = pg_query($db_conn, $sql);
		$rows    = pg_fetch_all($result);
		$tripArr = explode("-", $trip);
		$tmp 	 = "";
		foreach($tripArr AS $t){
			if ($tmp == "") {
				$tmp = rtrim(ltrim($t));
			}
			else{
				$legDist = 0;
				$leg = $tmp."-".$t;
				$tmp = rtrim(ltrim(preg_replace("/\([0-9]+T\)/", "", str_replace("(E)", "",$t))));

				foreach($rows AS $r){
					if(stristr($leg, "T)")) {
					    $totDist += $r['distance'];
						$totLoad += $r['distance'];
						continue;
					}
					elseif(stristr($leg, "(E)")) {
						$totDist += $r['distance'];
						continue;
					}
				}
				
			}
		}
		return $totDist>0 ? (100*$totLoad/$totDist) : 0;
	}

	return "0";
}
function list_group_legs($groupid, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT l.*, 
					(SELECT f.name FROM tblgroup f WHERE f.groupid=$groupid) AS fmgroup,
					(SELECT g.client_account FROM tblclient_gt g WHERE g.client_id=$groupid) AS gtgroup
	 			  FROM tbl_routeleg l WHERE l.groupid=$groupid ORDER BY leg";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function update_leg($id,$groupid,$leg,$code,$distance,$loaded,$empty,$dbh,$dbn,$dbu,$dbp){

	$sql 	  = "UPDATE tbl_routeleg SET groupid=?,leg=?,code=?,distance=?,duration_loaded=?,duration_empty=? WHERE legid=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $leg);
	$stmt     ->bindParam(3,  $code);
	$stmt     ->bindParam(4,  $distance);
	$stmt     ->bindParam(5,  $loaded);
	$stmt     ->bindParam(6,  $empty);
	$stmt     ->bindParam(7,  $id);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function delete_leg($legid,$dbh,$dbn,$dbu,$dbp){
	$sql 	  = "DELETE FROM tbl_routeleg WHERE legid=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $legid);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
// --- END LEGS ---


// TRANSIT TARGETS
function save_transit_target($groupid,$a,$b,$distance,$duration,$dbh,$dbn,$dbu,$dbp){
	$sql 	  = "INSERT INTO tbl_transittarget (groupid,position_a,position_b,distance,duration,created) 
					VALUES(?, ?, ?, ?, ?, ?);";
	
	$created  = Date('Y-M-d H:i:s');
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $a);
	$stmt     ->bindParam(3,  $b);
	$stmt     ->bindParam(4,  $distance);
	$stmt     ->bindParam(5,  $duration);
	$stmt     ->bindParam(6,  $created);

	if($stmt->execute()){
		$_id  = $db_conn->lastInsertId();
        return array('result'=>true,'id'=>$_id,'message'=>'Transit target saved.');
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function get_transit_target($id, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT t.*, g.name AS groupname FROM tbl_transittarget t, tblgroup g WHERE t.id=$id AND t.groupid=g.groupid ";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function list_transit_targets($groupid, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT t.*, g.name AS groupname FROM tbl_transittarget t, tblgroup g WHERE t.groupid=g.groupid ";
	if($groupid!=0) { $sql .= " AND t.groupid=$groupid "; } 
	$sql 	.= "ORDER BY groupname, position_a, position_b";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function update_transit_target($id,$a,$b,$distance,$duration,$dbh,$dbn,$dbu,$dbp){
	$sql 	  = "UPDATE tbl_transittarget SET position_a=?,position_b=?,distance=?,duration=? WHERE id=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $a);
	$stmt     ->bindParam(2,  $b);
	$stmt     ->bindParam(3,  $distance);
	$stmt     ->bindParam(4,  $duration);
	$stmt     ->bindParam(5,  $id);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function delete_transit_target($id,$dbh,$dbn,$dbu,$dbp){
	$sql 	  = "DELETE FROM tbl_transittarget WHERE id=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $id);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function save_custom_transit_event($groupid,$assetid,$driverid,$typeid,$datetime,$odo,$location,$dbh,$dbn,$dbu,$dbp){
	$sql 	  = "INSERT INTO tblevent (eventid,groupid,assetid,driverid,eventtypeid,startdatetime,value,locationname) 
					VALUES(?, ?, ?, ?, ?, ?, ?, ?);";
	
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$evId 	  = 0 - strtotime("now");
	$evDateTm = gmdate('Y-m-d H:i:s', strtotime($datetime));

	$stmt     ->bindParam(1,  $evId);
	$stmt     ->bindParam(2,  $groupid);
	$stmt     ->bindParam(3,  $assetid);
	$stmt     ->bindParam(4,  $driverid);
	$stmt     ->bindParam(5,  $typeid);
	$stmt     ->bindParam(6,  $evDateTm);
	$stmt     ->bindParam(7,  $odo);
	$stmt     ->bindParam(8,  $location);

	if($stmt->execute()){
        return array('result'=>true,'info'=>'Custom transit checkpoint event saved.', 'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function list_transit_positions($groupid, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT DISTINCT locationname FROM tblevent WHERE groupid=$groupid AND locationname LIKE 'CP %' AND startdatetime >= CURRENT_DATE-60 ORDER BY locationname ";

	// echo $sql;
	// exit; 
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function list_asset_transit_positions($assetid, $spikes, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT e.*, l.description AS event FROM tblevent e, tbllibraryevent l WHERE e.eventtypeid=l.eventtypeid AND assetid=$assetid AND locationname LIKE 'CP %' AND startdatetime>='$start' AND startdatetime<='$end' ORDER BY startdatetime ";

	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
// --- END TRANSITS ---


// JOB CARDS
function generate_next_jc_number($groupid,$gcfg,$dbh,$dbn,$dbu,$dbp) {
	$prefix  = "";
	foreach ($gcfg as $g) {
		if ($g['id']==$groupid) {
			$prefix = $g['jcprefix'];
			continue;
			$num   = intval(str_replace($g['jcprefix'],"",$row[0]['jobcard']));
			if ($num<10)  	 { $newJc .= "00".$num; 	}
			elseif ($num<100){ $newJc .= "0".$num; 	}
			else 			 { $newJc .= $num; 		}
		}
	}
	$sql     = "SELECT id, jobcard FROM tbl_jobcard WHERE groupid=$groupid ORDER BY id DESC LIMIT 1";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	if ($row) {
		$newJc = $prefix;
		$jcNum = intval(str_replace($prefix,"",$row[0]['jobcard']));
		if ($jcNum<10)  	 { $newJc .= "00".($jcNum+1); 	}
		elseif ($jcNum<100){ $newJc .= "0".($jcNum+1); 	}
		else 			 { $newJc .= ($jcNum+1); 		}

		return $newJc;
	}
	else{
		return $prefix."001";
	}
}
function save_job_card($usrid,$groupid,$assetid,$techid,$order,$type,$serial,$fault,$work,$comments,$travel,$labour,$parts,$gcfg,$dbh,$dbn,$dbu,$dbp){
	$jc 	  = generate_next_jc_number($groupid,$gcfg,$dbh,$dbn,$dbu,$dbp);
	$sql 	  = "INSERT INTO tbl_jobcard (userid,groupid,assetid,technicianid,jobcard,unittype,serial,fault,work,comments,travel,
					labour,parts,ordernum,created) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	
	$created  = Date('Y-M-d H:i:s');
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $usrid);
	$stmt     ->bindParam(2,  $groupid);
	$stmt     ->bindParam(3,  $assetid);
	$stmt     ->bindParam(4,  $techid);
	$stmt     ->bindParam(5,  $jc);
	$stmt     ->bindParam(6,  $type);
	$stmt     ->bindParam(7,  $serial);
	$stmt     ->bindParam(8,  $fault);
	$stmt     ->bindParam(9,  $work);
	$stmt     ->bindParam(10, $comments);
	$stmt     ->bindParam(11, $travel);
	$stmt     ->bindParam(12, $labour);
	$stmt     ->bindParam(13, $parts);
	$stmt     ->bindParam(14, $order);
	$stmt     ->bindParam(15, $created);

	if($stmt->execute()){
        $_id  = $db_conn->lastInsertId();
        return array('result'=>true,'id'=>$_id,'message'=>'Job card saved.');
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function get_job_card($id, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT j.*, 
					(SELECT STRING_AGG(description || ':' || registrationnumber, ':') asst 
						FROM tblasset a WHERE a.assetid=j.assetid) AS fm_asset, 
					(SELECT g.name FROM tblgroup g WHERE g.groupid=j.groupid) AS fm_group,    
					(SELECT unit_name FROM tblunit_gt u WHERE u.unit_id=j.assetid) AS gt_asset,    
					(SELECT client_account FROM tblclient_gt c WHERE c.client_id=j.groupid) AS gt_group    
					FROM tbl_jobcard j WHERE id=$id ";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	if ($row) {
		return array('result'=>true,'jobcard'=>$row[0]);
	}
	return array('result'=>false,'error'=>"Not found");
}
function list_job_cards($groupid, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT j.*, 
					(SELECT name FROM tblgroup f WHERE j.groupid=f.groupid) AS fmgroup, 
					(SELECT client_account FROM tblclient_gt g WHERE j.groupid=g.client_id) AS gtgroup, 
					(SELECT description FROM tblasset h WHERE j.assetid=h.assetid) AS fmasset, 
					(SELECT unit_name FROM tblunit_gt u WHERE j.assetid=u.unit_id) AS gtasset,   
					(SELECT name FROM tbl_user a WHERE j.userid=a.id) AS author,   
					(SELECT name FROM tbl_user t WHERE j.technicianid=t.id) AS tech    
				FROM tbl_jobcard j  ";
	if($groupid!=0) { $sql .= " WHERE j.groupid=$groupid "; } 
	$sql 	.= " ORDER BY created DESC";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function update_job_card($id,$groupid,$assetid,$techid,$type,$serial,$fault,$work,$comments,$travel,$labour,$parts,$order,$dbh,$dbn,$dbu,$dbp){
	$sql 	  = "UPDATE tbl_jobcard SET groupid=?,assetid=?,technicianid=?,unittype=?,serial=?,fault=?,work=?,comments=?,
					travel=?,labour=?,parts=?,ordernum=? WHERE id=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $assetid);
	$stmt     ->bindParam(3,  $techid);
	$stmt     ->bindParam(4,  $type);
	$stmt     ->bindParam(5,  $serial);
	$stmt     ->bindParam(6,  $fault);
	$stmt     ->bindParam(7,  $work);
	$stmt     ->bindParam(8,  $comments);
	$stmt     ->bindParam(9,  $travel);
	$stmt     ->bindParam(10, $labour);
	$stmt     ->bindParam(11, $parts);
	$stmt     ->bindParam(12, $order);
	$stmt     ->bindParam(13, $id);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function delete_job_card($id,$dbh,$dbn,$dbu,$dbp){
	$sql 	  = "DELETE FROM tbl_jobcard WHERE id=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $id);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
// --- END JC ---

//Decomission

function save_decomission($group,$groupType,$asset, $peripherals, $terminationnumber,$ticketurl,$decomcomment, $senpro_api, $dbh,$dbn,$dbu,$dbp){
	$headers = array(
		'Content-Type: multipart/form-data',
	);
	$docs = array();
	$target = $_FILES['decomDocument']; 
	foreach($_FILES["decomDocument"]["name"] as $index => $value){
		$name = $_FILES["decomDocument"]["name"][$index];
		$file_type = $_FILES["decomDocument"]["type"][$index];
		$tmp_name = $_FILES["decomDocument"]["tmp_name"][$index];
		$docs[$index] = curl_file_create($tmp_name, $file_type, $name);
	}
	$url = $senpro_api."decomissions";
	$post_data = array(
		"userid" => $_SESSION['user']['id'],
		"groupname" => $group,
		"username" => $_SESSION['user']['name'],
		"grouptype" => $groupType,
		"asset" => $asset,
		"peripherals" => $peripherals,
		"terminationnumber" => $terminationnumber,
		"decomdocument" => json_encode($docs),
		"ticketurl" => $ticketurl,
		"decomcomment" => $decomcomment
	);  
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true, 
		CURLOPT_SSL_VERIFYHOST => false,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_POST => true,   
		CURLOPT_POSTFIELDS => $post_data 
	));
	if (!empty($headers)) {
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	}
	$response = curl_exec($curl);
	$errno = curl_errno($curl);
	if ($errno) {
		return false;
	}
	curl_close($curl);
	return json_decode($response, true);
}
function get_decomission($id, $senpro_api, $dbh, $dbn, $dbu, $dbp){
    $url = $senpro_api."decomissions/".$id;
    $client   = curl_init($url);
    curl_setopt($client,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($client, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($client);
    return json_decode($response, true);
}
function list_decomissions($senpro_api, $start, $end, $dbh, $dbn, $dbu, $dbp){
	if(isset($start)){
		$url = $senpro_api."decomissions?start=".$start.'&end='.$end;
	} else {
		$url = $senpro_api."decomissions";
	}
	$client   = curl_init($url);
	curl_setopt($client,CURLOPT_RETURNTRANSFER,true);
	curl_setopt($client, CURLOPT_SSL_VERIFYPEER, false);
	$response = curl_exec($client);
	return json_decode($response, true);
}
function update_decomission($id, $group,$grouptype,$asset,$peripherals,$terminationnumber,$ticketurl,$decomcomment, $senpro_api){
    $url = $senpro_api."decomissions/".$id;
    $headers = array('Content-Type: application/json');
    $post_data = array(
        "userid" => $_SESSION['user']['id'],
        "groupname" => $group,
        "username" => $_SESSION['user']['name'],
        "grouptype" => $grouptype,
        "asset" => $asset,
		"peripherals" => $peripherals,
        "terminationnumber" => $terminationnumber,
        "ticketurl" => $ticketurl,
        "decomcomment" => $decomcomment
    );  
    $curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($post_data));
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    $response = curl_exec($curl);
    curl_close($curl); 
	return json_decode($response, true);
}
function delete_decomission($id, $senpro_api, $dbh,$dbn,$dbu,$dbp){
    $url = $senpro_api."decomissions/".$id;
    $client = curl_init();
    curl_setopt($client, CURLOPT_URL, $url);
    curl_setopt($client, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($client, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($client, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($client, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($client);
    curl_close($client);
	return json_decode($result, true);
}
// --- EndDecomission ---


// TRAILERS
function save_trailer($groupid,$name,$type,$desc,$mileage,$make,$model,$reg,$dbh,$dbn,$dbu,$dbp){
	$sql 	  = "INSERT INTO tbl_trailer (groupid,name,type,description,mileage,status,created,make,model,registration) VALUES(?,?,?,?,?,?,?,?,?,?);";
	
	$created  = Date('Y-M-d H:i:s');
	$status   = 'ACTIVE';
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $name);
	$stmt     ->bindParam(3,  $type);
	$stmt     ->bindParam(4,  $desc);
	$stmt     ->bindParam(5,  $mileage);
	$stmt     ->bindParam(6,  $status);
	$stmt     ->bindParam(7,  $created);
	$stmt     ->bindParam(8,  $make);
	$stmt     ->bindParam(9,  $model);
	$stmt     ->bindParam(10, $reg);

	if($stmt->execute()){
        $_id  = $db_conn->lastInsertId();
        return array('result'=>true,'id'=>$_id,'message'=>'Trailer saved.');
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function get_trailer($id, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT t.*, 
					(SELECT f.name FROM tblgroup f WHERE f.groupid=t.groupid) AS fmgroup,
					(SELECT g.client_account FROM tblclient_gt g WHERE g.client_id=t.groupid) AS gtgroup   
				  FROM tbl_trailer t WHERE id=$id ";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	if ($row) {
		return array('result'=>true,'trailer'=>$row[0]);
	}
	return array('result'=>false,'error'=>"Not found");
}
function list_trailers($groupid, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT t.*, 
					(SELECT f.name FROM tblgroup f WHERE f.groupid=t.groupid) AS fmgroup, 
					(SELECT g.client_account FROM tblclient_gt g WHERE g.client_id=t.groupid) AS gtgroup, 
					(SELECT MAX(s.service_date) FROM tbl_trailerservice s WHERE s.trailerid=t.id) AS last_svc_date, 
					(SELECT MAX(s1.mileage) FROM tbl_trailerservice s1 WHERE s1.trailerid=t.id) AS last_svc_odo  
				  FROM tbl_trailer t";
	if(intval($groupid)!=0) { $sql .= " WHERE groupid=$groupid "; } 
	$sql 	.= " ORDER BY name";
	// echo $sql;
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function save_trailer_service($groupid,$id,$userid,$type,$mileage,$service_date,$notes, $dbh,$dbn,$dbu,$dbp){
	$sql 	  = "INSERT INTO tbl_trailerservice (groupid,trailerid,userid,service_type,mileage,service_date,notes,created) VALUES(?,?,?,?,?,?,?,?);";
	
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$created  = Date('Y-M-d H:i:s');

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $id);
	$stmt     ->bindParam(3,  $userid);
	$stmt     ->bindParam(4,  $type);
	$stmt     ->bindParam(5,  $mileage);
	$stmt     ->bindParam(6,  $service_date);
	$stmt     ->bindParam(7,  $notes);
	$stmt     ->bindParam(8,  $created);

	if($stmt->execute()){
        $_id  = $db_conn->lastInsertId();
        return array('result'=>true,'id'=>$_id,'message'=>'Trailer service captured.');
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function update_trailer($id,$groupid,$name,$type,$desc,$mileage,$make,$model,$reg,$dbh,$dbn,$dbu,$dbp){
	$sql 	  = "UPDATE tbl_trailer SET groupid=?,name=?,type=?,description=?,mileage=?,make=?,model=?,registration=? WHERE id=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $name);
	$stmt     ->bindParam(3,  $type);
	$stmt     ->bindParam(4,  $desc);
	$stmt     ->bindParam(5,  $mileage);
	$stmt     ->bindParam(6,  $make);
	$stmt     ->bindParam(7,  $model);
	$stmt     ->bindParam(8,  $reg);
	$stmt     ->bindParam(9,  $id);

	if($stmt->execute()){
        return array('result'=>true,'message'=>$stmt->rowCount()." row(s) updated");
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function delete_trailer($id,$dbh,$dbn,$dbu,$dbp){
	$sql 	  = "UPDATE tbl_trailer SET status='DELETED' WHERE id=?;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $id);

	if($stmt->execute()){
        return array('result'=>true,'message'=>$stmt->rowCount()." row(s) updated");
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
// --- END TRAILERS ---


// USERS
function save_user($name, $type, $email, $username, $fm, $gt, $dbh, $dbn, $dbu, $dbp){
	$fm 	  = is_numeric($fm) ? $fm : 0;
	$gt 	  = is_numeric($gt) ? $gt : 0;

	$sql 	  = "INSERT INTO tbl_user (name, type, email, status, username, password, created, fm, gt) 
					VALUES(?, ?, ?, 'ACTIVE', ?, ?, ?, ?, ?);";
	
	$created  = Date('Y-M-d H:i:s');
	$password = md5('sendempass');
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $name);
	$stmt     ->bindParam(2,  $type);
	$stmt     ->bindParam(3,  $email);
	$stmt     ->bindParam(4,  $username);
	$stmt     ->bindParam(5,  $password);
	$stmt     ->bindParam(6,  $created);
	$stmt     ->bindParam(7,  $fm);
	$stmt     ->bindParam(8,  $gt);

	if($stmt->execute()){
        $_id  = $db_conn->lastInsertId();
        return array('result'=>true,'id'=>$_id,'message'=>'User has been created');
    }
    else{
    	$errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function save_user_old($groupid, $name, $type, $email, $username, $dbh, $dbn, $dbu, $dbp){

	$sql 	  = "INSERT INTO tbl_user (groupid, name, type, email, status, username, password, created) 
					VALUES(?, ?, ?, ?, 'ACTIVE', ?, ?, ?);";
	
	$created  = Date('Y-M-d H:i:s');
	$password = md5('sendempass');
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $name);
	$stmt     ->bindParam(3,  $type);
	$stmt     ->bindParam(4,  $email);
	$stmt     ->bindParam(5,  $username);
	$stmt     ->bindParam(6,  $password);
	$stmt     ->bindParam(7,  $created);

	$stmt	  ->execute();

	return $stmt->rowCount();
}
function login_user($user_id, $pwd, $senpro_api){
	$url = $senpro_api."v1/internallogin";
	$postData = array(
		'username' => $user_id,
		'password' => $pwd,
	);
	$headers = array(
		'Content-Type: application/json'
	);
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true, 
		CURLOPT_SSL_VERIFYHOST => false,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_POST => true,   
		CURLOPT_POSTFIELDS => json_encode($postData) 
	));
	if (!empty($headers)) {
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	}
	$response = curl_exec($curl);
	$errno = curl_errno($curl);
	if ($errno) {
		return false;
	}
	curl_close($curl);
	$res = json_decode($response, true);

	if (isset($res['Error'])) {
		return false;
	}
	var_dump($res);
	if($res['User']){
		$user 					 = $res['User'];
		$_SESSION['user'] 		 = $user;
		$_SESSION['token'] 		 = $res['Access Token'];
		if ($user['type'] != 'ADMINISTRATOR') {
			if ($user['fm']) {
				$_SESSION['fm']  = $user['fm'];
			} 
			if ($user['gt']) {
				$_SESSION['gt']  = $user['gt'];
			}		
		}		
		
		if($pwd == 'sendempass'){
			$_SESSION['changepwd']	= true;
		}
		// return true;
	}else{
		var_dump('sd');
		// return false;
	}
}
function change_user_password($id, $newpwd, $oldpwd, $senpro_api, $token){
	$postData = array(
		'oldpwd' => $oldpwd,
		'password' => $newpwd
	);
	$url = $senpro_api."v1/users/".$id;
	$headers = array(
		'Content-Type: application/json',
		'Authorization: Bearer '.$token
	);
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL 			=> $url,
		CURLOPT_RETURNTRANSFER 	=> true,
		CURLOPT_SSL_VERIFYHOST	=> false,
		CURLOPT_SSL_VERIFYPEER 	=> false, 
		CURLOPT_CUSTOMREQUEST	=> 'PUT', 
		CURLOPT_POSTFIELDS 		=> json_encode($postData),
		CURLOPT_HTTPHEADER 		=> $headers
	));
	$response = curl_exec($curl);
	$res = json_decode($response, true);
	$errno = curl_errno($curl);
	if ($errno) {
		return array('result'=>false,'error'=>curl_error($curl));
	}
	curl_close($curl);
	if($res['success']){
		if($newpwd != 'sendempass'){
			unset($_SESSION['changepwd']);
		}
		return true;
	} else {
		return false;
	}
}
function list_users($senpro_api, $token){
	$url = $senpro_api."v1/users";
	$headers = array(
		'Content-Type: application/json',
		'Authorization: Bearer '.$token
	);
    $curl   = curl_init($url);
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    $response = curl_exec($curl);
    return json_decode($response, true);
}
function get_group_utype($groupid,$userRoles,$dbh, $dbn, $dbu, $dbp){
	$roles = implode("','", $userRoles);
	$sql     = "SELECT u.*, 
					(SELECT name FROM tblgroup f WHERE u.fm=f.groupid) AS fmgroup, 
					(SELECT client_account FROM tblclient_gt g WHERE u.gt=g.client_id) AS gtgroup,  
					(SELECT added FROM tbl_activity a WHERE u.id=a.userid ORDER BY added DESC LIMIT 1) AS lastactive  
				FROM tbl_user u WHERE status<>'DELETED' AND u.type IN ('$roles') AND (u.fm=$groupid OR u.gt=$groupid)  
				ORDER BY u.name";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
function get_user($id, $senpro_api, $token){

	$url = $senpro_api."v1/users/".$id;
	$headers = array(
		'Content-Type: application/json',
		'Authorization: Bearer '.$token
	);
    $curl   = curl_init($url);
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    $response = curl_exec($curl);
    $res = json_decode($response, true);

	if (is_array($res)) {
		return $res;
	} else {
		return false;
	}	
}
function update_user($id, $name, $type, $senpro_api, $token){
	$postData = array(
		'name' => $name,
		'type' => $type
	);
	$url = $senpro_api."v1/users/".$id;
	$headers = array(
		'Content-Type: application/json',
		'Authorization: Bearer '.$token
	);
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL 			=> $url,
		CURLOPT_RETURNTRANSFER 	=> true,
		CURLOPT_SSL_VERIFYHOST	=> false,
		CURLOPT_SSL_VERIFYPEER 	=> false, 
		CURLOPT_CUSTOMREQUEST	=> 'PUT', 
		CURLOPT_POSTFIELDS 		=> json_encode($postData),
		CURLOPT_HTTPHEADER 		=> $headers
	));
	curl_exec($curl);
	$errno = curl_errno($curl);
	if ($errno) {
		return array('result'=>false,'error'=>curl_error($curl));
	}
	curl_close($curl);
	return array('result'=>true, 'message'=> 'User has been updated.');
}
function reset_user_pwd($id, $senpro_api, $token){
	$postData = array(
		'password' => 'sendempass',
	);
	$url = $senpro_api."v1/users/".$id;
	$headers = array(
		'Content-Type: application/json',
		'Authorization: Bearer '.$token
	);
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL 			=> $url,
		CURLOPT_RETURNTRANSFER 	=> true,
		CURLOPT_SSL_VERIFYHOST	=> false,
		CURLOPT_SSL_VERIFYPEER 	=> false, 
		CURLOPT_CUSTOMREQUEST	=> 'PUT', 
		CURLOPT_POSTFIELDS 		=> json_encode($postData),
		CURLOPT_HTTPHEADER 		=> $headers
	));
	curl_exec($curl);
	$errno = curl_errno($curl);
	if ($errno) {
		return array('result'=>false,'error'=>curl_error($curl));
	}
	curl_close($curl);
	return array('result'=>true, 'message'=> 'User password has been reset');
}
function delete_user($id, $senpro_api, $token){
	$url = $senpro_api."v1/users/".$id;
	$headers = array(
		'Content-Type: application/json',
		'Authorization: Bearer '.$token
	);
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL 			=> $url,
		CURLOPT_RETURNTRANSFER 	=> true,
		CURLOPT_SSL_VERIFYHOST => false,
		CURLOPT_SSL_VERIFYPEER => false, 
		CURLOPT_CUSTOMREQUEST	=> "DELETE",  
		CURLOPT_HTTPHEADER 		=> $headers
	));
	curl_exec($curl);
	$errno = curl_errno($curl);
	if ($errno) {
		return array('result'=>false,'error'=>curl_error($curl));
	}
	curl_close($curl);
	return array('result'=>true, 'message'=> 'Deleted');
}

function save_activity($userid, $activity, $dbh, $dbn, $dbu, $dbp){

	$sql 	  = "INSERT INTO tbl_activity (userid, activity, added) VALUES(?, ?, ?);";
	
	$created  = gmdate("Y-m-d H:i:s"); 
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $userid);
	$stmt     ->bindParam(2,  $activity);
	$stmt     ->bindParam(3,  $created);

	if($stmt->execute()){
        $_id  = $db_conn->lastInsertId();
        return array('result'=>true,'id'=>$_id,'message'=>'User activity saved.');
    }
    else{
    	$errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}
function list_activity($dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT a.*, u.name AS user       
					FROM tbl_activity a, tbl_user u      
					WHERE a.userid=u.id ";
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$row     = pg_fetch_all($result);

	return $row;
}
// --- END USERS ---

function get_id_array($group_id, $table_name, $column, $dbh, $dbn, $dbu, $dbp){
	$sql     = "SELECT $column FROM $table_name";
	if(!empty($group_id)){ $sql = $sql ." WHERE groupid=$group_id"; }
	$db_conn = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$result  = pg_query($db_conn, $sql);
	$arr     = array();
	while ($row = pg_fetch_assoc($result)) {
			$arr[] = $row[$column];
	}
	return $arr;
}

function get_asset_id_array($dbh, $dbn, $dbu, $dbp){
	return get_id_array("tblasset", "assetid", $dbh, $dbn, $dbu, $dbp);
}

function get_driver_id_array($dbh, $dbn, $dbu, $dbp){
	return get_id_array("tbldriver", "driverid", $dbh, $dbn, $dbu, $dbp);
}

function get_vehicle_byid($vehicle_id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT * FROM tblasset WHERE assetid=$vehicle_id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_assoc($db_result);

	return $row;
}

function get_depot_byid($depot_id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT * FROM tbl_depot WHERE id=$depot_id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_assoc($db_result);

	return $row;
}

function get_leg_byid($leg_id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT * FROM tbl_routeleg WHERE legid=$leg_id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_assoc($db_result);

	return $row;
}

function get_product_byid($prod_id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT * FROM tbl_product WHERE id=$prod_id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_assoc($db_result);

	return $row;
}

function get_group_customer($custid, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT * FROM tbl_customer WHERE id=$custid";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_assoc($db_result);

	return $row;
}

function get_driver_name($driver_id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT name FROM tbldriver WHERE driverid=$driver_id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_assoc($db_result);

	return $row['name'];
}

function get_asset_name($asset_id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT description FROM tblasset WHERE assetid=$asset_id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_assoc($db_result);

	return $row['description'];
}

function get_product_name($product_id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT name FROM tbl_product WHERE id=$product_id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_assoc($db_result);

	return $row['name'];
}

function get_depot_name($depot_id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT name FROM tbl_depot WHERE id=$depot_id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_assoc($db_result);

	return $row['name'];
}

function get_group_customer_name($customer_id, $dbh, $dbn, $dbu, $dbp){
	if ($customer_id=="") {
		return "Not Specified";
	}
	$sql       = "SELECT name FROM tbl_customer WHERE id=$customer_id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_assoc($db_result);

	if (is_array($row)) {
		return $row['name'];
	} else {
		return "Not Specified";
	}	
}

function get_organisation_ids($dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT groupid FROM tblgroup";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	return $row;
}

function get_fm_groups($dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT g.*, 
						(SELECT COUNT(a.assetid) FROM tblasset a WHERE a.groupid=g.groupid AND a.userstate<>'Decommissioned') AS assets,  
						(SELECT COUNT(d.driverid) FROM tbldriver d, tblsubgroup s WHERE d.groupid=g.groupid AND d.siteid=s.subgroupid AND s.name NOT LIKE '%Decommissioned%') AS drivers,  
						(SELECT startdatetime FROM tblevent e where startdatetime IS NOT NULL AND e.groupid=g.groupid AND startdatetime > CURRENT_DATE- interval '30 days' ORDER BY startdatetime DESC LIMIT 1) AS lastevent 
					FROM tblgroup g ORDER BY name";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	return $row;
}

function get_fm_group($group_id, $dbh, $dbn, $dbu, $dbp){
	$group_id  = addslashes($group_id);
	$sql       = "SELECT * FROM tblgroup WHERE groupid=$group_id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	if ($row) {
		return $row[0];
	}
	return array();
}

function get_gt_related_fm_group($gt_id, $dbh, $dbn, $dbu, $dbp){
	$group_id  = addslashes($group_id);
	$sql       = "SELECT g.* FROM tblgroup g, tbl_client_gt c WHERE client_id=$gt_id AND fm_groupid=g.groupid";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	if ($row) {
		return $row[0];
	}
	return array();
}

function get_drivers($dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT d.* FROM tbldriver d, tblsubgroup s WHERE d.siteid=s.subgroupid AND s.name NOT LIKE '%Decommissioned%'";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	return $row;
}

function get_asset_by_id($id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT * FROM tblasset WHERE assetid=$id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	return $row[0];
}

function get_trip_target_data($trip_legs, $groupid, $dbh, $dbn, $dbu, $dbp){
	$distance   = 0;
	$duration   = 0;

	$clear_trip = str_replace("(L)", "", $trip_legs);
	$clear_trip = str_replace("(E)", "", $clear_trip);

	$trip_arr   = explode("-", $trip_legs);
	$code_arr   = explode("-", $clear_trip);
	$legs  		= array();
	$legs_full	= array();
	$sql        = "SELECT * FROM tbl_routeleg WHERE groupid=$groupid AND (";
	// Build route leg combinations
	for($i=0; $i<count($code_arr)-1; $i++){
		if(isset($code_arr[$i+1])){
			$legs[] 	 = $code_arr[$i] ."-".$code_arr[$i+1];
			$legs_full[] = $code_arr[$i] ."-".$trip_arr[$i+1];
		}
	}
	// SQL fetch conditions (without duplicates)
	$route_legs = array_unique($legs);
	for($i=0; $i<count($route_legs); $i++){
		if($i>0){ $sql .= " OR ";}
		$sql  .= " code='$route_legs[$i]' ";
	}
	$sql 	  .= ")";
	// echo $sql;
	
	// Get route_leg data
	$db_conn    = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result  = pg_query($db_conn, $sql);
	$row        = pg_fetch_all($db_result);
	$legs_count = array_count_values($legs);
	$legf_count = array_count_values($legs_full);
	// Get actual values for return
	foreach($row as $route){
		$distance += $legs_count[$route['code']]*$route['distance'];
		if (isset($route['duration_loaded']) && isset($legf_count[$route['code'] ."(L)"])) {
			$duration += $legf_count[$route['code'] ."(L)"]*$route['duration_loaded'];
		}
		else{
			$duration += $legs_count[$route['code']]*$route['duration_empty'];
		}
	}
	
	$return_array = array("distance"=>$distance, "duration"=>$duration);

	return $return_array;
}

function calc_advanced_targets($groupid, $trip, $emptycons, $loadcons, $useVarCons, $maxLoad, $dbh, $dbn, $dbu, $dbp){
	$fuel   	 	= 0;
	$distance    	= 0;
	$duration    	= 0;
	$consumption 	= 0;

	if ($useVarCons) {
		$clear 		 	= preg_replace("/\([0-9]+T\)/", "", str_replace("(E)", "", $trip));
		$tripLegArr  	= explode("-", $trip);
		$clearLegArr 	= explode("-", $clear);

		$fullMiniTrips 	= array();
		$clearMiniTrips = array();

		$sql        	= "SELECT * FROM tbl_routeleg WHERE groupid=$groupid AND (";
		for($i=0; $i<count($tripLegArr)-1; $i++){
			if(isset($tripLegArr[$i+1])){
				$fullMiniTrips[]  = $tripLegArr[$i] ."-".$tripLegArr[$i+1];
				$clearMiniTrips[] = $clearLegArr[$i] ."-".$clearLegArr[$i+1];
			}
		}
		$uniqueLegs = array_unique($clearMiniTrips);
		for($i=0; $i<count($uniqueLegs); $i++){
			if($i>0){ $sql .= " OR ";}
			$sql  .= " code='$uniqueLegs[$i]' ";
		}
		$sql  .= ")";
		// echo $sql;

		$db_conn    = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
		$db_result  = pg_query($db_conn, $sql);
		$rows       = pg_fetch_all($db_result);

		for ($i=1; $i < count($fullMiniTrips); $i++) { 
			$leg1 	= substr($fullMiniTrips[$i], 0, strpos($fullMiniTrips[$i], "-"));
			$leg1 	= preg_replace("/\([0-9]+T\)/", "", str_replace("(E)", "", $leg1));
			$leg2 	= substr($fullMiniTrips[$i], strpos($fullMiniTrips[$i], "-")+1, strlen($fullMiniTrips[$i]));
			$fullMiniTrips[$i] = $leg1."-".$leg2;
		}

		foreach ($rows as $r) {
			foreach ($fullMiniTrips as $l) {
				if (stristr($l, $r['code'])) {
					$distance    	 += $r['distance'];
					if ($r['code'].'(E)'==$l) {
						$duration    += $r['duration_empty'];
						$consumption += $emptycons*$r['distance'];
						$fuel   	 += ($emptycons>0) ? $r['distance']/$emptycons : 0;
					}
					elseif (stristr($l, "T)") || $r['code'].'(L)'==$l) {
						$duration    += $r['duration_loaded'];
						if ($useVarCons) {
							$legLoads	  = 0;
							preg_match('#\((.*?)\)#', $l, $loadMatch);
							$legLoad      = isset($loadMatch[1]) ? str_replace("T", "", $loadMatch[1]) : 0;
							$cons         = $emptycons + (($legLoad/$maxLoad)*($loadcons-$emptycons));
							$consumption += $cons*$r['distance'];
							$fuel 		 += $cons>0 ? $distance/$cons : 0;
							
						} else{
							$consumption += $loadcons*$r['distance'];
							$fuel   	 += $loadcons>0 ? $distance/$loadcons : 0;
						}
					}
				}
			}
		}

	} else {
		$clear 		 	= str_replace("(L)", "", str_replace("(E)", "", $trip));
		$tripLegArr  	= explode("-", $trip);
		$clearLegArr 	= explode("-", $clear);

		$fullMiniTrips 	= array();
		$clearMiniTrips = array();

		$sql        	= "SELECT * FROM tbl_routeleg WHERE groupid=$groupid AND (";
		for($i=0; $i<count($tripLegArr)-1; $i++){
			if(isset($tripLegArr[$i+1])){
				$fullMiniTrips[]  = $tripLegArr[$i] ."-".$tripLegArr[$i+1];
				$clearMiniTrips[] = $clearLegArr[$i] ."-".$clearLegArr[$i+1];
			}
		}
		$uniqueLegs = array_unique($clearMiniTrips);
		for($i=0; $i<count($uniqueLegs); $i++){
			if($i>0){ $sql .= " OR ";}
			$sql  .= " code='$uniqueLegs[$i]' ";
		}
		$sql  .= ")";
		// echo $sql;

		$db_conn    = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
		$db_result  = pg_query($db_conn, $sql);
		$rows       = pg_fetch_all($db_result);

		for ($i=1; $i < count($fullMiniTrips); $i++) { 
			$leg1 	= substr($fullMiniTrips[$i], 0, strpos($fullMiniTrips[$i], "-"));
			$leg1 	= str_replace("(L)", "", str_replace("(E)", "", $leg1));
			$leg2 	= substr($fullMiniTrips[$i], strpos($fullMiniTrips[$i], "-")+1, strlen($fullMiniTrips[$i]));
			$fullMiniTrips[$i] = $leg1."-".$leg2;
		}

		foreach ($rows as $r) {
			foreach ($fullMiniTrips as $l) {
				if ($r['code'].'(E)'==$l) {
					$fuel   	 += ($emptycons>0) ? $r['distance']/$emptycons : 0;
					$distance    += $r['distance'];
					$duration    += $r['duration_empty'];
					$consumption += $emptycons*$r['distance'];
				}
				elseif ($r['code'].'(L)'==$l) {
					$fuel   	 += ($loadcons>0) ? $r['distance']/$loadcons : 0;
					$distance    += $r['distance'];
					$duration    += $r['duration_loaded'];
					$consumption += $loadcons*$r['distance'];
				}
			}
		}
	}
	
	return array("distance"=>$distance, "duration"=>$duration, "fuel"=>$fuel, "consumption"=>($consumption/$distance));
}

function get_asset_target_data($asset_id, $trip_legs, $dbh, $dbn, $dbu, $dbp){
	$asset 		= get_asset_by_id($asset_id, $dbh, $dbn, $dbu, $dbp);
	$load_cons  = $asset['targetfuelconsumption'];
	$empty_cons = $asset['targetfuelconsumption'];
	if (isset($asset['loadconsumption']) && $asset['loadconsumption']>0) {
		$load_cons  = $asset['loadconsumption'];
	}
	if (isset($asset['emptyconsumption']) && $asset['emptyconsumption']>0) {
		$empty_cons = $asset['emptyconsumption'];
	}

	$distance   = 0;
	$duration   = 0;
	$fuel       = 0;

	$clear_trip = str_replace("(L)", "", $trip_legs);
	$clear_trip = str_replace("(E)", "", $clear_trip);

	$trip_arr   = explode("-", $trip_legs);
	$code_arr   = explode("-", $clear_trip);
	$legs  		= array();
	$legs_full	= array();

	$sql        = "SELECT * FROM tbl_routeleg WHERE ";
	// Build route leg combinations
	for($i=0; $i<count($code_arr)-1; $i++){
		if(isset($code_arr[$i+1])){
			$legs[] 	 = $code_arr[$i] ."-".$code_arr[$i+1];
			$legs_full[] = $code_arr[$i] ."-".$trip_arr[$i+1];
		}
	}
	// SQL fetch conditions (without duplicates)
	$route_legs = array_unique($legs);
	for($i=0; $i<count($route_legs); $i++){
		if($i>0){ $sql .= " OR ";}
		$sql  .= " code='$route_legs[$i]' ";
	}
	// Get route_leg data
	$db_conn    = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");

	// echo $sql;
	// exit;

	$db_result  = pg_query($db_conn, $sql);
	$row        = pg_fetch_all($db_result);
	$legs_count = array_count_values($legs);
	$legf_count = array_count_values($legs_full);
	// Get actual values for return
	foreach($row as $route){
		$distance += $legs_count[$route['code']]*$route['distance'];
		if (isset($route['duration_loaded']) && isset($legf_count[$route['code'] ."(L)"])) {
			$duration += $legf_count[$route['code'] ."(L)"]*$route['duration_loaded'];
			$fuel 	  += $load_cons>0?(($legf_count[$route['code'] ."(L)"]*$route['distance'])/$load_cons):0;
		}
		else{
			$duration += $legs_count[$route['code']]*$route['duration_empty'];
			$fuel 	  += $empty_cons>0?(($legs_count[$route['code']]*$route['distance'])/$empty_cons):0;
		}
	}
	$return_array = array("distance"=>$distance, "duration"=>$duration, "fuel"=>$fuel);

	return $return_array;
}

function get_fm_assets($dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT * FROM tblasset WHERE userstate<>'Decommissioned'";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	return $row;
}

function get_fm_asset($asset_id, $dbh, $dbn, $dbu, $dbp){
	$asset_id = addslashes($asset_id);
	$sql       = "SELECT * FROM tblasset WHERE assetid=$asset_id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	if ($row) {
		return $row[0];
	}
	return array();
}

function get_fm_group_drivers($group_id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT d.* FROM tbldriver d, tblsubgroup s WHERE d.groupid=$group_id 
					AND d.siteid=s.subgroupid AND s.name NOT LIKE '%Decommissioned%' ORDER BY name";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	return $row;
}

function get_fm_driver($driver_id, $dbh, $dbn, $dbu, $dbp){
	$driver_id = addslashes($driver_id);
	$sql       = "SELECT d.* FROM tbldriver d, tblsubgroup s WHERE d.driverid=$driver_id 
					AND d.siteid=s.subgroupid AND s.name NOT LIKE '%Decommissioned%' ";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	if ($row) {
		return $row[0];
	}
	return array();
}

function get_fm_group_assets($group_id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT * FROM tblasset WHERE groupid=$group_id AND userstate<>'Decommissioned' ORDER BY description";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	return $row;
}

function get_tripsheet_by_id($id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT * FROM tbltripsheet WHERE tripsheetid=$id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	if ($row) {
		return $row;
	}
	return array();
}

function get_gt_tripsheet_by_id($id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT * FROM tbl_tripsheet WHERE tripsheetid=$id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	return $row;
}

function increment_version($version, $parent_id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT version FROM tbltripsheet WHERE parentid=$parent_id ORDER BY version DESC LIMIT 1";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);
	$ver_bump  = "1";
	if ($row) {
		$v 	 	  = explode(".", $row[0]['version']);
		$v 	 	  = array_reverse($v);
		$ver_bump = $v[0] + 1;
	}
	if ($version=="") { $version = "1"; }
	return $version .".". $ver_bump;
}

function get_gt_group_assets($client_id, $dbh, $dbn, $dbu, $dbp){
	$client_id = addslashes($client_id);
	$sql       = "SELECT DISTINCT ON (a.asset_id, a.unit_name) asset_id, a.unit_id, a.client_id, a.unit_name, g.client_account AS group, 
					a.targetemptyconsumption, a.targetloadedconsumption    
				  FROM tblunit_gt a, tblclient_gt g    
				  WHERE a.client_id=$client_id AND a.client_id=g.client_id AND a.asset_id IS NOT NULL  
				  ORDER BY unit_name, asset_id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	return $row;
}

function get_gt_asset($unit_id, $dbh, $dbn, $dbu, $dbp){
	$unit_id   = addslashes($unit_id);
	$sql       = "SELECT * FROM tblunit_gt WHERE asset_id=$unit_id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	if ($row) {
		return $row[0];
	}
	return array();
}

function get_gt_group_drivers($group_id, $dbh, $dbn, $dbu, $dbp){
	$group_id = addslashes($group_id);
	$sql       = "SELECT DISTINCT * FROM tbldriver_gt WHERE client_id=$group_id ORDER BY name";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	return $row;
}

function get_gt_driver($driver_id, $dbh, $dbn, $dbu, $dbp){
	$driver_id = addslashes($driver_id);
	$sql       = "SELECT * FROM tbldriver_gt WHERE driverid=$driver_id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	if ($row) {
		return $row[0];
	}
	return array();
}

function get_combined_groups($dbh, $dbn, $dbu, $dbp){
	$gt_groups = get_gt_groups($dbh, $dbn, $dbu, $dbp);
	$fm_groups = get_fm_groups($dbh, $dbn, $dbu, $dbp);
	$returnArr = array();

	if ($gt_groups) {
		foreach ($gt_groups as $g) {
			$returnArr[] = array('id'=>$g["client_id"],'name'=>$g["client_account"],'type'=>"GT",'assets'=>$g['assets'],'drivers'=>$g['drivers']);
		}
	}
	if ($fm_groups) {
		foreach ($fm_groups as $g) {
			$returnArr[] = array('id'=>$g["groupid"],'name'=>str_replace("Africa - Zimbabwe - ", "", $g['name']),'type'=>"FM",'assets'=>$g['assets'],'drivers'=>$g['drivers']);
		}
	}

	if ($returnArr) {
		// define a sorting column
		$name  = array_column($returnArr, 'name');
		array_multisort($name, SORT_ASC, $returnArr);
	}
	
	return $returnArr;
}

function filter_combined_group($groups, $key){
	if ($groups) {
		foreach ($groups as $g) {
			if ($g && $g['id']==$key) {
				return $g;
			}
		}
	}
	return array();
}

function get_gt_groups($dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT g.*, 
						(SELECT COUNT(DISTINCT u.unit_name) FROM tblunit_gt u WHERE u.client_id=g.client_id) AS assets,  
						(SELECT COUNT(d.driverid) FROM tbldriver_gt d WHERE d.client_id=g.client_id) AS drivers, 
						(SELECT datetime FROM tblposition_gt p where datetime IS NOT NULL AND p.client_id=g.client_id AND datetime > CURRENT_DATE- interval '30 days' ORDER BY datetime DESC LIMIT 1) AS lastposition   
					FROM tblclient_gt g ORDER BY client_account";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	return $row;
}

function get_gt_group($group_id, $dbh, $dbn, $dbu, $dbp){
	$group_id = addslashes($group_id);
	$sql       = "SELECT * FROM tblclient_gt WHERE client_id=$group_id";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	if ($row) {
		return $row[0];
	}
	return array();
}

function get_gt_units($dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT unit_id, unit_name FROM tblunit_gt ORDER BY unit_name";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	return $row;
}

function get_trip_legs($dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT * FROM tbl_routeleg ORDER BY leg";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	return $row;
}

function get_group_legs($group_id, $dbh, $dbn, $dbu, $dbp){
	$group_id = addslashes($group_id);
	$sql       = "SELECT * FROM tbl_routeleg WHERE groupid=$group_id ORDER BY leg";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	return $row;
}

function generate_short_trip($trip){
	$legsArr = explode("-", $trip);
	$legs 	 = count($legsArr);
	if ($legs<5) {
		return $trip;
	}
	else{
		$first 	= $legsArr[0];
		$f_ccat = stristr($legsArr[1], "(E)") ? "(E)" : "(L)";
		$last 	= $legsArr[$legs-1];
		$second = "";
		$third  = "";
		for ($i=2; $i < $legs; $i++) {
			if($second=="" && stristr($legsArr[$i],"(L)") && explode("(", $legsArr[$i-1])[0]!=$first){
				$second = explode("(", $legsArr[$i-1])[0];
			}
			if($second!="" && stristr($legsArr[$i],"(L)") && explode("(",$legsArr[$i-1])[0]!=$second){
				$third = explode("(", $legsArr[$i-1])[0];
			}
		}
		return "$first$f_ccat-$second(L)-$third(L)-$last";
	}
}

function get_products($dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT * FROM tbl_product ORDER BY name";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	return $row;
}

function get_group_products($group_id, $dbh, $dbn, $dbu, $dbp){
	$group_id  = addslashes($group_id);
	$sql       = "SELECT * FROM tbl_product WHERE groupid=$group_id ORDER BY name";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	return $row;
}

function get_customers($dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT * FROM tbl_customer ORDER BY name";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		return $rows;
	}
	return array();
}

function get_group_customers($group_id, $dbh, $dbn, $dbu, $dbp){
	$group_id  = addslashes($group_id);
	$sql       = "SELECT * FROM tbl_customer WHERE groupid=$group_id ORDER BY name";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		return $rows;
	}
	return array();
}

function get_loading_depots($dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT * FROM tbl_depot WHERE type='loading' OR type='both' ORDER BY name";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		return $rows;
	}
	return array();
}

function get_group_loading($group_id, $dbh, $dbn, $dbu, $dbp){
	$group_id  = addslashes($group_id);
	$sql       = "SELECT * FROM tbl_depot WHERE groupid=$group_id AND (type='loading' OR   
					type='both') ORDER BY name";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		return $rows;
	}
	return array();
}

function get_offload_depots($dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT * FROM tbl_depot WHERE type='offload' OR type='both' ORDER BY name";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		return $rows;
	}
	return array();
}

function get_group_offload($group_id, $dbh, $dbn, $dbu, $dbp){
	$group_id  = addslashes($group_id);
	$sql       = "SELECT * FROM tbl_depot WHERE groupid=$group_id AND (type='offload' OR   
					type='both') ORDER BY name";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		return $rows;
	}
	return array();
}

function get_events_by_driver($group_id, $driver_id, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT e.*, a.description AS asset, d.name AS driver, l.description AS eventtype 
					FROM tblevent e, tblasset a, tbldriver d, tbllibraryevent l 
					WHERE 
						a.assetid			= e.assetid  
						AND d.driverid		= e.driverid  
						AND l.eventtypeid	= e.eventtypeid  
						AND e.groupid 		= $group_id  
						AND e.driverid 		= $driver_id  
						AND startdatetime  >= '$start'  
						AND enddatetime    <= '$end'  
					ORDER BY startdatetime"; 
	// echo $sql;
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$row       = pg_fetch_all($db_result);

	return $row;
}

function get_events_by_asset($group_id, $asset_id, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT e.*, a.description AS asset, d.name AS driver, l.description AS eventtype 
					FROM tblevent e, tblasset a, tbldriver d, tbllibraryevent l 
					WHERE 
						a.assetid			= e.assetid 
						AND d.driverid		= e.driverid 
						AND l.eventtypeid	= e.eventtypeid 
						AND e.groupid		= $group_id 
						AND e.assetid		= $asset_id  
						AND (startdatetime  >= '$start' OR enddatetime >= '$start')  
						AND (enddatetime    <= '$end' OR startdatetime  <= '$end')   
					ORDER BY startdatetime"; 
	// echo $sql;
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_door_events_by_asset($group_id, $asset_id, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT e.*, a.description AS asset, d.name AS driver, l.description AS eventtype, 
						(SELECT formattedaddress FROM tblposition p WHERE p.positionsourceid=e.eventid LIMIT 1) AS locationname    
					FROM tblevent e, tblasset a, tbldriver d, tbllibraryevent l 
					WHERE 
						a.assetid			= e.assetid 
						AND d.driverid		= e.driverid 
						AND l.eventtypeid	= e.eventtypeid 
						AND l.description	LIKE '%Door Opened%'  
						AND e.groupid		= $group_id 
						AND e.assetid		= $asset_id  
						AND startdatetime  >= '$start' AND startdatetime <= '$end'   
					ORDER BY startdatetime"; 
	// echo $sql;
	// exit; 
	
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_gt_events($asset_id, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT p.* FROM tblposition_gt p, tblunit_gt u      
					WHERE u.asset_id=$asset_id    
						AND u.unit_name = p.unit_name        
						AND datetime  >= '$start'     
						AND datetime  <= '$end'    
					ORDER BY datetime"; 
	// echo $sql;
	// exit;
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_gt_fuel($events, $dbh, $dbn, $dbu, $dbp){
	$positionIds = "";
	if ($events) {
		$i = 0;
		foreach($events as $e){
			if($i>0){ $positionIds .= ","; }
			$positionIds .= $e["position_id"];
			$i++;
		}
	}
	$sql       = "SELECT * FROM tblfuel_gt WHERE start_position_id IN ($positionIds) OR start_position_id IN ($positionIds) 
					ORDER BY start_time, end_time"; 
	// echo $sql;
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_gt_tripsheet_events($ts_id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT p.* FROM tbltripsheet t, tblposition_gt p               
					WHERE  
						tripsheetid=$ts_id      
						AND t.assetid = p.unit_id  
						AND p.datetime >= t.start  
						AND p.datetime <= t.end    
					ORDER BY datetime";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_gt_distance($events, $spikes){
	// $returnDist = 0;
	$odoHigh 	= 0;
	$odoLow 	= 0;
	if (is_array($events)) {
		foreach($events as $e){
			if (!in_array($e['position_id'], $spikes)) {
				if ($odoHigh<$e['odometer']) { 
					$odoHigh	= $e['odometer']; 
					if ($odoLow ==0) { 
						$odoLow = $e['odometer']; 
					}
				}
				if ($odoLow > $e['odometer'] && $e['odometer']>0) { 
					$odoLow = $e['odometer']; 
				}
				// $returnDist += $e['distance']; 
			}
		}
	}
	return $odoHigh-$odoLow;
}

function extract_gt_movement_report($events, $spikes){
	$movementReport 	= array();
	$fullTrip 			= array();
	if (is_array($events)) {
		foreach($events as $e){
			if (!in_array($e['position_id'], $spikes) && ($e['event']=='trip start' || $e['event']=='trip stop')) {
				if ($e['event']=='trip start') {
					if ($fullTrip) {
						$movementReport[] = $fullTrip;
					} 
					$fullTrip = array(
                        "start_pos" =>$e['position_text'],
                        "start_dt"  =>$e['datetime'],
                        "start_odo" =>$e['odometer'],
                        "stop_pos"  =>'',
                        "stop_dt"   =>'',
                        "stop_odo"  =>'',
                        "stop_end"  =>$e['datetime']
                    );
				}
				elseif ($e['event']=='trip stop') {
					if ($fullTrip && $fullTrip['stop_dt']!='') {
						$movementReport[] 	  = $fullTrip;
						$fullTrip 			  = array();
					} 
					elseif ($fullTrip) {
						$fullTrip['stop_pos'] = $e['position_text'];
						$fullTrip['stop_dt']  = $e['datetime'];
						$fullTrip['stop_odo'] = $e['odometer'];
						$movementReport[] 	  = $fullTrip;
						$fullTrip 			  = array();
					}
					elseif (!$fullTrip){
						$fullTrip = array(
	                        "start_pos" =>'',
	                        "start_dt"  =>'',
	                        "start_odo" =>'',
	                        "stop_pos"  =>$e['position_text'],
	                        "stop_dt"   =>$e['datetime'],
	                        "stop_odo"  =>$e['odometer'],
	                        "stop_end"  =>''
	                    );
					}
				}
	        }
        }
	}
	return $movementReport;
}

function extract_gt_exceptions($events, $returnExcp, $spikes){
	if (is_array($events)) {
		foreach($events as $e){
			if (!in_array($e['position_id'], $spikes)) {
				if($e['event']=='speeding start'){
	                $returnExcp['speeding']['count']++;
	            }
	            elseif($e['event']=='area speed violation'){
	                $returnExcp['escarpment']['count']++;
	                if($e['speed'] > $returnExcp['escarpment']['value']){
	                    $returnExcp['escarpment']['value'] = $e['speed'];
	                }
	            }
	            elseif($e['event']=='panic'){
	                $returnExcp['panic']['count']++;
	            }
	            elseif($e['event']=='harsh acceleration'){
	                $returnExcp['acceleration']['count']++;
	            }
	            elseif($e['event']=='harsh braking'){
	                $returnExcp['braking']['count']++;
	            }
	            if($e['speed'] > $returnExcp['speeding']['value']){
	                $returnExcp['speeding']['value'] = $e['speed'];
	            }
	            $event_dt  = new DateTime($e['datetime'] .' (UTC)');
	            $event_dt  ->setTimezone(new DateTimeZone('CAT'));
	            if($e['speed'] > 10 && ($event_dt->format('H')<4 || $event_dt->format('H')>21)){
	                $returnExcp['night']['count']++;
	            }
	        }
		}
	}
	return $returnExcp;
}

function extract_fm_exceptions($trips, $events, $spikes, $std_exceptions, $custom_exceptions){
	$exceptions = array("standard"=>array(),"custom"=>array());
	if($events){
		foreach ($events as $e) {
            if(!in_array($e['eventid'], $spikes)){
                $key = strtolower(str_replace(" ","_", $e['driver']));
                $key = strtolower(str_replace("'","", $key));
                $aid = $e['assetid'];
                
                if (!isset($exceptions["standard"][$key])) {
                    $exceptions["standard"][$key]  = array(
                        'driver_name' => str_replace("'","", $e['driver']),
                        'driver_id'   => $e['driverid'],
                        'driver_dist' => 0,
                        'driver_speed' => 0,
                        'driver_rpm' => 0,
                        'driver_brake' => 0,
                        'driver_accl' => 0,
                        'violations'  => array(
                            "speeding"=>array("assetid"=>$aid,"value"=>0,"occur"=>0,"duration"=>0),
                            "revving"=>array("assetid"=>$aid,"value"=>0,"occur"=>0,"duration"=>0),
                            "acceleration"=>array("assetid"=>$aid,"value"=>0,"occur"=>0,"duration"=>0),
                            "braking"=>array("assetid"=>$aid,"value"=>0,"occur"=>0,"duration"=>0),
                            "idle"=>array("assetid"=>$aid,"occur"=>0,"duration"=>0),
                            "green_band"=>array("assetid"=>$aid,"occur"=>0,"score"=>0,"duration"=>0)
                        )
                    );
                }
                if (!isset($exceptions["custom"][$key])) {
                    $exceptions["custom"][$key]  = array(
                        'driver_name' => str_replace("'","", $e['driver']),
                        'driver_id'   => $e['driverid'],
                        'driver_dist' => 0,
                        'driver_speed' => 0,
                        'driver_rpm' => 0,
                        'driver_brake' => 0,
                        'driver_accl' => 0,
                        'violations'  => array(
                            "town"=>array("assetid"=>$aid,"occur"=>0,"duration"=>0),
                            "escarpment"=>array("assetid"=>$aid,"occur"=>0,"duration"=>0),
                            "free"=>array("assetid"=>$aid,"occur"=>0,"duration"=>0),
                            "night"=>array("assetid"=>$aid,"occur"=>0,"duration"=>0),
                            "continuous"=>array("assetid"=>$aid,"occur"=>0,"duration"=>0),
                            "panic"=>array("assetid"=>$aid,"occur"=>0,"duration"=>0),
							"depot"=>array("assetid"=>$aid,"occur"=>0,"duration"=>0)
                        )
                    );
                }
                if(array_value_exists($std_exceptions, $e['event'])){
                    $event_key  = get_event_key($std_exceptions, $e['event']);
                    if ($event_key!="idle" && $event_key!="green_band") {
                        if($exceptions["standard"][$key]['violations'][$event_key]['value'] < $e['value']){
                            $exceptions["standard"][$key]['violations'][$event_key]['value'] = $e['value'];
                        }
                    }
                    $exceptions["standard"][$key]['violations'][$event_key]['occur'] = $exceptions["standard"][$key]['violations'][$event_key]['occur'] + $e['occur'];
                    $exceptions["standard"][$key]['violations'][$event_key]['duration'] = $exceptions["standard"][$key]['violations'][$event_key]['duration'] + $e['duration'];
                }
                if(array_value_exists($custom_exceptions, $e['event'])){
                    $event_key  = get_event_key($custom_exceptions, $e['event']);

                    if(isset($exceptions["custom"][$key]['violations'][$event_key]['value']) && $exceptions["custom"][$key]['violations'][$event_key]['value'] < $e['value']){
                        $exceptions["custom"][$key]['violations'][$event_key]['value'] = $e['value'];
                    }
                    $exceptions["custom"][$key]['violations'][$event_key]['occur'] = $exceptions["custom"][$key]['violations'][$event_key]['occur'] + $e['occur'];
                    $exceptions["custom"][$key]['violations'][$event_key]['duration'] = $exceptions["custom"][$key]['violations'][$event_key]['duration'] + $e['duration'];
                }
            }
        }
    }

    if ($trips) {
    	foreach ($trips as $t) {
	    	$key    =  strtolower(str_replace(" ","_", $t['driver']));

	        if(isset($exceptions["standard"][$key]['driver_name'])){
	            $exceptions["standard"][$key]['driver_dist'] = $exceptions["standard"][$key]['driver_dist'] + $t['distancekilometers'];
	            $exceptions["custom"][$key]['driver_dist'] = $exceptions["custom"][$key]['driver_dist'] + $t['distancekilometers'];
	            if($exceptions["standard"][$key]['driver_speed'] < $t['maxspeedkilometersperhour']){
	                $exceptions["standard"][$key]['driver_speed'] = $t['maxspeedkilometersperhour'];
	                $exceptions["standard"][$key]['violations']['speeding']['value'] = $t['maxspeedkilometersperhour'];
	            }
	            if($exceptions["standard"][$key]['driver_rpm'] < $t['maxrpm']){
	                $exceptions["standard"][$key]['driver_rpm'] = $t['maxrpm'];
	                $exceptions["standard"][$key]['violations']['revving']['value'] = $t['maxrpm'];
	            }
	            if($exceptions["standard"][$key]['driver_brake'] < $t['maxdecelerationkilometersperhourpersecond']){
	                $exceptions["standard"][$key]['driver_brake'] = $t['maxdecelerationkilometersperhourpersecond'];
	                $exceptions["standard"][$key]['violations']['braking']['value'] = $t['maxdecelerationkilometersperhourpersecond'];
	            }
	            if($exceptions["standard"][$key]['driver_accl'] < $t['maxaccelerationkilometersperhourpersecond']){
	                $exceptions["standard"][$key]['driver_accl'] = $t['maxaccelerationkilometersperhourpersecond'];
	                $exceptions["standard"][$key]['violations']['acceleration']['value'] = $t['maxaccelerationkilometersperhourpersecond'];
	            }
	        }
	    }
    }
    return $exceptions;
}

function extract_fm_transit_time($events, $spikes){
	$duration 	= 0;
	$wait   	= false; 
	if($events){
		foreach ($events as $e) {
			if(!in_array($e['eventid'], $spikes)){
                if (stristr($e['event'], 'transit check')) {
                    if (!$wait) {
                        $transit['start'] = $e['startdatetime'];
                    }
                    else{
                        $duration   	= strtotime($e['startdatetime']) - strtotime($transit['start']);
                        $endArr    		= explode(" ", $e['startdatetime']);
                        $startArr    	= explode(" ", $transit['start']);
                        if($startArr != $endArr) {
                            $startDate  = new DateTime($startArr[0]);
                            $endDate    = new DateTime($endArr[0]);
                            $days       = $endDate->diff($startDate);
                            $duration 	= $duration - (7*60*60*$days->d); 
                        }
                    }
                    $wait = !$wait;
                }
            }
        }
    }
    return $duration;
}

function build_incentives_report($sheets){
	if ($sheets) {
		$drivers    	 = array();
		foreach ($sheets as $sh) {
            $key         = str_replace(" ", "_", strtolower($sh['driver']));
            $sheetDist   = json_decode(stripslashes($sh["distance"]),true);
            $sheetIncntv = json_decode(stripslashes($sh['bonus']),true);
            $exceptions  = json_decode(stripslashes($sh['exceptions']),true);
            $scores      = json_decode(stripslashes($sh['scores']),true);

            $bonus       = 0;
            $drvDist     = $exceptions['standard'][$key]['driver_dist'];
            $townDur     = $exceptions['custom'][$key]['violations']['town']['duration'];
            $escapDur    = $exceptions['custom'][$key]['violations']['escarpment']['duration'];
            $freeWDur    = $exceptions['custom'][$key]['violations']['free']['duration'];
            $fmSc        = $scores['overall_edit']!=$scores['overall'] && $scores['overall_edit']>0 ? $scores['overall_edit'] : $scores['overall'];
            if($sheetIncntv){
                $bonus = $sheetIncntv['local']>0 ? ($sheetIncntv['local']*$sheetIncntv['localrate']) : ($sheetIncntv['xbord']*$sheetIncntv['xbordrate']);
            }
            if(isset($drivers[$key])){
                $drivers[$key]['count']     = $drivers[$key]['count'] + 1;
                $drivers[$key]['distance']  = $drivers[$key]['distance'] + $drvDist;
                $drivers[$key]['basicTNS']  = $drivers[$key]['basicTNS'] + $bonus;
                $drivers[$key]['townDur']   = $drivers[$key]['townDur']  + $townDur;
                $drivers[$key]['escapDur']  = $drivers[$key]['escapDur'] + $escapDur;
                $drivers[$key]['freeWDur']  = $drivers[$key]['freeWDur'] + $freeWDur;
                $drivers[$key]['fuelScore'] = $drivers[$key]['fuelScore'] + ($scores['fuel']>100 ? 100 : $scores['fuel']);
                $drivers[$key]['fmScore']   = $drivers[$key]['fmScore']   + $fmSc;
                $drivers[$key]['cstScore']  = $drivers[$key]['cstScore']  + $scores['advanced'];
            }
            else{
                $drivers[$key] = array(
                    'count'     => 1, 
                    'name'      => $sh['driver'], 
                    'site'      => $sh['driversite'], 
                    'distance'  => $drvDist, 
                    'basicTNS'  => $bonus, 
                    'townDur'   => $townDur, 
                    'escapDur'  => $escapDur, 
                    'freeWDur'  => $freeWDur, 
                    'fuelScore' => ($scores['fuel']>100 ? 100 : $scores['fuel']), 
                    'fmScore'   => $fmSc, 
                    'cstScore'  => $scores['advanced'], 
                );
            }
        }

        $totals      = array();
        foreach ($drivers as $d) {
            $totals['count'] = isset($totals['count']) ? $totals['count']+1 : 1;
            $townSc  = $d['townDur']>60 ? ((100-(($d['townDur']-60)/12))<0 ? 0 : (100-(($d['townDur']-60)/12))) : 100;
            $escapSc = $d['escapDur']>15 ? ((100-(($d['escapDur']-15)/0.9999))<0 ? 0 : (100-(($d['escapDur']-15)/0.9999))) : 100;
            $freeWSc = $d['freeWDur']>15 ? ((100-(($d['freeWDur']-15)/0.9999))<0 ? 0 : (100-(($d['freeWDur']-15)/0.9999))) : 100;
            $fuelSc  = ($d['fuelScore']/$d['count']);
            $custSc  = ($townSc+$escapSc+$freeWSc)/3;
            $violSc  = (($d['fmScore']/$d['count'])+$custSc)/2;
            $cstVSc  = ($townSc+$escapSc+$freeWSc)/3;
            $domSc   = ((($d['fuelScore']+$d['fmScore'])/$d['count'])+$cstVSc)/3;
            $tnsFuel = (100-$d['fuelScore']/$d['count'])<2.5  ? $d['basicTNS']*0.15 : 0;
            $tnsViol = (100-$violSc)<5 ? $d['basicTNS']*0.1 : 0;
            
            $totals['dist']  = isset($totals['dist']) ? $totals['dist']+$d['distance'] : $d['distance'];
            $totals['twnSc'] = isset($totals['twnSc']) ? $totals['twnSc']+$townSc : $townSc;
            $totals['escSc'] = isset($totals['escSc']) ? $totals['escSc']+$escapSc : $escapSc;
            $totals['frwSc'] = isset($totals['frwSc']) ? $totals['frwSc']+$freeWSc : $freeWSc;
            $totals['fueSc'] = isset($totals['fueSc']) ? $totals['fueSc']+$fuelSc  : $fuelSc;
            $totals['vioSc'] = isset($totals['vioSc']) ? $totals['vioSc']+$violSc  : $violSc;
            $totals['domSc'] = isset($totals['domSc']) ? $totals['domSc']+$domSc   : $domSc;
            $totals['bscTs'] = isset($totals['bscTs']) ? $totals['bscTs']+$d['basicTNS'] : $d['basicTNS'];
            $totals['fueTs'] = isset($totals['fueTs']) ? $totals['fueTs']+$tnsFuel : $tnsFuel;
            $totals['vioTs'] = isset($totals['vioTs']) ? $totals['vioTs']+$tnsViol : $tnsViol;
        }

        return array('drivers' => $drivers, 'totals' => $totals);
	}

	return array();
}

/*
function calculate_driving_time($startCp, $endCp, $trips){
    $startCpDt 	= new DateTime($startCp);
    $endCpDt 	= new DateTime($endCp);
	$day 		= $startCpDt->format('Y-m-d');
    $dayStart	= $startCpDt->format('Y-m-d H:i:s');
    
    if ($startCpDt->format('Y-m-d')==$endCpDt->format('Y-m-d')) {
    	return strtotime($endCp)-strtotime($startCp);
    }
    else{
    	$duration 	= 0;
    	$cpStarted 	= false;
    	$cpEnded 	= false;
    	$dayEnd		= "";
    	foreach ($trips as $t) {
    		$ts 	= new DateTime($t['tripstart']);
    		$te 	= new DateTime($t['tripend']);

    		if ($ts<$endCpDt && $te>$startCpDt && $t['distancekilometers']>0) {
    			
    			if ($ts->format('Y-m-d')==$day) {							// Same day
    				$dayEnd = $te->format('Y-m-d H:i:s');
    			}
    			else{														// New day
    				$day 	   = $ts->format('Y-m-d');
    				$duration += strtotime($dayEnd)-strtotime($dayStart); 	// Add prev day's duration

    				if ($endCpDt->format('Y-m-d')==$day) {
	    				$duration += strtotime($endCp)-strtotime($ts->format('Y-m-d H:i:s'));
	    				break;
	    			}
	    			else{
	    				$dayStart  = $ts->format('Y-m-d H:i:s');
	    				$dayEnd    = $te->format('Y-m-d H:i:s');
	    			}
    			}
	    	}
	    	$dayEnd    = $te->format('Y-m-d H:i:s');
	    }
    	return $duration;
    }
}
*/

function calculate_driving_time($startCp, $endCp, $assetid, $trips){
	$CpStart 	= new DateTime($startCp);
    $CpEnd  	= new DateTime($endCp);
    
    if ($CpStart->format('Y-m-d') == $CpEnd->format('Y-m-d')) {
    	return strtotime($endCp)-strtotime($startCp);
    }
    else{
    	$duration 	= 0;
		$day 		= $CpStart->format('Y-m-d');
	    $dayStart	= $CpStart;
	    $dayEnd 	= $CpStart;
	    foreach ($trips as $t) {
	    	if ($assetid==$t['assetid']) {
		    	$ts 	= new DateTime($t['tripstart']);
	    		$te 	= new DateTime($t['tripend']);
		    	
		    	if($t['distancekilometers']>5 && $CpStart<$te && $CpEnd>$ts){
		    		if ($day != $te->format('Y-m-d')) {
		    			$duration += strtotime($dayEnd->format('Y-m-d H:i:s'))-strtotime($dayStart->format('Y-m-d H:i:s'));
		    			$dayStart  = $ts;
		    			$dayEnd    = $te;
		    		}
		    		if ($ts->format('Y-m-d') == $CpEnd->format('Y-m-d')) {
		    			return $duration + strtotime($CpEnd->format('Y-m-d H:i:s'))-strtotime($ts->format('Y-m-d H:i:s'));
		    		}
		    		elseif ($ts->format('Y-m-d') != $te->format('Y-m-d')) {
		    			$duration += strtotime($te->format('Y-m-d H:i:s'))-strtotime($ts->format('Y-m-d H:i:s'));
		    		}
		    		else {
		    			$dayStart  = $ts<$dayStart ? $ts : $dayStart; 
		    			$dayEnd    = $te>$dayEnd ? $te : $dayStart;
		    		}
		    	}
		    }
	    }
	    if ($duration==0) {
	    	return strtotime($endCp)-strtotime($startCp);
	    }
	    return $duration;
    }
}

function calculate_night_time_driving($transits){
	$night_duration		 	 = 0;
	$night_count		 	 = 0;
	if (isset($transits["movement"])) {
        foreach ($transits["movement"] as $e) {
            $day_duration  	 = calculate_driving_time($e['start']['startdatetime'],$e['end']['startdatetime']);
            $night_duration += strtotime($e['end']['startdatetime'])-strtotime($e['start']['startdatetime'])-$day_duration;
            $night_count++;
        }
	}
    return array('count' => $night_count, 'duration'=>$night_duration);
}

function calculate_early_park_duration($tripEnd, $depart, $arrive){
	$departDay 	= explode(" ", $depart)[0];
	$arriveDay 	= explode(" ", $arrive)[0];
	if ($departDay != $arriveDay) {
		$tripTime 	= explode(" ", $tripEnd)[1];
		return strtotime("19:00:00")-strtotime($tripTime);
	}
	return 0;
}

function extract_fm_transit_checks($events, $spikes, $trip_endings){
	// print_r($events);
	// exit;
    $transit_events = array();
    $transit 		= array('start'=>array(), 'end'=>array());
    $positionIds 	= array();
    $addresses 		= array();
    $start 			= true;
	if($events){
		foreach ($events as $e) {
			if(empty($spikes) || !in_array($e['eventid'], $spikes)){
                if ($e['event']=='transit check') {
                	if ($start) {
	                    $transit['start'] = $e;
            			$positionIds[] 	  = $e['eventid'];
	                    $start 			  = false;
                	}
                	else{
                		$transit['end']   = $e;
            			$positionIds[] 	  = $e['eventid'];
                		$transit_events[] = $transit;
	                    $start 			  = true;
	                    $transit 		  = array('start'=>array(), 'end'=>array());
                	}
                }
	        }
        }
    }

    if ($trip_endings) {
    	for ($i=0; $i < count($transit_events); $i++) { 
			$eDatetime 	= DateTime::createFromFormat('Y-m-d H:i:s', $transit_events[$i]['start']['startdatetime']);
			$eventDate 	= $eDatetime->format('Y-m-d');
			foreach ($trip_endings as $t) {
	    		$lastTrip = DateTime::createFromFormat('Y-m-d H:i:s', $t['lasttrip']);
				$tripDate = $lastTrip->format('Y-m-d');

				if ($tripDate==$eventDate) {
					$transit_events[$i]['end']['tripend'] = $t['lasttrip'];
				}
			}
    	}
    }

    return $transit_events;
}

function update_position_names($movementTransits, $api_base_url, $token){
	if($movementTransits){
		for ($i=0; $i < count($movementTransits); $i++) {
			$e = $movementTransits[$i];
			if (isset($e['start']['endlongitude']) && isset($e['start']['endlatitude'])) {
				$cLocation = fetchLocationName($api_base_url, $e['start']['endlongitude'], $e['start']['endlatitude'], $e['start']['groupid'], $token);
				if (trim($cLocation)!='') {
					$movementTransits[$i]['start']['location'] = $cLocation;
				}
			}
			if (isset($e['end']['endlongitude']) && isset($e['end']['endlatitude'])) {
				$cLocation = fetchLocationName($api_base_url, $e['end']['endlongitude'], $e['end']['endlatitude'], $e['end']['groupid'], $token);
				if (trim($cLocation)!='') {
					$movementTransits[$i]['end']['location'] = $cLocation;
				}
			}
		}
		return $movementTransits;
	}
	else{
		return array();
	}
}

function fetchLocationName($api_base_url, $lat, $lng, $groupid, $token){
	$score_url    = "/api/locations/group/$groupid/nearest";
	$post_data    = '{"Longitude":'.$lng.',"Latitude":'.$lat.'}';
	$rest_client  = new RestClient([
		'base_url'=>$api_base_url, 
		'headers' =>['Authorization' => 'Bearer '.$token], 
	]);
	$response  	  = json_decode($rest_client->post($score_url, $post_data, array('Content-Type' => 'application/json'))->response);

	return isset($response->Location->Name) ? $response->Location->Name : '';
}

function get_fm_transit_positions($positionIds, $dbh, $dbn, $dbu, $dbp){
	$strIds    = "";
	if ($positionIds) {
		foreach ($positionIds as $i) { $strIds .= ($strIds!="") ? ",".$i : $i; }
		$sql       = "SELECT positionsourceid AS eventid, formattedaddress FROM tblposition WHERE positionsourceid IN ($strIds)";
		$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
		$db_result = pg_query($db_conn, $sql);
		$rows      = pg_fetch_all($db_result);

		return $rows;
	}
	return array();	
}

function get_fm_transit_last_trips($assetid, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$sql 	= "SELECT DATE(tripend) day_end, MIN(tripstart) AS firsttrip, MAX(tripend) AS lasttrip FROM tbltrip 
				WHERE assetid=$assetid AND tripend>='$start' AND tripend<='$end' AND distancekilometers>1    
				GROUP BY day_end ORDER BY lasttrip";

	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function extract_accelerometer_events($events,$spikes){
	$accl_events  = array("corner"=>0,"accident"=>0,"bump"=>0,"tow"=>0,"lift"=>0,"roll"=>0);
	if ($events) {
		foreach ($events as $e) {
			if(!in_array($e['eventid'], $spikes) && stristr($e['event'],"3-Axis")){
				if(stristr($e['event'], "Cornering")){
		            $accl_events['corner'] += $e['occur'];
		        }
		        elseif(stristr($e['event'], "Accident (In")){
		            $accl_events['accident'] += $e['occur'];
		        }
		        elseif(stristr($e['event'], "Bumper")){
		            $accl_events['bump'] += $e['occur'];
		        }
		        elseif(stristr($e['event'], "Towing")){ //
		            $accl_events['tow'] += $e['occur'];
		        }
		        elseif(stristr($e['event'], "Lift Off")){ //liftoff
		            $accl_events['lift'] += $e['occur'];
		        }
		        elseif(stristr($e['event'], "Rollover")){ // follover
		            $accl_events['roll'] += $e['occur'];
		        }
			}
		}
	}
	return $accl_events;
}

function extract_fm_vision_events($events,$spikes){
	$vision_events  = array("corner"=>0,"accident_in"=>0,"accident_out"=>0,"bump"=>0,"speed"=>0,"rev"=>0,"brake"=>0,"accl"=>0);
	if ($events) {
		foreach ($events as $e) {
			if(!in_array($e['eventid'], $spikes) && (!is_null($e['mediaurls']) || !empty($e['mediaurls']))) {
				if(stristr($e['event'], "Cornering")){
		            $vision_events['corner'] += $e['occur'];
		        }
		        elseif(stristr($e['event'], "Accident (In")){
		            $vision_events['accident_in'] += $e['occur'];
		        }
		        elseif(stristr($e['event'], "Accident (Out")){
		            $vision_events['accident_out'] += $e['occur'];
		        }
		        elseif(stristr($e['event'], "Bumper")){
		            $vision_events['bump'] += $e['occur'];
		        }
		        elseif(stristr($e['event'], "Over speeding")){
		            $vision_events['speed'] += $e['occur'];
		        }
		        elseif(stristr($e['event'], "Over revving")){
		            $vision_events['rev'] += $e['occur'];
		        }
		        elseif(stristr($e['event'], "Harsh braking")){
		            $vision_events['brake'] += $e['occur'];
		        }
		        elseif(stristr($e['event'], "Harsh acceleration")){
		            $vision_events['accl'] += $e['occur'];
		        }
		    }
		}
	}
	return $vision_events;
}

function fetch_group_rpm_bands($groupid, $dbh, $dbn, $dbu, $dbp){
	$returnArr = array();
	$sql       = "SELECT DISTINCT description, eventtypeid FROM tbllibraryevent 
					WHERE groupid=$groupid AND description like 'RPM Band%' ORDER BY description";
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		foreach($rows AS $r){
			$bandKey = strtolower(str_replace(" ","", str_replace("-","_", str_replace("(","_", str_replace(")","_", $r['description'])))));
			$returnArr[$bandKey] = array('eventtypeid'=>$r['eventtypeid'],'band'=>$r['description'], 'duration'=>0);
		}
	}

	return $returnArr;
}

function extract_rpm_bands($groupid,$events,$spikes, $dbh, $dbn, $dbu, $dbp){
	$bands  = fetch_group_rpm_bands($groupid, $dbh, $dbn, $dbu, $dbp);
	if ($events) {
		foreach ($events as $e) {
			$bandKey = strtolower(str_replace(" ","", str_replace("-","_", str_replace("(","_", str_replace(")","_", $e['event'])))));
			if(!in_array($e['eventid'], $spikes) && stristr($e['event'], "RPM Band") ) {
				if (array_key_exists($bandKey, $bands)) {
					$bands[$bandKey]['duration'] = $bands[$bandKey]['duration'] + $e['duration'];
				}
		    }
		}
	}
	return $bands;
}

function extract_fm_trip_drivers($trips){
	$drivers  = array();
	if ($trips) {
		foreach ($trips as $t) {
			$key = strtolower(str_replace(" ","_", $t['driver']));
            if(!in_array($key, $drivers)){
                $drivers[$key] = array(
                    'name'     => $t['driver'], 
                    'driverid' => $t['driverid'],
                    'distance' => 0
                ); 
            }
		}
	}
	return $drivers;
}

function extract_fm_max_driver($exceptions){
	$driverid = 0;
	$topDist  = 0;
	if ($exceptions["standard"]) {
		foreach ($exceptions["standard"] as $e) {
            // if ($e['driver_name']!="Unknown" && $e['driver_id']!="" && $e['driver_dist']>0 && $topDist<=$e['driver_dist']) {
            if ($e['driver_id']!="" && $e['driver_dist']>0 && $topDist<=$e['driver_dist']) {
                $driverid = $e['driver_id'];
                $topDist  = $e['driver_dist'];
            }
        }
	}
	return $driverid;
}

function exctract_fm_overview($trips){
	$overview = array();
	if ($trips) {
		foreach ($trips as $t) {
			if(count($overview)==0){
                $overview = array( 
                    "startdate" => $t['tripstart'],
                    "enddate"   => $t['tripend'],
                    "drivetime" => $t['drivingtime'],
                    "duration"  => $t['duration'],
                    "fueltype"  => $t['fueltype'],
                    "fuelused"  => $t['fuelusedlitres'],
                    "distance"  => $t['distancekilometers'],
                    "endodo"    => $t['endodometerkilometers'],
                    "startodo"  => $t['startodometerkilometers'],
                    "maxspeed"  => $t['maxspeedkilometersperhour'],
                    "targetconsumption" => $t['targetfuelconsumption'],
                    "maxacceleration"   => $t['maxaccelerationkilometersperhourpersecond'],
                    "maxdeceleration"   => $t['maxdecelerationkilometersperhourpersecond'],
                );
                // echo "St:".$overview["startodo"] ." End:".$overview["endodo"] ."<br>";
            }
            else {
                $overview['drivetime']  = ($overview['drivetime']+$t['drivingtime']);
                $overview['duration'] 	= ($overview['duration']+$t['duration']);
                $overview['fuelused'] 	= ($overview['fuelused']+$t['fuelusedlitres']);
                $overview['distance'] 	= ($overview['distance']+$t['distancekilometers']);

                if($overview['enddate'] < $t['tripend']){
                    $overview['enddate'] = $t['tripend'];
                }
                if($overview['startdate'] > $t['tripstart']){
                    $overview['startdate'] = $t['tripstart'];
                }
                if($overview['endodo'] < $t['endodometerkilometers']){
                    $overview['endodo'] = $t['endodometerkilometers'];

                }
                if($overview['startodo'] > $t['startodometerkilometers']){
                    $overview['startodo'] = $t['startodometerkilometers'];
                }
                if($overview['maxspeed'] < $t['maxspeedkilometersperhour']){
                    $overview['maxspeed'] = $t['maxspeedkilometersperhour'];
                }
                if($overview['maxacceleration'] > $t['maxaccelerationkilometersperhourpersecond']){
                    $overview['maxacceleration'] = $t['maxaccelerationkilometersperhourpersecond'];
                }
                if($overview['maxdeceleration'] < $t['maxdecelerationkilometersperhourpersecond']){
                    $overview['maxdeceleration'] = $t['maxdecelerationkilometersperhourpersecond'];
                }

                // echo "St:".$overview["startodo"] ." End:".$overview["endodo"] ."<br>";
            }
		}
	}
	// print_r($overview);
	// exit;
	return $overview;
}

function extract_fm_country_transits($positions){
	$country_transits   = array(); 
	if ($positions) {
		$oldTime   		= "";
		foreach ($positions AS $p){
		    $address    = explode(",",$p['formattedaddress']);
		    $newCountry = ltrim(rtrim($address[count($address)-1]));
		    $country    = empty($newCountry) ? $country : $newCountry;
		    // $time_gmt	= new DateTime($p['timestamp'].' (UTC)');
		    // $time 		= $time_gmt->setTimezone(new DateTimeZone('CAT'))->format('Y-m-d H:i:s');
		    $time 		= $p['timestamp'];
		    $diff       = 0;
		    $nights     = 0;

		    if ($oldTime!="") {
		        $diff   = strtotime($time)-strtotime($oldTime);
		        $curArr = explode(" ", $time);
		        $oldArr = explode(" ", $oldTime);
		        if($time != $oldTime) {
		            $curDate = new DateTime($curArr[0]);
		            $oldDate = new DateTime($oldArr[0]);
		            $days    = $curDate->diff($oldDate);
		            $nights  = $days->d; 
		        }
		    }
		    else{ $nights = 0; }

		    $country = str_replace("ç", "c", str_replace("é", "e", $country));

		    $key  	 =  strtolower(str_replace(" ","_", $country));
		    if($key != ""){
		        if(!isset($country_transits[$key])){
		            $country_transits[$key] = array(
		                'name'=>$country,'time'=>$diff,'firstin'=>$time,'lastout'=>$time,'nights'=>$nights
		            );
		        }
		        else{
		            $country_transits[$key]['time']   = $country_transits[$key]['time'] + $diff;
		            $country_transits[$key]['nights'] = $country_transits[$key]['nights'] + $nights;
		            if (strtotime($country_transits[$key]['firstin']) > strtotime($time)) {
		                $country_transits[$key]['firstin'] = $time;
		            }
		            elseif(strtotime($country_transits[$key]['lastout']) < strtotime($time)){
		                $country_transits[$key]['lastout'] = $time;
		            }
		        }
		    }
		    $oldTime  = $time;
		}
	}
	return $country_transits;
}

function calculate_fm_advanced_score($groupid,$exceptions,$driver){
    $advanced   = 100;
    $key        =  strtolower(str_replace(" ","_", $driver));
    if ($exceptions["custom"]) {
 		$deduction  = 0;
    	foreach ($exceptions["custom"][$key]['violations'] as $k => $c) {
    		if ($k!='panic') {
    			if ($groupid==1853956352811964940 || $groupid=2065541667759962877){
    				if($k!='night' && $k!='continuous') {
	    				$deduction += $c['occur']+$c['duration'];
	    			}
    			}
    			elseif ($groupid==-5511162494231442644) {
    				if($k=='town' || $k=='escarpment') {
	    				$deduction += $c['occur']+$c['duration'];
	    			}
    			}
    			else{
    				$deduction += $c['occur']+$c['duration'];
    			}
    		}
	    }
	    $advanced   = 100 - $deduction;
    }
    return ($advanced<0) ? 0 : $advanced;
}

function get_fm_tripsheet_scores($token, $api_base_url, $driver_id, $start, $end){
	// echo $driver_id;
	// exit;
	$scores 	= array("green"=>0,"overall"=>0,"speed"=>0,"revv"=>0,"brake"=>0,"accl"=>0,"idle"=>0);
	$scoresArr	= array();
	$startDate  = new DateTime($start);
    $endDate    = new DateTime($end);
    $duration   = $startDate->diff($endDate);

    if ($duration->y > 0 || $duration->m > 0 || $duration->d>29) {
        $t1     = $start;
        $days 	= 0;
        for($s=0; $t1<$end; $s++){
            $t2 = date('Y-m-d H:i:s', strtotime($t1 .' + 28 days'));
            if($t2>$end){ $t2 = $end; }

            $response 	 = fetch_fm_driver_scores($token, $api_base_url, $driver_id, $t1, $t2);
            // print_r($response);
            // exit;
            $interval 	 = dateDiffInDays($t1, $t2);
            $days 	 	+= $interval;
            $scoresArr[] = array("response"=>$response, "days"=>$interval);
            $t1       	 = $t2;

            sleep(12);
        }
        foreach ($scoresArr as $sc) {
        	if (!empty($sc['response']->response)) {
        		$overall  = $sc['response'][0]->OverallScore 			   * ($sc['days']/$days);
	            $revv     = $sc['response'][0]->OverRevvingScore 		   * ($sc['days']/$days);
	            $speed    = $sc['response'][0]->OverSpeedingScore  		   * ($sc['days']/$days);
	            $brake    = $sc['response'][0]->HarshBrakingScore 		   * ($sc['days']/$days);
	            $idle  	  = $sc['response'][0]->ExcessiveIdlingScore 	   * ($sc['days']/$days);
	            $accl     = $sc['response'][0]->HarshAccelerationScore 	   * ($sc['days']/$days);
	            $green 	  = $sc['response'][0]->OutOfGreenBandDrivingScore * ($sc['days']/$days);

	            $scores["revv"]    += $revv;
	            $scores["accl"]    += $accl;
	            $scores["idle"]    += $idle;
	            $scores["green"]   += $green;
	            $scores["speed"]   += $speed;
	            $scores["brake"]   += $brake;
	            $scores["overall"] += $overall;
        	}
        }
    }
    else{
        $response   		= fetch_fm_driver_scores($token, $api_base_url, $driver_id, $start, $end);
        $scoresArr  		= json_decode($response->response);
        if (isset($scoresArr[0])) {
	        $scores["overall"] 	= $scoresArr[0]->OverallScore;
	        $scores["revv"] 	= $scoresArr[0]->OverRevvingScore;
	        $scores["speed"] 	= $scoresArr[0]->OverSpeedingScore;
	        $scores["brake"] 	= $scoresArr[0]->HarshBrakingScore;
	        $scores["idle"] 	= $scoresArr[0]->ExcessiveIdlingScore;
	        $scores["accl"] 	= $scoresArr[0]->HarshAccelerationScore;
	        $scores["green"]   	= $scoresArr[0]->OutOfGreenBandDrivingScore;
        }
    }
    return $scores;
}

function update_tripsheet_score($id, $new_scores, $dbh, $dbn, $dbu, $dbp){
	$sql      = "UPDATE tbltripsheet SET scores=? WHERE tripsheetid=? ";

	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $new_scores);
	$stmt     ->bindParam(2,  $id);

	if($stmt->execute()){
        $_id  = $db_conn->lastInsertId();
        return array('result'=>true,'id'=>$_id,'message'=>'TS has been updated');
    }
    else{
    	$errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}

function get_linked_gt_distance($asset_id, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT MAX(odometer) AS endodo, MIN(odometer) AS startodo FROM tblposition_gt p, tblunit_gt u 
					WHERE u.fm_assetid=$asset_id AND u.unit_id=p.unit_id AND p.datetime>='$start' AND p.datetime<='$end'"; 
	// echo $sql;
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	$distance  = 0;
	if ($rows) {
		$distance = $rows[0]['endodo'] - $rows[0]['startodo'];
	}

	return $distance;
}

function get_start_positions($group_id, $asset_id, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT positionid, assetid, timestamp, odometerkilometres, formattedaddress 
					FROM tblstartposition        
					WHERE groupid=$group_id AND assetid=$asset_id AND timestamp >= '$start' AND timestamp <= '$end'       
					ORDER BY timestamp"; 
	// echo $sql;
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_fm_positions($group_id, $asset_id, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT * FROM tblposition        
					WHERE groupid=$group_id  
						AND assetid=$asset_id  
						AND timestamp >= '$start' AND timestamp <= '$end'       
					ORDER BY timestamp"; 
	// echo $sql;
	// exit;
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_end_positions($group_id, $asset_id, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT positionid, assetid, timestamp, odometerkilometres, formattedaddress 
					FROM tblendposition        
					WHERE groupid=$group_id AND assetid=$asset_id AND timestamp >= '$start' AND timestamp <= '$end'       
					ORDER BY timestamp"; 
	// echo $sql;
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_tripsheet_driver_exceptions($tripsheet_id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT exceptionid, tripsheetid, t.driverid, d.name AS driver, t.assetid, a.description AS asset, exceptiontype, eventname, count, duration, maximum   
					FROM tbl_tripexception t, tblasset a, tbldriver d     
					WHERE  
						t.tripsheetid = $tripsheet_id
						AND t.driverid = d.driverid   
						AND t.assetid = a.assetid     
					ORDER BY exceptiontype, driver";

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	$return_a  = array();

	foreach($rows AS $r){
		$key            = strtolower(str_replace(" ","_", $r['driver']));
		
		if (!driver_exists_in_array($return_a, $r['driver'])){
			$return_a[$key]  = array(
					'driver_name'  	=> $r['driver'],
					'driver_id'   	=> $r['driverid'],'driver_dist' => 0,
					'driver_speed'	=> 0,
					'driver_rpm'  	=> 0,
					'driver_brake'	=> 0,
					'driver_accl' 	=> 0,
					'violations'  	=> array($r['eventname'] => array(
						'type'      => $r['exceptiontype'],
						'value'     => $r['maximum'],
						'asset'     => $r['asset'],
						'assetid'   => $r['assetid'],
						'occur'     => $r['count'],
						'event'     => $r['eventname'],
						'duration'  => $r['duration']
					))
				);	
		}
		elseif(!event_exists_in_array($return_a, $r['driver'], $r['eventname'])){
			$return_a[$key]['violations'][$r['eventname']]  = array(
					'type'      => $r['exceptiontype'],
					'value'     => $r['maximum'],
					'asset'     => $r['asset'],
					'assetid'   => $r['assetid'],
					'occur'     => $r['count'],
					'event'     => $r['eventname'],
					'duration'  => $r['duration']
				);
		}
	}

	return $return_a;
}

function get_driver_exception_history($tripsheet_id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT exceptionid, tripsheetid, t.driverid, d.name AS driver, t.assetid, a.description AS asset, exceptiontype, eventname, count, duration, maximum   
					FROM tbl_tripexception t, tblasset a, tbldriver d     
					WHERE  
						t.tripsheetid IN (SELECT tripsheetid WHERE tripsheetid<$tripsheet_id)
						AND t.driverid = d.driverid   
						AND t.assetid = a.assetid     
					ORDER BY exceptiontype, driver     
					LIMIT 3";

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	$return_a  = array();

	foreach($rows AS $r){
		$key            = strtolower(str_replace(" ","_", $r['driver']));
		
		if (!driver_exists_in_array($return_a, $r['driver'])){
			$return_a[$key]  = array(
					'driver_name' 	=> $r['driver'],
					'driver_id'   	=> $r['driverid'],'driver_dist' => 0,
					'driver_speed'	=> 0,
					'driver_rpm'  	=> 0,
					'driver_brake'	=> 0,
					'driver_accl' 	=> 0,
					'violations'  	=> array($r['eventname'] => array(
						'type'      => $r['exceptiontype'],
						'value'     => $r['maximum'],
						'asset'     => $r['asset'],
						'assetid'   => $r['assetid'],
						'occur'     => $r['count'],
						'event'     => $r['eventname'],
						'duration'  => $r['duration']
					))
				);	
		}
		elseif(!event_exists_in_array($return_a, $r['driver'], $r['eventname'])){
			$return_a[$key]['violations'][$r['eventname']]  = array(
					'type'      => $r['exceptiontype'],
					'value'     => $r['maximum'],
					'asset'     => $r['asset'],
					'assetid'   => $r['assetid'],
					'occur'     => $r['count'],
					'event'     => $r['eventname'],
					'duration'  => $r['duration']
				);
		}
	}

	return $return_a;
}

function get_fm_asset_events($group_id, $asset_id, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT e.groupid, e.eventid, e.eventtypeid, e.assetid, e.driverid, e.mediaurls, d.name AS driver, 
						t.description AS event, a.description AS asset, startdatetime, enddatetime, 
						e.totaloccurances AS occur, e.totaltimeseconds AS duration, e.value, eventtype, endlatitude, endlongitude, locationname    
					FROM tblevent e, tbllibraryevent t, tblasset a, tbldriver d     
					WHERE    
						e.assetid=$asset_id  
						AND t.eventtypeid = e.eventtypeid   
						AND e.assetid = a.assetid    
						AND e.driverid = d.driverid    
						AND t.groupid=$group_id  
						AND startdatetime>='$start'   
						AND startdatetime<='$end'    
					ORDER BY startdatetime, enddatetime, t.description";
	// Here we ignore enddatetime because its sometimes comes as blank and will cause events such as transit checks to be omitted
	// echo $sql;
	// exit;

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}


function get_nights_out_trips($group_id, $site_id, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT(DATE(t.tripend)) AS dte, t.tripend, t.assetid, t.driverid, d.name AS driver, a.description AS asset, 
						(SELECT formattedaddress FROM tblposition p WHERE p.positionsourceid=t.tripid LIMIT 1) AS locationname  
					FROM tbltrip t, tblasset a, tbldriver d  
					WHERE t.groupid=$group_id AND t.assetid=a.assetid AND t.driverid=d.driverid AND date(t.tripend)=date(t.tripend) 
						AND t.tripend>='$start' AND t.tripend<='$end' ";

	if (intval($site_id)!=0) {
	 	$sql  .= " AND a.siteid=$site_id ";
	} 
	$sql  	  .= " ORDER BY asset, tripend, driver";
	// Here we ignore enddatetime because its sometimes comes as blank and will cause events such as transit checks to be omitted
	// echo $sql;
	// exit;

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}


function get_fm_tripsheet_events($ts_id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT e.eventid, e.assetid, e.driverid, e.mediaurls, d.name AS driver, 
						l.description AS event, a.description AS asset, startdatetime, enddatetime, 
						e.totaloccurances AS occur, e.totaltimeseconds AS duration, e.value, eventtype  
					FROM tbltripsheet t, tblevent e, tbllibraryevent l, tblasset a, tbldriver d              
					WHERE  
						tripsheetid=$ts_id      
						AND l.eventtypeid = e.eventtypeid   
						AND t.assetid = e.assetid  
						AND e.assetid = a.assetid    
						AND e.driverid = d.driverid   
						AND e.startdatetime >= t.start  
						AND e.startdatetime <= t.end    
					ORDER BY startdatetime";

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_spike_events($spikes, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT e.eventid, e.assetid, e.driverid, e.mediaurls, d.name AS driver, 
						l.description AS event, a.description AS asset, startdatetime, enddatetime, 
						e.totaloccurances AS occur, e.totaltimeseconds AS duration, e.value, eventtype  
					FROM tblevent e, tbllibraryevent l, tblasset a, tbldriver d              
					WHERE eventid IN ($spikes)       
						AND l.eventtypeid = e.eventtypeid   
						AND e.assetid = a.assetid    
						AND e.driverid = d.driverid    
					ORDER BY startdatetime";

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_fm_trips($group_id, $driver_id, $asset_id, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT d.name AS driver, t.assetid, a.description AS asset, tripstart, tripend, firstdepart, lasthalt, duration, drivingtime, 
						standingtime, startpositionid, endpositionid, distancekilometers, startodometerkilometers, endodometerkilometers, fuelusedlitres, 
						maxspeedkilometersperhour, maxaccelerationkilometersperhourpersecond, maxdecelerationkilometersperhourpersecond, 
						targetfuelconsumption, fueltype  
					FROM tbltrip t, tbldriver d, tblasset a      
					WHERE  
						t.driverid = d.driverid   
						AND t.assetid=a.assetid    
						AND tripstart>='$start'   
						AND tripstart<='$end' ";

	if (!is_null($group_id) && !empty($group_id) && $group_id!=0) {
	 	$sql  .= " AND t.groupid=$group_id ";
	} 
	if (!is_null($driver_id) && !empty($driver_id) && $driver_id!=0) {
	 	$sql  .= " AND t.driverid=$driver_id ";
	}
	if (!is_null($asset_id) && !empty($asset_id) && $asset_id!=0) {
	 	$sql  .= " AND t.assetid=$asset_id ";
	}
	$sql  	  .= " ORDER BY tripstart, tripend";

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_driver_trip_data($group_id, $driver_id, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT d.name AS driver, a.description AS asset, tripstart, tripend, firstdepart, lasthalt, duration, drivingtime, 
						standingtime, startpositionid, endpositionid, distancekilometers, startodometerkilometers, endodometerkilometers, fuelusedlitres, 
						maxspeedkilometersperhour, maxaccelerationkilometersperhourpersecond, maxdecelerationkilometersperhourpersecond, 
						targetfuelconsumption, fueltype  
					FROM tbltrip t, tbldriver d, tblasset a      
					WHERE  
						t.driverid = d.driverid     
						AND t.driverid=$driver_id  
						AND t.assetid=a.assetid   
						AND t.groupid=$group_id   
						AND tripstart>='$start'   
						AND tripstart<='$end'     
					ORDER BY tripstart, tripend";

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_fm_asset_trips ($group_id, $asset_id, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT d.name AS driver, d.driverid, a.description AS asset, a.assetid, tripstart, tripend, firstdepart, lasthalt, duration, drivingtime, standingtime, startpositionid, endpositionid, 
						distancekilometers, startodometerkilometers, endodometerkilometers, fuelusedlitres, maxrpm, maxspeedkilometersperhour, 
						maxaccelerationkilometersperhourpersecond, maxdecelerationkilometersperhourpersecond, targetfuelconsumption, fueltype  
					FROM tbltrip t, tbldriver d, tblasset a      
					WHERE  
						t.driverid = d.driverid     
						AND t.assetid=$asset_id   
						AND t.assetid=a.assetid   
						AND t.groupid=$group_id   
						AND tripstart>='$start'   
						AND tripstart<='$end'     
					ORDER BY tripstart, tripend";

	// echo $sql;
	// exit;

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_driver_exceptions_old($group_id, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT d.name, e.driverid, t.description, SUM(e.totaloccurances) AS occur, SUM(e.totaltimeseconds) AS duration
					FROM tbldriver d, tbllibraryevent t   
						LEFT JOIN tblevent e ON t.eventtypeid = e.eventtypeid   
					WHERE 
						t.groupid=$group_id 
						AND e.driverid=d.driverid  
						AND tripstart>='$start'   
						AND tripstart<='$end'     
					GROUP BY t.description, e.driverid, d.name  
					ORDER BY name LIMIT 2000";

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_tripsheets($dbh, $dbn, $dbu, $dbp){
	$subgroup 	= isset($_COOKIE['site']) ? substr($_COOKIE['site'], strpos($_COOKIE['site'], '=') + 1) : null;
	if(isset($_COOKIE['url'])){
		$dateURL 	=  explode("=", $_COOKIE['url']);
		$startDate	= substr($dateURL[1],0,strrpos($dateURL[1],'&'));
		$endDate 	= $dateURL[2];
	}
	if(isset($subgroup) && $subgroup != 0 && isset($startDate)){
		$sql       = "SELECT DISTINCT ON (t.tripsheet) tripsheet, t.tripsheetid, t.trip, t.created, t.version, t.group, distance, 
					fuel, targets, scores, asset, approved, verificationdate, verificationuser, driver, t.status, t.start, t.end, t.type, u.name AS user,
					(
						SELECT COUNT(v.tripsheetid) FROM tbltripsheet v WHERE v.parentid=t.tripsheetid
					) AS versions
					FROM tbltripsheet t, tbl_user u , tblasset a   
					WHERE t.userid=u.id AND t.assetid =a.assetid AND t.groupid<>0 AND a.siteid =$subgroup AND t.status<>'DELETED' AND t.end >= '$startDate' AND t.end < '$endDate'
					ORDER BY tripsheet, tripsheetid";
	} else if(isset($subgroup) && $subgroup != 0){
		$sql       = "SELECT DISTINCT ON (t.tripsheet) tripsheet, t.tripsheetid, t.trip, t.created, t.version, t.group, distance, 
					fuel, targets, scores, asset, approved, verificationdate, verificationuser, driver, t.status, t.start, t.end, t.type, u.name AS user,
					(
						SELECT COUNT(v.tripsheetid) FROM tbltripsheet v WHERE v.parentid=t.tripsheetid
					) AS versions
					FROM tbltripsheet t, tbl_user u , tblasset a   
					WHERE t.userid=u.id AND t.assetid =a.assetid AND t.groupid<>0 AND a.siteid =$subgroup AND t.status<>'DELETED' AND t.created > (CURRENT_DATE - INTERVAL '91 days')    
					ORDER BY tripsheet, tripsheetid";
	} else if(isset($_COOKIE['url'])){
		$sql       = "SELECT DISTINCT ON (t.tripsheet) tripsheet, t.tripsheetid, t.trip, t.created, t.version, t.group, distance, 
					fuel, targets, scores, asset, approved, verificationdate, verificationuser, driver, t.status, t.start, t.end, t.type, u.name AS user,
					(
						SELECT COUNT(v.tripsheetid) FROM tbltripsheet v WHERE v.parentid=t.tripsheetid
					) AS versions
					FROM tbltripsheet t, tbl_user u   
					WHERE t.userid=u.id AND t.groupid<>0 AND t.status<>'DELETED' AND t.end >= '$startDate' AND t.end < '$endDate'    
					ORDER BY tripsheet, tripsheetid";

	}else {
		$sql       = "SELECT DISTINCT ON (t.tripsheet) tripsheet, t.tripsheetid, t.trip, t.created, t.version, t.group, distance, 
					fuel, targets, scores, asset, approved, verificationdate, verificationuser, driver, t.status, t.start, t.end, t.type, u.name AS user,
					(
						SELECT COUNT(v.tripsheetid) FROM tbltripsheet v WHERE v.parentid=t.tripsheetid
					) AS versions
					FROM tbltripsheet t, tbl_user u 
					WHERE t.userid=u.id AND t.groupid<>0 AND t.status<>'DELETED' AND t.created > (CURRENT_DATE - INTERVAL '91 days')    
					ORDER BY tripsheet, tripsheetid";
	}

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		$tDate = array_column($rows, 'created');
		array_multisort($tDate, SORT_DESC, $rows);
		return $rows;
	}
	return array();
}

function get_driver_tripsheets_full($groupid, $siteid, $driverid, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$params = "";
	if ($groupid!=0)  { $params .= " t.groupid=$groupid AND "; }
	else{ 				$params .= " t.groupid<>0 AND ";  		} 
	if ($siteid!=0)   { $params .= " (d.siteid=$siteid OR a.siteid=$siteid) AND "; } 
	if ($driverid!=0) { $params .= " t.driverid=$driverid AND "; } 
	if ($start!="" && $end!="") { 
						$params .= " t.end>='$start' AND t.end<='$end' AND "; 
	}
	$sql       = "SELECT DISTINCT ON (t.tripsheet) tripsheet, t.tripsheetid, u.name AS user, bonus, t.exceptions, t.trip, t.created, 
					t.version, t.group, distance, fuel, targets, scores, asset, driver, t.start, t.end, t.type, d.siteid AS driversiteid,    
					(SELECT ag.name FROM tblasset a INNER JOIN tblsubgroup ag ON a.siteid=ag.subgroupid WHERE a.assetid=t.assetid) AS assetsite, 
					(SELECT dg.name FROM tbldriver d INNER JOIN tblsubgroup dg ON d.siteid=dg.subgroupid WHERE d.driverid=t.driverid) AS driversite, 
					(SELECT COUNT(v.tripsheetid) FROM tbltripsheet v WHERE v.parentid=t.tripsheetid ) AS versions 
				  FROM tbltripsheet t, tbl_user u, tbldriver d, tblasset a 
				  WHERE $params t.userid=u.id AND t.driverid=d.driverid AND t.assetid=a.assetid AND t.status<>'DELETED'
				  ORDER BY tripsheet, tripsheetid DESC";

	// echo $sql;
	// exit;

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		$tDate = array_column($rows, 'created');
		array_multisort($tDate, SORT_DESC, $rows);
		return $rows;
	}
	return array();
}

function get_asset_tripsheets_full($groupid, $siteid, $assetid, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT ON (t.tripsheet) tripsheet, t.tripsheetid, u.name AS user, bonus, 
					t.trip, t.created, t.version, t.group, distance, fuel, targets, scores, asset, driver, t.start, t.end, t.type, 
					a.siteid AS assetsiteid, ast.name AS assetsite, 
					(
						SELECT COUNT(v.tripsheetid) FROM tbltripsheet v WHERE v.parentid=t.tripsheetid
					) AS versions      
					FROM tbltripsheet t, tbl_user u, tblasset a, tblsubgroup ast     
					WHERE t.userid=u.id AND t.groupid<>0 AND t.assetid=a.assetid AND a.siteid=ast.subgroupid AND t.status<>'DELETED'  ";

	if ($groupid!=0)  { $sql .= " AND t.groupid=$groupid "; } 
	if ($siteid!=0)   { $sql .= " AND a.siteid=$siteid "; } 
	if ($assetid!=0)  { $sql .= " AND t.assetid=$assetid "; } 
	if ($driverid!=0) { $sql .= " AND t.driverid=$driverid "; }
	if ($start!="" && $end!="") { $sql .= " AND t.end>='$start' AND t.end<='$end' "; } 

	$sql 	  .= " ORDER BY tripsheet, tripsheetid ";
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		$tDate = array_column($rows, 'created');
		array_multisort($tDate, SORT_DESC, $rows);
		return $rows;
	}
	return array();
}

function get_tripsheets_by_range($groupid,$start,$end,$dbh,$dbn,$dbu,$dbp){
	$sql       = "SELECT DISTINCT ON (t.tripsheet) tripsheet, d.siteid AS driversiteid, a.siteid AS assetsiteid, t.tripsheetid, t.trip, 
					t.created, t.version, t.group, distance, fuel, targets, scores, asset, t.assetid, driver, t.driverid, t.start, t.end, 
					t.type, u.name AS user, exceptions      
					FROM tbltripsheet t, tbldriver d, tblasset a, tbl_user u   
					WHERE t.driverid=d.driverid AND t.assetid=a.assetid AND t.userid=u.id ";

	if ($groupid!=0 && $groupid!="") { $sql .= " AND t.groupid=$groupid "; 	}
	else{ 							   $sql .= " AND t.groupid<>0 ";		}
	if ($start!="" && $end!="") { $sql .= " AND t.end>='$start' AND t.end<='$end' AND t.status<>'DELETED' "; }
					     
	$sql 	  .= " ORDER BY tripsheet, tripsheetid";

	// echo $sql;
	// exit;

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		$tDate = array_column($rows, 'start');
		array_multisort($tDate, SORT_DESC, $rows);
		return $rows;
	}
	return array();
}

function get_driver_tripsheets($drv,$start,$end, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT ON (t.tripsheet) tripsheet, t.tripsheetid, t.trip, t.created, t.version, t.group, distance, 
					fuel, targets, scores, exceptions, asset, driver, t.start, t.end, t.type, u.name AS user,
					(
						SELECT COUNT(v.tripsheetid) FROM tbltripsheet v WHERE v.parentid=t.tripsheetid
					) AS versions      
					FROM tbltripsheet t, tbl_user u   
					WHERE t.userid=u.id AND t.driverid=$drv AND t.end>='$start' AND t.end<='$end' AND t.status<>'DELETED' 
					ORDER BY tripsheet, tripsheetid";
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		$tDate = array_column($rows, 'created');
		array_multisort($tDate, SORT_DESC, $rows);
		return $rows;
	}
	return array();
}

function get_tripsheet($tripsheet_id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT t.*, u.name AS user, 
					(SELECT c.name FROM tbl_customer c WHERE t.gcustomerid=c.id) AS customer, 
					(SELECT p.name FROM tbl_product p WHERE t.productid=p.id) AS product, 
					(SELECT l.name FROM tbl_depot l WHERE t.loadingid=l.id) AS loading, 
					(SELECT o.name FROM tbl_depot o WHERE t.offloadid=o.id) AS offload        
					FROM tbltripsheet t, tbl_user u    
					WHERE t.tripsheetid=$tripsheet_id AND t.userid=u.id  ";

	// echo $sql;
	// exit; 

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		return $rows[0];
	}
	return array();
}

function get_tripsheets_with_missing_scores($dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT * FROM tbltripsheet WHERE scores='' ORDER BY tripsheetid DESC";
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		return $rows;
	}
	return array();
}

function tripsheet_name_exists($tripsheet, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT tripsheetid FROM tbltripsheet WHERE tripsheet='$tripsheet' AND status<>'DELETED'";
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		return true;
	}
	return false;
}

function get_group_tripsheets_legacy($group_id, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT t.*, u.name AS user FROM tbltripsheet t, tbl_user u WHERE t.groupid=$group_id AND t.userid=u.id AND t.status<>'DELETED'   
					ORDER BY created DESC, tripsheet";
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_bulk_transit($group, $driver, $asset, $start, $end, $dbh, $dbn, $dbu, $dbp){
	$sql 	   = "SELECT DISTINCT ON (t.tripsheet) t.* FROM tbltripsheet t WHERE t.end>='$start' AND t.start<='$end' AND t.status<>'DELETED' ";

	if (is_numeric($group) && $group!=0) {    $sql  .= " AND t.groupid=$group "; }
	if (is_numeric($driver) && $driver!=0) { $sql  .= "AND t.driverid=$driver "; 	}  
	if (is_numeric($asset) && $asset!=0)   { $sql  .= "AND t.assetid=$asset "; 	}  
	
	$sql  	  .= "ORDER BY tripsheet, created";

					// ORDER BY tripsheet, tripsheetid";
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		// $tDate = array_column($rows, 'created');
		// array_multisort($tDate, SORT_DESC, $rows);
		return $rows;
	}
	return array();
}

function get_trip_sheets_legacy($dbh, $dbn, $dbu, $dbp){
	// $sql       = "SELECT DISTINCT t.*, a.description AS asset, u.name AS user, g.name AS group, 
	// 				(
	// 					SELECT COUNT(DISTINCT driverid) FROM tbl_tripexception e 
	// 					WHERE e.tripsheetid=t.tripsheetid
	// 				) AS drivers    
	// 				FROM tblasset a, tbl_tripsheet t, tbl_user u, tblgroup g        
	// 				WHERE t.assetid=a.assetid AND t.userid=u.id AND t.groupid=g.groupid     
	// 				ORDER BY t.created DESC, t.startdate, t.enddate;";

	$sql       = "SELECT DISTINCT ON (t.tripsheet) tripsheet, t.tripsheetid, t.groupid, t.assetid, 
					t.trip, g.name AS group, t.distance_actual,
				 	t.distance_empty,t.distance_fm, t.distance_gt, t.fuel_actual, t.fuel_fm, 
				 	t.fuel_gt, t.startdate, t.enddate, t.created, t.userid, t.spikes, t.svc_km,
				 	t.svc_last_date, t.svc_next_date, t.loading_id, t.offloading_id,t.product_id, 
				 	t.parent_id, t.version, t.comment, a.description AS asset, u.name AS user, 
					(
						SELECT COUNT(DISTINCT driverid) FROM tbl_tripexception e 
						WHERE e.tripsheetid=t.tripsheetid
					) AS drivers, 
					(
						SELECT COUNT(v.tripsheetid) FROM tbl_tripsheet v 
						WHERE v.parent_id=t.tripsheetid
					) AS versions      
					FROM tblasset a, tbl_tripsheet t, tbl_user u, tblgroup g       
					WHERE t.assetid=a.assetid AND t.userid=u.id AND t.groupid=g.groupid AND t.status<>'DELETED'     
					ORDER BY tripsheet, tripsheetid, t.startdate, t.enddate;";
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_group_tripsheets($group_id, $dbh, $dbn, $dbu, $dbp){
	$subgroup = isset($_COOKIE['site']) ? substr($_COOKIE['site'], strpos($_COOKIE['site'], '=') + 1) : null;
	if(isset($_COOKIE['url'])){
		$dateURL 	=  explode("=", $_COOKIE['url']);
		$startDate	= substr($dateURL[1],0,strrpos($dateURL[1],'&'));
		$endDate 	= $dateURL[2];
	}
	
	if(isset($subgroup) && $subgroup != 0 && isset($startDate)){
		$sql       = "SELECT DISTINCT ON (t.tripsheet) tripsheet, t.tripsheetid, t.trip, t.created, t.version, t.group, distance, 
					fuel, targets, scores, asset, approved, verificationdate, verificationuser, driver, t.status, t.start, t.end, t.type, u.name AS user,
					(
						SELECT COUNT(v.tripsheetid) FROM tbltripsheet v WHERE v.parentid=t.tripsheetid
					) AS versions
					FROM tbltripsheet t, tbl_user u, tblasset a
					WHERE t.userid=u.id AND t.groupid=$group_id AND t.assetid =a.assetid AND a.siteid =$subgroup AND t.status<>'DELETED' AND t.end >= '$startDate' AND t.end < '$endDate'   
					ORDER BY tripsheet, tripsheetid";

	} else if(isset($subgroup) && $subgroup != 0){
		$sql       = "SELECT DISTINCT ON (t.tripsheet) tripsheet, t.tripsheetid, t.trip, t.created, t.version, t.group, distance, 
					fuel, targets, scores, asset, approved, verificationdate, verificationuser, driver, t.status, t.start, t.end, t.type, u.name AS user,
					(
						SELECT COUNT(v.tripsheetid) FROM tbltripsheet v WHERE v.parentid=t.tripsheetid
					) AS versions
					FROM tbltripsheet t, tbl_user u, tblasset a
					WHERE t.userid=u.id AND t.groupid=$group_id AND t.assetid =a.assetid AND a.siteid =$subgroup AND t.status<>'DELETED' AND t.created > (CURRENT_DATE - INTERVAL '91 days')    
					ORDER BY tripsheet, tripsheetid";

	} else if(isset($_COOKIE['url'])){
		$sql       = "SELECT DISTINCT ON (t.tripsheet) tripsheet, t.tripsheetid, t.trip, t.created, t.version, t.group, distance, 
					fuel, targets, scores, asset, approved, verificationdate, verificationuser, driver, t.status, t.start, t.end, t.type, u.name AS user,
					(
						SELECT COUNT(v.tripsheetid) FROM tbltripsheet v WHERE v.parentid=t.tripsheetid
					) AS versions
					FROM tbltripsheet t, tbl_user u
					WHERE t.userid=u.id AND t.groupid=$group_id AND t.status<>'DELETED' AND t.end >= '$startDate' AND t.end < '$endDate'
					ORDER BY tripsheet, tripsheetid";

	} else {
		$sql       = "SELECT DISTINCT ON (t.tripsheet) tripsheet, t.tripsheetid, t.trip, t.created, t.version, t.group, distance, 
					fuel, targets, scores, asset, approved, verificationdate, verificationuser, driver, t.status, t.start, t.end, t.type, u.name AS user,
					(
						SELECT COUNT(v.tripsheetid) FROM tbltripsheet v WHERE v.parentid=t.tripsheetid
					) AS versions
					FROM tbltripsheet t, tbl_user u   
					WHERE t.userid=u.id AND t.groupid=$group_id AND t.status<>'DELETED' AND t.created > (CURRENT_DATE - INTERVAL '91 days')    
					ORDER BY tripsheet, tripsheetid";
	}
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		$tDate = array_column($rows, 'created');
		array_multisort($tDate, SORT_DESC, $rows);
		return $rows;
	}
	return array();
}

function get_trip_sheets($dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT ON (t.tripsheet) tripsheet, tripsheetid, trip, 
					(
						SELECT COUNT(v.tripsheetid) FROM tbltripsheet v WHERE v.parentid=t.tripsheetid
					) AS versions      
					FROM tbltripsheet t ORDER BY tripsheet, t.start";
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_gt_trip_sheets($dbh, $dbn, $dbu, $dbp){

	$sql       = "SELECT DISTINCT ON (t.tripsheet) tripsheet, t.tripsheetid, t.groupid, t.assetid, 
					t.trip, g.client_account AS group, t.distance_actual,
				 	t.distance_empty,t.distance_fm, t.distance_gt, t.fuel_actual, t.fuel_fm, 
				 	t.fuel_gt, t.startdate, t.enddate, t.created, t.userid, t.spikes, t.svc_km,
				 	t.svc_last_date, t.svc_next_date, t.loading_id, t.offloading_id,t.product_id, 
				 	t.parent_id, t.version, t.comment, a.unit_name AS asset, u.name AS user, 
					(SELECT name FROM tbldriver_gt WHERE driverid IN
						(SELECT driverid FROM tbl_tripexception e WHERE e.tripsheetid=t.tripsheetid LIMIT 1)
					) AS driver, 
					(SELECT COUNT(v.tripsheetid) FROM tbl_tripsheet v 
						WHERE v.parent_id=t.tripsheetid
					) AS versions      
					FROM tblunit_gt a, tbl_tripsheet t, tbl_user u, tblclient_gt g           
					WHERE t.assetid=a.unit_id AND t.userid=u.id AND t.groupid=g.client_id     
					ORDER BY tripsheet, tripsheetid, t.startdate, t.enddate;";
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_customer_trip_sheets($groupid, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT ON (t.tripsheet) tripsheet, t.tripsheetid, t.groupid, t.assetid, t.trip, 
					t.distance_actual,
				 	t.distance_empty,t.distance_fm, t.distance_gt, t.fuel_actual, t.fuel_fm, 
				 	t.fuel_gt, t.startdate, t.enddate, t.created, t.userid, t.spikes, t.svc_km,
				 	t.svc_last_date, t.svc_next_date, t.loading_id, t.offloading_id,t.product_id, 
				 	t.parent_id, t.version, t.comment, a.description AS asset, u.name AS user, 
					(
						SELECT COUNT(DISTINCT driverid) FROM tbl_tripexception e 
						WHERE e.tripsheetid=t.tripsheetid
					) AS drivers, 
					(
						SELECT COUNT(v.tripsheetid) FROM tbl_tripsheet v 
						WHERE v.parent_id=t.tripsheetid
					) AS versions      
					FROM tblasset a, tbl_tripsheet t, tbl_user u     
					WHERE t.groupid=$groupid AND t.assetid=a.assetid AND t.userid=u.id    
					ORDER BY tripsheet, tripsheetid, t.startdate, t.enddate;";
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_gt_group_trip_sheets($groupid, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT ON (t.tripsheet) tripsheet, t.tripsheetid, t.groupid, t.assetid, t.trip, 
					t.distance_actual,
				 	t.distance_empty,t.distance_fm, t.distance_gt, t.fuel_actual, t.fuel_fm, 
				 	t.fuel_gt, t.startdate, t.enddate, t.created, t.userid, t.spikes, t.svc_km,
				 	t.svc_last_date, t.svc_next_date, t.loading_id, t.offloading_id,t.product_id, 
				 	t.parent_id, t.version, t.comment, a.unit_name AS asset, u.name AS user, 
					(
						SELECT COUNT(DISTINCT driverid) FROM tbl_tripexception e 
						WHERE e.tripsheetid=t.tripsheetid
					) AS drivers, 
					(
						SELECT COUNT(v.tripsheetid) FROM tbl_tripsheet v 
						WHERE v.parent_id=t.tripsheetid
					) AS versions      
					FROM tblunit_gt a, tbl_tripsheet t, tbl_user u     
					WHERE t.groupid=$groupid AND t.assetid=a.unit_id AND t.userid=u.id    
					ORDER BY tripsheet, tripsheetid, t.startdate, t.enddate;";
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_tripsheet_versions($parentid, $dbh, $dbn, $dbu, $dbp){
	$sql 	   = "SELECT t.*, u.name AS user, 
						(SELECT COUNT(v.tripsheetid) FROM tbltripsheet v WHERE v.parentid=t.tripsheetid) AS versions  
					FROM tbltripsheet t, tbl_user u  
					WHERE t.tripsheet=(select p.tripsheet from tbltripsheet p WHERE tripsheetid=$parentid) AND userid=u.id AND t.status<>'DELETED'  
					ORDER BY tripsheetid";
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_tsheet_info($tripsheetid, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT t.*, a.description AS asset, u.name AS author, 
					(
						SELECT COUNT(DISTINCT driverid) FROM tbl_tripexception e 
						WHERE e.tripsheetid=t.tripsheetid
					) AS drivers  
					FROM tblasset a, tbl_tripsheet t, tbl_user u      
					WHERE t.tripsheetid=$tripsheetid AND t.assetid=a.assetid AND userid=u.id      
					ORDER BY t.startdate, t.enddate;";
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	$scores    = get_tsheet_driver_scores($tripsheetid,$dbh,$dbn,$dbu,$dbp);

	// print_r($scores);
	// exit;

	$rows[0]['driver']	  = $scores[0]['driver'];
	$rows[0]['driverid']  = $scores[0]['driverid'];

	return $rows[0];
}

function get_tripsheet_ids_by_range($group,$customer,$start,$end,$dbh,$dbn,$dbu,$dbp){
	$sql  = "SELECT DISTINCT ON (tripsheet) tripsheet, tripsheetid FROM tbl_tripsheet    
				WHERE enddate>'$start' AND enddate<'$end' ";
	if(intval($group)!=0){ 	$sql .= "AND groupid=$group "; 		 }
	if(intval($customer)!=0){  $sql .= "AND customerid=$customer "; }
	$sql .= "ORDER BY tripsheet, tripsheetid DESC;";

	// echo $sql;
	// exit;

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);
	$returnArr = array();
	if (is_array($rows)) {
		foreach ($rows as $r) {
			$returnArr[] = $r['tripsheetid'];
		}
	}

	return $returnArr;
}

function get_tripsheets_for_export($group_id,$cust_id,$start,$end,$dbh,$dbn,$dbu,$dbp){
	$params    = "";
	if (intval($group_id) != 0) { $params .= "t.groupid=$group_id AND "; 	}
	if (intval($cust_id)  != 0) { $params .= "gcustomerid=$cust_id AND "; 	}
	
	$sql       = "SELECT DISTINCT ON (t.tripsheet) t.*, u.name AS user,  
					(
						SELECT COUNT(v.tripsheetid) FROM tbltripsheet v WHERE v.parentid=t.tripsheetid
					) AS versions      
					FROM tbltripsheet t, tbl_user u     
					WHERE $params 
						t.userid=u.id AND t.created>='$start' AND t.created<='$end' AND t.status<>'DELETED'     
					ORDER BY tripsheet, created DESC";

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);
	
	if ($rows) {
		return $rows;
	}
	return array();
}

function get_tripsheets_by_params($groupid, $siteid, $customerid, $start, $end, $useDate, $dbh, $dbn, $dbu, $dbp)
{
	$params    = "";
	if (is_numeric($groupid) && $groupid!=0) 	{ $params .= "t.groupid=$groupid AND "; 	}
	if (is_numeric($siteid) && $siteid!=0) 		{ $params .= "(d.siteid=$siteid OR a.siteid=$siteid) AND "; 	}
	if (is_numeric($customerid) && $customerid!=0) { 
		$cstPattern  = '%\"customer_id\\\":\\\"'.$customerid.'\\\"%'; 
		$params 	.= "(t.gcustomerid=$customerid OR t.products LIKE '$cstPattern') AND "; 	
	}

	if ($useDate=='close') 	{ $params .= "t.end>='$start' AND t.end<='$end' AND "; } 
	else { 					  $params .= "t.created>='$start' AND t.created<='$end' AND "; }
	
	$sql       = "SELECT * FROM (
					  SELECT DISTINCT ON (t.tripsheet) tripsheet, t.*, a.siteid AS assetsiteid, d.siteid AS driversiteid, u.name AS user, 
						(SELECT c.name FROM tbl_customer c WHERE t.gcustomerid=c.id) AS customer, 
						(SELECT p.name FROM tbl_product p WHERE t.productid=p.id) AS product, 
						(SELECT l.name FROM tbl_depot l WHERE t.loadingid=l.id) AS loading, 
						(SELECT o.name FROM tbl_depot o WHERE t.offloadid=o.id) AS offload,   
						(SELECT ag.name FROM tblasset a INNER JOIN tblsubgroup ag ON a.siteid=ag.subgroupid WHERE a.assetid=t.assetid) AS assetsite, 
						(SELECT dg.name FROM tbldriver d INNER JOIN tblsubgroup dg ON d.siteid=dg.subgroupid WHERE d.driverid=t.driverid) AS driversite 
					  FROM tbltripsheet t, tblasset a, tbldriver d, tbl_user u        
					  WHERE $params t.userid=u.id AND a.assetid=t.assetid AND d.driverid=t.driverid AND t.status<>'DELETED'   
					  ORDER BY tripsheet, tripsheetid DESC) AS foo 
				  ORDER BY ";
	if ($useDate=='close') 	{ $sql .= " foo.end "; 		}
	else 					{ $sql .= " foo.created "; 	}

	// echo $sql;
	// exit;

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function get_tripsheets_export_data($groupid, $siteid, $customerid, $start, $end, $useDate, $grpCfg, $dbh, $dbn, $dbu, $dbp){
	$rows 	   = get_tripsheets_by_params($groupid, $siteid, $customerid, $start, $end, $useDate, $dbh, $dbn, $dbu, $dbp);
	$returnArr = array();

	if (is_array($rows)) {
		for ($i=0; $i<count($rows); $i++) {
			$ts 		 = $rows[$i];

			$defaultCons = true;
			$varLoads    = true;
		    $showTargets = true;
		    $showGt      = true;
		    foreach ($grpCfg AS $cnf) {
		        if ($cnf['id']==$ts['groupid']) {
		            $defaultCons = $cnf['consumption']=='km/l' ? true : false;
		            $varLoads    = $cnf['variableCons'];
		            $showTargets = $cnf['showtargets'];
		            $showGt      = $cnf['showgt'];
		        }
		    }

			$key 		 = strtolower(str_replace(" ","_", $ts['driver']));
			$trip 		 = json_decode(stripslashes($ts['trip']), true);
			$fuel 		 = json_decode(stripslashes($ts['fuel']), true);
			$scores 	 = json_decode(stripslashes($ts['scores']), true);
			$service 	 = json_decode(stripslashes($ts['service']), true);
			$targets  	 = json_decode(stripslashes($ts['targets']), true);
			$duration 	 = json_decode(stripslashes($ts['duration']), true);
			$products	 = json_decode(stripslashes($ts['products']), true);
			$distances 	 = json_decode(stripslashes($ts['distance']), true);
			$exceptions	 = json_decode(stripslashes($ts['exceptions']), true);

			$std 		 = isset($exceptions['standard'][$key]['violations']) ? $exceptions['standard'][$key]['violations'] : null;
			$cst 		 = isset($exceptions['custom'][$key]['violations']) ? $exceptions['custom'][$key]['violations'] : null;

			// print_r($scores);
			// exit;
			$dynamicFuel = 0; 
            if ($ts['type'] == "GT") {     $dynamicFuel = $fuel['gt'];  }
            elseif ($ts['type'] == "FM") { $dynamicFuel = $fuel['fm'];  }
			$dynamicDist         = ($distances['fm_edit']!=$distances['fm'] && floatval($distances['fm_edit'])>0) ? 
										floatval($distances['fm_edit']) : floatval($distances['fm']);
			$actualDist          = (floatval($distances['actual'])>0) ? floatval($distances['actual']) : 0;
			$targetDist          = (floatval($targets['distance'])>0) ? floatval($targets['distance']) : 0;
			$actualFuel          = (floatval($fuel['actual'])>0) ? floatval($fuel['actual']) : 0;

		    $actualConsumption   = ($actualFuel>0) ? $actualDist/$actualFuel : 0;
		    $actAltConsumption 	 = ($actualDist>0) ? ($actualFuel/$actualDist)*100 : 0;
		    $fmAltConsumption    = ($dynamicDist>0) ? ($fuel['fm']/$dynamicDist)*100 : 0;
		    $dynamicConsumption  = ($dynamicFuel>0) ? $dynamicDist/$dynamicFuel : 0; 
    		$scores['fuel']      = !$showTargets ? ($actualFuel>0 ? ($fuel['fm']/$actualFuel)*100 : 0) : $scores['fuel'];
		    $scores['fuel']      = $scores['fuel']>100 ? 100 : $scores['fuel'];
		    $loadPc  			 = $varLoads ? calc_load_percent_new($ts['groupid'], $trip['long'], $dbh, $dbn, $dbu, $dbp) : 
		    						calc_load_percent($ts['groupid'], $trip['long'], $dbh, $dbn, $dbu, $dbp);
		    $trunk 				 = $targetDist;
		    $shunt       		 = $dynamicDist>$targetDist ? $dynamicDist-$targetDist : 0;

		    $fuelTarget 		 = 0;
            if ($ts['groupid']  == -3273034392599683215) {
                $fuelTarget 	 = ($targets['consumption']>0) ? $dynamicDist/$targets['consumption'] : 0;
            }
            else{
                $fuelTarget 	 = $targets['fuel'];
            }
   			// ($showTargets ? 
			// 	(isset($targets['fuel'])&&isset($fuel['actual']) ? number_format($targets['fuel']-$fuel['actual'],1) : 0) : 
			// 	(number_format($fuel['fm']-$fuel['actual'],1))
			// )

			$startCAT   = new DateTime($ts['start'] .' (UTC)');
            $endCAT     = new DateTime($ts['end'] .' (UTC)');
            $startCAT   ->setTimezone(new DateTimeZone('CAT'));
            $endCAT     ->setTimezone(new DateTimeZone('CAT'));
            $product 	= isset($products['loads'][0]) ? $products['loads'][0] : array();

			$returnArr[] = array(
				"Date"=>$ts['created'], 
				"Produced By"=>$ts['user'],
				"Group"=>$ts['group'],
				"Date Out"=>$startCAT->format('d/m/Y H:i'),
				"Date In"=>$endCAT->format('d/m/Y H:i'),
				"Tripsheet"=>$ts['tripsheet'],
				"Route"=>$trip['long'], 
				"Horse"=>$ts['asset'],
				"Asset Site"=>$ts['assetsite'],
				"Driver"=>$ts['driver'],  
				"Driver Site"=>$ts['driversite'],  
				"Manifest"=>  isset($product['manifest']) ? $product['manifest'] : "",
				"Direction"=> isset($product['direction']) ? $product['direction'] : "", 
				"Customer"=>  isset($product['customer_name']) ? $product['customer_name'] : "",
				"Product"=>   isset($product['product_name']) ? $product['product_name'] : "",  
				"Qty"=>  	  isset($product['quantity']) && is_numeric($product['quantity']) ? number_format($product['quantity'],1) : "",  
				"Units"=>     isset($product['units']) ? $product['units'] : "", 
				"Loading"=>   isset($product['loading_name']) ? $product['loading_name'] : "", 
				"Offload"=>   isset($product['offload_name']) ? $product['offload_name'] : "", 
				"Load %"=>number_format($loadPc,2), 
				"Target Distance"=>number_format($targetDist,1), 
				"FM Distance"=>number_format($dynamicDist), 
				"Actual Distance"=>number_format($actualDist,1), 
				"Empty Distance"=>(number_format(($trunk*(100-$loadPc)/100)+($shunt*0.5),1)), 
				"Distance +/-"=>number_format($targetDist-$actualDist,1), 
				"Target Fuel"=>isset($targets['fuel']) ? number_format($targets['fuel'],1) : 0, 
				"Actual Fuel"=>isset($fuel['actual']) ? number_format($fuel['actual'],1) : 0, 
				"FM Fuel"=>isset($fuel['fm']) ? number_format($fuel['fm'],1) : 0, 
				"Fuel +/-"=>($showTargets ? number_format($fuelTarget-$fuel['actual'],1) : number_format($fuel['fm']-$fuel['actual'],1)),
				"Target Consumption"=>(($showTargets && isset($targets['consumption'])) ? number_format($targets['consumption'],2) : 0), 
				"FM Consumption"=>(show_consumption($dynamicConsumption,$defaultCons)),
				"Actual Consumption"=>(show_consumption((($fuel['actual']>0) ? $distances['actual']/$fuel['actual'] : 0),$defaultCons)),
				"Last Service"	 =>$service['last_date'], 
				'Next Service' 	 =>$service['next_date'], 
				'Days in Transit'=>number_format($duration['actual']/86400,2), 
				'OS Score' 		 => isset($scores['speed']) ? number_format($scores['speed'],2) : 0,
				'OS Count' 		 => (isset($std['speeding']) ? $std['speeding']['occur'] : 0), 
				'OS Max' 		 => (isset($std['speeding']) ? $std['speeding']['value'] : 0),
				'OS Duration' 	 => seconds_to_time_min((isset($std['speeding']) ? $std['speeding']['duration'] : 0)),
				'OR Score' 		 => isset($scores['revv']) ? number_format($scores['revv'],2) : 0,
				'OR Count' 		 => (isset($std['revving']) ? $std['revving']['occur'] : 0),
				'OR Max' 		 => (isset($std['revving']) ? $std['revving']['value'] : 0),
				'OR Duration' 	 => seconds_to_time_min((isset($std['revving']) ? $std['revving']['duration'] : 0)),
				'HA Score' 		 => isset($scores['accl']) ? number_format($scores['accl'],2) : 0,
				'HA Count' 		 => (isset($std['acceleration']) ? $std['acceleration']['occur'] : 0),
				'HA Max' 		 => (isset($std['acceleration']) ? $std['acceleration']['value'] : 0),
				'HA Duration' 	 => seconds_to_time_min((isset($std['acceleration']) ? $std['acceleration']['duration'] : 0)),
				'HB Score' 		 => isset($scores['brake']) ? number_format($scores['brake'],2) : 0,
				'HB Count' 		 => (isset($std['braking']) ? $std['braking']['occur'] : 0),
				'HB Max' 		 => (isset($std['braking']) ? $std['braking']['value'] : 0),
				'HB Duration' 	 => seconds_to_time_min((isset($std['braking']) ? $std['braking']['duration'] : 0)),
				'EI Score' 		 => isset($scores['idle']) ? number_format($scores['idle'],2) : 0,
				'EI Count' 		 => (isset($std['idle']) ? $std['idle']['occur'] : 0),
				'EI Duration' 	 => seconds_to_time_min((isset($std['idle']) ? $std['idle']['duration'] : 0)),
				'GB Score' 		 => (isset($std['green_band']) ? $std['green_band']['score'] : 0),
				'GB Duration' 	 => seconds_to_time_min((isset($std['green_band']) ? $std['green_band']['duration'] : 0)),
				'GB Score' 		 => isset($scores['green']) ? number_format($scores['green'],2) : 0,
				'TS Count' 		 => (isset($cst['town']) ? $cst['town']['occur'] : 0),
				'TS Duration' 	 => seconds_to_time_min((isset($cst['town']) ? $cst['town']['duration'] : 0)),
				'ES Count' 		 => (isset($cst['escarpment']) ? $cst['escarpment']['occur'] : 0),
				'ES Duration' 	 => seconds_to_time_min((isset($cst['escarpment']) ? $cst['escarpment']['duration'] : 0)),
				'FW Count' 		 => (isset($cst['free']) ? $cst['free']['occur'] : 0),
				'FW Duration' 	 => seconds_to_time_min((isset($cst['free']) ? $cst['free']['duration'] : 0)),
				'NTD Count' 	 => (isset($cst['night']) ? $cst['night']['occur'] : 0),
				'NTD Duration' 	 => seconds_to_time_min((isset($cst['night']) ? $cst['night']['duration'] : 0)),
				'CD Count' 		 => (isset($cst['continuous']) ? $cst['continuous']['occur'] : 0),
				'CD Duration' 	 => seconds_to_time_min((isset($cst['continuous']) ? $cst['continuous']['duration'] : 0)),
				'Panic Pressed'  => (isset($cst['panic']['count']) ? $cst['panic']['count'] : 0),
				'Transits Score' => isset($scores['transits']) ? number_format($scores['transits'],2) : 0,
				'Fuel Score' 	 => $ts['groupid']==1853956352811964940 ? 
					(isset($fuel['actual']) ? number_format($fuel['fm']/$fuel['actual'],2) : 0) : 
					(isset($scores['fuel']) ? number_format($scores['fuel'],2) : 0),
				'FM Score' 		 => ($scores['overall_edit']!=$scores['overall'] && $scores['overall_edit']>0) ? 
					number_format($scores['overall_edit'],2) : number_format($scores['overall'],2),
				'Advanced Score' => isset($scores['advanced']) ? number_format($scores['advanced'],2) : 0,
				'Sendem Score' 	 => isset($scores['sendem']) ? number_format($scores['sendem'],2) : 0
			); 
		}

	}
	
	return $returnArr;
}

function get_tripsheets_billing_data($groupid, $siteid, $start, $end, $grpCfg, $dbh, $dbn, $dbu, $dbp){
	$params    = "";
	if (is_numeric($groupid) && $groupid!=0) 	    { $params .= "ts.groupid=$groupid AND "; 	}


	$sql       = "SELECT DISTINCT(ts.asset) AS assetDescription, count(ts.tripsheet) AS tripsheetsClosed, t2.name
					FROM tbltripsheet ts INNER JOIN tblasset t ON ts.assetid = t.assetid INNER JOIN tblsubgroup t2 ON t.siteid = t2.subgroupid
					WHERE $params  (ts.created >= '$start' AND ts.created <= '$end')
					GROUP BY ts.asset, t2.name ORDER BY ts.asset ASC";

	// echo $sql;
	// exit;

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);
	$returnArr = array();
	if (is_array($rows) && count($rows)>0) {
		foreach ($rows as $r) {
			$returnArr[] = array(
				'Asset Description' 	=> $r['assetdescription'], 
				'Tripsheets Closed' 	=> $r['tripsheetsclosed'], 
				'Site' 	=> $r['name']
			);
		}
	}	
	return $returnArr;
}

function get_asset_billing_data($groupid, $siteid, $start, $end, $grpCfg, $dbh, $dbn, $dbu, $dbp){
	$params    = "";
	if (is_numeric($groupid) && $groupid!=0) 	{ $params .= "t.groupid=$groupid AND "; 	}
	if (is_numeric($siteid) && $siteid!=0) 		{ $params .= "(d.siteid=$siteid OR a.siteid=$siteid) AND "; 	}


	$sql = "SELECT DISTINCT(t2.description) as assetDescription, t3.name, min(t.tripstart) as firstTrip, max(t.tripend) as lastTrip
				from tbltrip t inner join tblasset t2 on t.assetid = t2.assetid inner join tblsubgroup t3 on t2.siteid = t3.subgroupid
				where  $params t.tripstart >= '$start' and t.tripend <= '$end'
				group by t2.description, t3.name order by t2.description asc";

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);
	$returnArr = array();
	if (is_array($rows) && count($rows)>0) {
		foreach ($rows as $r) {
			$returnArr[] = array(
				'Asset Description' => $r['assetdescription'], 
				'Site' 				=> $r['name'], 
				'First Trip' 	=> $r['firsttrip'], 
				'Last Trip' 	=> $r['lasttrip']
			);
		}
	}	
	return $returnArr;
}

function get_tsheet_driver_scores($tripsheet_id,$dbh,$dbn,$dbu,$dbp){
	$sql       = "SELECT e.*, d.name AS driver        
					FROM tbl_tripexception e, tbldriver d              
					WHERE     
						e.tripsheetid=$tripsheet_id AND e.driverid=d.driverid   
						AND sc_fuel>0 AND sc_adv>0 AND sc_sendem>0 AND sc_green>0          
					ORDER BY driver;";
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);
	$returnArr = array();

	if (is_array($rows) && count($rows)>0) {
		foreach ($rows as $r) {
			if (count($returnArr)<1) {
				$returnArr[] = array(
					'driverid' 	=> $r['driverid'], 
					'driver' 	=> $r['driver'], 
					'sc_adv' 	=> $r['sc_adv'], 
					'sc_fuel' 	=> $r['sc_fuel'], 
					'sc_fm' 	=> $r['sc_fm'], 
					'sc_green' 	=> $r['sc_green'], 
					'sc_sendem' => $r['sc_sendem'], 
					'speeding' 		=> array('count'=>0,'value'=>$r['speed'],'duration'=>0), 
					'revving' 		=> array('count'=>0,'value'=>0,'duration'=>0), 
					'acceleration' 	=> array('count'=>0,'value'=>0,'duration'=>0), 
					'braking' 		=> array('count'=>0,'value'=>0,'duration'=>0), 
					'idle' 			=> array('count'=>0,'value'=>0,'duration'=>0), 
					'green_band' 	=> array('count'=>0,'value'=>0,'duration'=>0), 
					'town' 			=> array('count'=>0,'value'=>0,'duration'=>0), 
					'escarpment' 	=> array('count'=>0,'value'=>0,'duration'=>0), 
					'free' 			=> array('count'=>0,'value'=>0,'duration'=>0), 
					'night' 		=> array('count'=>0,'value'=>0,'duration'=>0), 
					'continuous' 	=> array('count'=>0,'value'=>0,'duration'=>0), 
					'panic' 		=> array('count'=>0,'value'=>0,'duration'=>0), 
				);
			}
			if ($r['eventname']=='speeding') {
				$returnArr[0][$r['eventname']] = array(
					'count'		=> $r['count'],
					'duration'	=> $r['duration']
				);
				if (isset($returnArr[0]['speeding']['value']) && $returnArr[0]['speeding']['value']<$r['maximum']) {
					$returnArr[0]['speeding']['value']<$r['maximum'];
				}
			} 
			else {
				$returnArr[0][$r['eventname']] = array(
					'count'		=> $r['count'],
					'duration'	=> $r['duration'],
					'value'		=> $r['maximum']
				);
			}

			if(isset($returnArr[0]['speeding']['value']) && $returnArr[0]['speeding']['value']<$r['speed']){
				$returnArr[0]['speeding']['value']<$r['speed'];
			}
		}
	}
	return $returnArr;
}

function get_fm_driver_history($driver_id,$dbh,$dbn,$dbu,$dbp){
	$sql       = "SELECT e.*, t.tripsheet, t.trip, t.fuel_actual, t.fuel_fm, t.distance_fm, t.startdate, 
						a.description AS asset     
					FROM tbl_tripexception e, tblasset a, tbl_tripsheet t      
					WHERE    
						e.driverid=$driver_id AND e.tripsheetid=t.tripsheetid   
						AND e.assetid=a.assetid AND t.status<>'DELETED'    
					ORDER BY t.startdate;";

					// echo $sql;
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);
	$history   = array();

	if (is_array($rows)) {
		foreach ($rows as $r) {
			$tskey = strtolower(str_replace(" ", "_", $r['tripsheet']));
			if (!isset($history[$tskey])) {
				$history[$tskey] = array(
					'tripsheet'  => $r['tripsheet'], 
					'assetid' 	 => $r['assetid'], 
					'asset' 	 => $r['asset'], 
					'trip' 	 	 => $r['trip'], 
					'fuelused' 	 => $r['fuel_actual'], 
					'fuelfm' 	 => $r['fuel_fm'], 
					'distfm' 	 => $r['distance_fm'], 
					'speeding' 	 => ($r['eventname']=='speeding') ? 
						array('count'=>$r['count'],'duration'=>$r['duration'],'value'=>$r['maximum']) :
						array('count'=>0,'duration'=>0,'value'=>$r['speed']), 
					'escarpment' => ($r['eventname']=='escarpment') ? 
						array('count'=>$r['count'],'duration'=>$r['duration'],'value'=>$r['maximum']) :
						array('count'=>0,'duration'=>0,'value'=>0), 
					'freewheel'  => ($r['eventname']=='free') ?  
						array('count'=>$r['count'],'duration'=>$r['duration']) :
						array('count'=>0,'duration'=>0),
					'night' 	 => ($r['eventname']=='night') ?  
						array('count'=>$r['count'],'duration'=>$r['duration']) :
						array('count'=>0,'duration'=>0),
					'fuelscore'  => $r['sc_fuel']
				);
			} 
			else {
				if ($history[$tskey]['speeding']['value']<$r['speed']) {
					$history[$tskey]['speeding']['value'] 		= $r['speed'];
				}
				if($r['eventname']=='speeding') { 	
					$history[$tskey]['speeding']['count'] 	 	= $r['count'];
					$history[$tskey]['speeding']['duration'] 	= $r['duration'];
					if ($history[$tskey]['speeding']['value']<$r['maximum']) {
						$history[$tskey]['speeding']['value'] 	= $r['maximum'];
					}
				}
				if($r['eventname']=='escarpment') { 
					$history[$tskey]['escarpment']['count'] 	= $r['count'];
					$history[$tskey]['escarpment']['duration'] 	= $r['duration'];
					$history[$tskey]['escarpment']['value'] 	= $r['maximum'];
				}
				if($r['eventname']=='free') { 		
					$history[$tskey]['freewheel']['count'] 	 	= $r['count'];
					$history[$tskey]['freewheel']['duration'] 	= $r['duration'];
				}
				if($r['eventname']=='night') { 		
					$history[$tskey]['night']['count'] 	 		= $r['count'];
					$history[$tskey]['night']['duration'] 		= $r['duration'];
				}
			}
        }
	}

	return $history;
}

function get_recent_driver_history($group_id, $driver_id,$maxDate,$dbh,$dbn,$dbu,$dbp, $tripsheetid=0){
	// $maxDate   = gmdate("Y-m-d H:i:s");
	// if ($tripsheetid>0) {
	// 	$maxDate = get_tripsheet($tripsheetid, $dbh, $dbn, $dbu, $dbp)['end'];
	// }
	$sql       = "SELECT DISTINCT ON (tripsheet) tripsheet, tripsheetid, t.end, trip, asset, driver, fuel, distance, targets, scores, exceptions, 
					u.name AS user   
					FROM tbltripsheet t, tbl_user u   
					WHERE u.id=t.userid AND driverid=$driver_id AND t.groupid=$group_id AND t.end<='$maxDate' AND t.status<>'DELETED'   
					GROUP BY t.end, tripsheet, tripsheetid, trip, asset, driver, fuel, distance, targets, scores, exceptions, u.name";

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	if ($db_result) {
		$rows      = pg_fetch_all($db_result);

		if ($rows) {
			$tDate = array_column($rows, 'end');
			array_multisort($tDate, SORT_DESC, $rows);
			return $rows;
		}
	}
	return array();
}

function get_tsheet_scores($tripsheetid, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT sc_adv, sc_fuel, sc_fm, sc_sendem, sc_green    
					FROM tbl_tripexception     
					WHERE tripsheetid=$tripsheetid   
					ORDER BY sc_sendem DESC   
					LIMIT 1";
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows[0];
}

function get_tsheet_incidents($tripsheetid, $dbh, $dbn, $dbu, $dbp){
	$sql       = "SELECT DISTINCT d.name AS driver, i.type, i.description, remedy         
					FROM tbl_incident i, tbldriver d     
					WHERE i.tripsheetid=$tripsheetid AND i.driverid=d.driverid";
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function save_trip_sheet_legacy($groupid, $assetid, $tripsheet, $trip, $distance_actual, $distance_empty, $distance_fm, $distance_gt, $fuel_actual, $fuel_fm, $fuel_gt, $startdate, $enddate, $exceptions, $spikes, $incidents, $sc_grn, $sc_adv, $sc_fl, $sc_fm, $sc_sen, $driverid, $svc_km, $svc_last_date, $svc_next_date,$loading_id,$offloading_id,$product_id,$parentid,$version,$comment, $customerid, $dbh, $dbn, $dbu, $dbp){

	// echo $exceptions ."<br>";

	// echo "g $groupid, a $assetid, ts $tripsheet, tp $trip, da $distance_actual, de $distance_empty, df $distance_fm, dg $distance_gt, fa $fuel_actual, ff $fuel_fm, fg $fuel_gt, sd $startdate, ed $enddate, ex --, sp $spikes, in $incidents, sg $sc_grn, sa $sc_adv, sf $sc_fl, sm $sc_fm, ss $sc_sen, di $driverid, sk $svc_km, ld $svc_last_date, nd $svc_next_date, li $loading_id, oi $offloading_id, pi $product_id, pr $parentid, vr $version, cm $comment, cs $customerid";
	// exit;

	// $sql 	  = "INSERT INTO public.tbl_tripsheet (groupid, assetid, tripsheet, trip, distance_actual, distance_empty, 
	// 			distance_fm, distance_gt, fuel_actual, fuel_fm, fuel_gt, startdate, enddate, created, userid, spikes, svc_km, svc_last_date, svc_next_date, loading_id, offloading_id, product_id,parent_id,version,comment,customerid) VALUES 
	// 			(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING tripsheetid;";

	$sql 	  = "INSERT INTO public.tbl_tripsheet (groupid, assetid, tripsheet, trip, distance_actual, distance_empty, 
				distance_fm, distance_gt, fuel_actual, fuel_fm, fuel_gt, startdate, enddate, created, userid, spikes, loading_id, offloading_id, product_id,parent_id,version,comment,customerid) VALUES 
				(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING tripsheetid;";
	
	$database = new Database();
	$created  = gmdate("Y-m-d H:i:s"); 
	$userid   = $_SESSION['user']['id'];
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$loading_id 	= ($loading_id=="") ? 0 : $loading_id;
	$offloading_id 	= ($offloading_id=="") ? 0 : $offloading_id;
	$product_id 	= ($product_id=="") ? 0 : $product_id;
	$parentid 		= ($parentid=="") ? 0 : $parentid;

	$stmt     ->bindParam(1,  $groupid);
	$stmt     ->bindParam(2,  $assetid);
	$stmt     ->bindParam(3,  $tripsheet);
	$stmt     ->bindParam(4,  $trip);
	$stmt     ->bindParam(5,  $distance_actual);
	$stmt     ->bindParam(6,  $distance_empty);
	$stmt     ->bindParam(7,  $distance_fm);
	$stmt     ->bindParam(8,  $distance_gt);
	$stmt     ->bindParam(9,  $fuel_actual);
	$stmt     ->bindParam(10, $fuel_fm);
	$stmt     ->bindParam(11, $fuel_gt);
	$stmt     ->bindParam(12, $startdate);
	$stmt     ->bindParam(13, $enddate);
	$stmt     ->bindParam(14, $created);
	$stmt     ->bindParam(15, $userid);
	$stmt     ->bindParam(16, $spikes);
	$stmt     ->bindParam(17, $loading_id);
	$stmt     ->bindParam(18, $offloading_id);
	$stmt     ->bindParam(19, $product_id);
	$stmt     ->bindParam(20, $parentid);
	$stmt     ->bindParam(21, $version);
	$stmt     ->bindParam(22, $comment);
	$stmt     ->bindParam(23, $customerid);

	if ($stmt ->execute()) {
		$_id  = $db_conn->lastInsertId();
		// SAVE EXCEPTIONS
		$sql  = "INSERT INTO tbl_tripexception (tripsheetid, driverid, groupid, assetid, eventname, exceptiontype, count, duration, maximum, speed, sc_green, sc_adv, sc_fuel, sc_fm, sc_sendem) VALUES";
		$ex_array = explode(";", $exceptions);

		// print_r($ex_array);

		foreach($ex_array AS $drv_excp){
			if(ltrim(rtrim($drv_excp))!=""){
				$e_param = explode("|", $drv_excp);
				$sql    .= "($_id, ";
				$sql 	.= (ltrim(rtrim($e_param[0]))=="") ? "0," : $e_param[0].",";
				$sql 	.= (ltrim(rtrim($e_param[1]))=="") ? "0," : $e_param[1].",";
				$sql 	.= (ltrim(rtrim($e_param[2]))=="") ? "0," : $e_param[2].",";
				$sql 	.= (ltrim(rtrim($e_param[3]))=="") ? "'0'," : "'".$e_param[3]."',";
				$sql 	.= (ltrim(rtrim($e_param[4]))=="") ? "'0'," : "'".$e_param[4]."',";
				$sql 	.= (ltrim(rtrim($e_param[5]))=="") ? "0," : $e_param[5].",";
				$sql 	.= (ltrim(rtrim($e_param[6]))=="") ? "0," : $e_param[6].",";
				$sql 	.= (ltrim(rtrim($e_param[7]))=="") ? "0," : $e_param[7].",";
				$sql 	.= (ltrim(rtrim($e_param[8]))=="") ? "0," : $e_param[8].",";
				if(ltrim(rtrim($e_param[0])) == $driverid){
					$sql.= $sc_grn .",". $sc_adv .",". $sc_fl .",". $sc_fm .",". $sc_sen;
				}
				else{
					$sql.= "0,0,0,0,0";
				}
				$sql 	.= ")";
			}
		}
		$sql 	  = str_replace(")(","),(",$sql);
		$stmt     = $db_conn->prepare($sql);

		// echo "<br>". $sql ."<br>";

		if ($stmt ->execute()) {
			// SAVE INCIDENTS
			$sql  = "INSERT INTO tbl_incident (tripsheetid, driverid, groupid, assetid, type, description, remedy, added) VALUES";
			$inc_array = explode("#", $incidents);
			foreach($inc_array AS $inc){
				if(ltrim(rtrim($inc))!=""){
					$i_param = explode("|", $inc);
					$sql    .= "($_id, ";
					$sql 	.= (ltrim(rtrim($i_param[0]))=="") ? "0," : $i_param[0].",";
					$sql 	.= $groupid .", ";
					$sql 	.= $assetid .", ";
					$sql 	.= (ltrim(rtrim($i_param[1]))=="") ? "'0'," : "'".$i_param[1]."',";
					$sql 	.= (ltrim(rtrim($i_param[2]))=="") ? "'0'," : "'".$i_param[2]."',";
					$sql 	.= (ltrim(rtrim($i_param[3]))=="") ? "'0'," : "'".$i_param[3]."',";
					$sql 	.= "'".$created ."')";
				}
			}
			$sql 	  = str_replace(")(","),(",$sql);
			$stmt     = $db_conn->prepare($sql);

			if ($stmt ->execute()) {
				return array('result'=>true,'id'=>$_id,'message'=>'Tripsheet saved with id '. $_id);
			}
			else{
				$errors = $stmt->errorInfo();
				return array('result'=>true,'id'=>$_id,'message'=>'Tripsheet saved with id '.$_id,'error'=>$errors[2]);
			}
		}
		else{
			$errors = $stmt->errorInfo();
			return array('result'=>false,'id'=>$_id,'message'=>'Tripsheet saved with id '. $_id,'error'=>$errors[2]);
		}
	}
	else{
		$errors = $stmt->errorInfo();
		return array('result'=>false,'message'=>'Tripsheet not saved','error'=>$errors[2]);
	}
}


function  save_trip_sheet($type, $group_id, $group, $asset_id, $asset, $driver_id, $driver, $tripsheet, $tripJson, $distancesJson, $durationsJson, 
	$fuelJson, $targetsJson, $startdate, $enddate, $exceptionsJson, $axisEventsJson, $incidentsJson, $transitsJson, $scoresJson, $serviceJson, 
    $parent_id, $version, $comment, $trailers, $bonus, $products, $rpm_bands, $fill_ups, $dbh, $dbn, $dbu, $dbp ){

	// echo "$type, $group_id, $group, $asset_id, $asset, $driver_id, $driver, $tripsheet, ".strlen($tripJson).", ".strlen($distancesJson).", ".strlen($durationsJson).", 
	// ".strlen($fuelJson).", ".strlen($targetsJson).", $startdate, $enddate, ".strlen($exceptionsJson).", ".strlen($axisEventsJson).", ".strlen($incidentsJson).", ".strlen($transitsJson).", ".strlen($scoresJson).", ".strlen($serviceJson).", 
 //    $customer_id, $loading_id, $offload_id, $product_id, $parent_id, $version, $comment, ".strlen($trailers).", ".strlen($bonus).", ".strlen($products);

 //    exit;

	$sql 	  = "INSERT INTO tbltripsheet (groupid, \"group\", assetid, asset, driverid, driver, userid, parentid,  
							\"type\", tripsheet, trip, distance, fuel, duration, targets, \"start\", \"end\", exceptions, axisevents, scores,
							transits, service, \"version\", \"comment\", \"created\", incidents, trailers, bonus, products,rpmbands,fill_ups)  
						VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING tripsheetid;";

	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$userid    = $_SESSION['user']['id'];
	$created   = gmdate("Y-m-d H:i:s"); 
	$parent_id = ($parent_id=="") ? 0 : $parent_id;

	$stmt     ->bindParam(1,  $group_id);
	$stmt     ->bindParam(2,  $group);
	$stmt     ->bindParam(3,  $asset_id);
	$stmt     ->bindParam(4,  $asset);
	$stmt     ->bindParam(5,  $driver_id);
	$stmt     ->bindParam(6,  $driver);
	$stmt     ->bindParam(7,  $userid);
	$stmt     ->bindParam(8,  $parent_id);
	$stmt     ->bindParam(9,  $type);
	$stmt     ->bindParam(10, $tripsheet);
	$stmt     ->bindParam(11, $tripJson);
	$stmt     ->bindParam(12, $distancesJson);
	$stmt     ->bindParam(13, $fuelJson);
	$stmt     ->bindParam(14, $durationsJson);
	$stmt     ->bindParam(15, $targetsJson);
	$stmt     ->bindParam(16, $startdate);
	$stmt     ->bindParam(17, $enddate);
	$stmt     ->bindParam(18, $exceptionsJson);
	$stmt     ->bindParam(19, $axisEventsJson);
	$stmt     ->bindParam(20, $scoresJson);
	$stmt     ->bindParam(21, $transitsJson);
	$stmt     ->bindParam(22, $serviceJson);
	$stmt     ->bindParam(23, $version);
	$stmt     ->bindParam(24, $comment);
	$stmt     ->bindParam(25, $created);
	$stmt     ->bindParam(26, $incidentsJson);
	$stmt     ->bindParam(27, $trailers);
	$stmt     ->bindParam(28, $bonus);
	$stmt     ->bindParam(29, $products);
	$stmt     ->bindParam(30, $rpm_bands);
	$stmt     ->bindParam(31, $fill_ups);

	if ($stmt ->execute()) {
		$_id  = $db_conn->lastInsertId();
		return array('result'=>true,'id'=>$_id,'message'=>'Tripsheet saved. ');
	}
	else{
		$errors = $stmt->errorInfo();
		return array('result'=>false,'message'=>'Tripsheet not saved','error'=>$errors[2]);
	}
}

function create_open_tripsheet($type,$group_id,$group,$asset_id,$asset,$driver_id,$driver_name,$tripsheet,$legsJson,$startdate,$trailersJson,$loadsJson, 
	$dbh, $dbn, $dbu, $dbp ){
	$sql 	  = "INSERT INTO tbltripsheet (groupid,\"group\",assetid,asset,driverid,driver,userid,status,\"type\",tripsheet,trip,\"start\",trailers,
					products,\"created\") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING tripsheetid;";

	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$userid   = $_SESSION['user']['id'];
	$status   = 'OPEN';
	$created  = gmdate("Y-m-d H:i:s"); 

	$stmt     ->bindParam(1,  $group_id);
	$stmt     ->bindParam(2,  $group);
	$stmt     ->bindParam(3,  $asset_id);
	$stmt     ->bindParam(4,  $asset);
	$stmt     ->bindParam(5,  $driver_id);
	$stmt     ->bindParam(6,  $driver_name);
	$stmt     ->bindParam(7,  $userid);
	$stmt     ->bindParam(8,  $status);
	$stmt     ->bindParam(9,  $type);
	$stmt     ->bindParam(10,  $tripsheet);
	$stmt     ->bindParam(11, $legsJson);
	$stmt     ->bindParam(12, $startdate);
	$stmt     ->bindParam(13, $trailersJson);
	$stmt     ->bindParam(14, $loadsJson);
	$stmt     ->bindParam(15, $created);

	if ($stmt ->execute()) {
		$_id  = $db_conn->lastInsertId();
		return array('result'=>true,'id'=>$_id,'message'=>'Tripsheet successfully opened. ');
	}
	else{
		$errors = $stmt->errorInfo();
		return array('result'=>false,'message'=>'Tripsheet could not be opened','error'=>$errors[2]);
	}
}

function update_open_tripsheet($_id, $legsJson, $distancesJson, $durationsJson, $fuelJson, $targetsJson, $startdate, $enddate, 
		$exceptionsJson, $incidentsJson, $transitsJson, $scoresJson, $trailers, $bonus, $products, $dbh, $dbn, $dbu, $dbp ){
	$sql 	  = "UPDATE tbltripsheet SET trip=:trp, distance=:dst, fuel=:fwl, duration=:drn, targets=:trg, 
						\"start\"=:str, \"end\"=:edn, exceptions=:exc, scores=:scr, transits=:trn, incidents=:inc, 
						trailers=:trl, bonus=:bns, products=:prd     
					WHERE tripsheetid=:tid AND status=:sta;";

	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$status   = 'OPEN';

	$stmt     ->bindParam(":tid", $_id);
	$stmt     ->bindParam(":sta", $status);

	$stmt     ->bindParam(":trp", $legsJson);
	$stmt     ->bindParam(":dst", $distancesJson);
	$stmt     ->bindParam(":fwl", $fuelJson);
	$stmt     ->bindParam(":drn", $durationsJson);
	$stmt     ->bindParam(":trg", $targetsJson);
	$stmt     ->bindParam(":str", $startdate);
	$stmt     ->bindParam(":edn", $enddate);
	$stmt     ->bindParam(":exc", $exceptionsJson);
	$stmt     ->bindParam(":scr", $scoresJson);
	$stmt     ->bindParam(":trn", $transitsJson);
	$stmt     ->bindParam(":inc", $incidentsJson);
	$stmt     ->bindParam(":trl", $trailers);
	$stmt     ->bindParam(":bns", $bonus);
	$stmt     ->bindParam(":prd", $products);

	if ($stmt ->execute()) {
		return array('result'=>true,'message'=>'Open Tripsheet successfully updated. ');
	}
	else{
		$errors = $stmt->errorInfo();
		return array('result'=>false,'message'=>'Tripsheet could not be updated','error'=>$errors[2]);
	}
}

function close_tripsheet($ts_id,$drv_id,$driver,$enddate,$distances,$durations,$fuel,$targets,$exceptions,$axisEvents,$transits,$scores,$service,$rpms, 
	$dbh,$dbn,$dbu,$dbp){
	$sql 	  = "UPDATE tbltripsheet SET driverid=:did,driver=:drv,\"end\"=:edn,exceptions=:exc,axisevents=:axs,transits=:trn,scores=:scr,service=:svc,
					distance=:dst, fuel=:fwl, duration=:drn, targets=:trg,rpmbands=:rpm,status=:new WHERE tripsheetid=:tid AND status=:old;";

	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$new   	  = 'ACTIVE';
	$old   	  = 'OPEN';

	$stmt     ->bindParam(":tid", $ts_id);
	$stmt     ->bindParam(":old", $old);
	
	$stmt     ->bindParam(":did", $drv_id);
	$stmt     ->bindParam(":drv", $driver);
	$stmt     ->bindParam(":edn", $enddate);
	$stmt     ->bindParam(":dst", $distances);
	$stmt     ->bindParam(":fwl", $fuel);
	$stmt     ->bindParam(":drn", $durations);
	$stmt     ->bindParam(":trg", $targets);
	$stmt     ->bindParam(":exc", $exceptions);
	$stmt     ->bindParam(":axs", $axisEvents);
	$stmt     ->bindParam(":trn", $transits);
	$stmt     ->bindParam(":scr", $scores);
	$stmt     ->bindParam(":svc", $service);
	$stmt     ->bindParam(":rpm", $rpms);
	$stmt     ->bindParam(":new", $new);

	if ($stmt ->execute()) {
		return array('result'=>true,'message'=>'Open Tripsheet successfully closed. ');
	}
	else{
		$errors = $stmt->errorInfo();
		return array('result'=>false,'message'=>'Tripsheet could not be closed','error'=>$errors[2]);
	}
}

function delete_tripsheet($id, $dbh, $dbn, $dbu, $dbp)
{
	$sql 	  = "UPDATE tbltripsheet SET status='DELETED' WHERE tripsheetid=? ";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $id);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}

function save_fill_up($user_id, $group_id, $driver_id, $asset_id, $pump_id, $leg_id, $location, $qty, 
		$odo, $pump_open, $pump_close, $fill_dt,  $dbh, $dbn, $dbu, $dbp ){

	$sql 	  = "INSERT INTO tbl_fillup (userid, groupid, driverid, assetid, pumpid, legid, location, quantity, odometer, 
						opening, closing, filldatetime, created)  
					VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING fillid;";

	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$created  = gmdate("Y-m-d H:i:s"); 
	$status   = "ACTIVE";

	$stmt     ->bindParam(1,  $user_id);
	$stmt     ->bindParam(2,  $group_id);
	$stmt     ->bindParam(3,  $driver_id);
	$stmt     ->bindParam(4,  $asset_id);
	$stmt     ->bindParam(5,  $pump_id);
	$stmt     ->bindParam(6,  $leg_id);
	$stmt     ->bindParam(7,  $location);
	$stmt     ->bindParam(8,  $qty);
	$stmt     ->bindParam(9,  $odo);
	$stmt     ->bindParam(10, $pump_open);
	$stmt     ->bindParam(11, $pump_close);
	$stmt     ->bindParam(12, $fill_dt);
	$stmt     ->bindParam(13, $created);

	if ($stmt ->execute()) {
		$_id  = $db_conn->lastInsertId();
		return array('result'=>true,'id'=>$_id,'message'=>'Fuel fill-up saved. ');
	}
	else{
		$errors = $stmt->errorInfo();
		return array('result'=>false,'message'=>'Fuel fill-up not saved','error'=>$errors[2]);
	}
}

function get_fill_ups($groupid=0, $start="", $end="", $dbh, $dbn, $dbu, $dbp)
{	
	$params    	= intval($groupid) != 0 ? " AND f.groupid=$groupid " : "";
	$params    .= $start != "" ? " AND f.filldatetime>='$start' " : "";
	$params    .= $end != "" ? " AND f.filldatetime<='$end' " : "";

	$sql       	= "SELECT f.*,
						(SELECT g.name FROM tblgroup g WHERE f.groupid=g.groupid) AS group,   
						(SELECT a.description FROM tblasset a WHERE f.assetid=a.assetid) AS asset,   
						(SELECT d.name FROM tbldriver d WHERE f.driverid=d.driverid) AS driver   
					FROM tbl_fillup f   
					WHERE f.status<>'DELETED' $params  
					ORDER BY filldatetime DESC";

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	return $rows;
}

function delete_fill_up($id, $dbh, $dbn, $dbu, $dbp)
{
	$sql 	  = "UPDATE tbl_fillup SET status='DELETED' WHERE fillid=? ";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $id);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}



function save_driver_review($group_id, $driver_id, $driver, $distance, $fuel, $startdate, $enddate, 
		$oldContract, $newContract, $rank, $spread, $scoring, $violations, $dbh, $dbn, $dbu, $dbp ){

	$sql 	  = "INSERT INTO tbl_driver_review (groupid, driverid, driver, userid, distance, fuel, review_start, review_end, 
						old_contract, new_contract, rank, spread, scoring, violations, created)  
					VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id;";

	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$userid    = $_SESSION['user']['id'];
	$created   = gmdate("Y-m-d H:i:s"); 

	$stmt     ->bindParam(1,  $group_id);
	$stmt     ->bindParam(2,  $driver_id);
	$stmt     ->bindParam(3,  $driver);
	$stmt     ->bindParam(4,  $userid);
	$stmt     ->bindParam(5,  $distance);
	$stmt     ->bindParam(6,  $fuel);
	$stmt     ->bindParam(7,  $startdate);
	$stmt     ->bindParam(8,  $enddate);
	$stmt     ->bindParam(9,  $oldContract);
	$stmt     ->bindParam(10, $newContract);
	$stmt     ->bindParam(11, $rank);
	$stmt     ->bindParam(12, $spread);
	$stmt     ->bindParam(13, $scoring);
	$stmt     ->bindParam(14, $violations);
	$stmt     ->bindParam(15, $created);

	if ($stmt ->execute()) {
		$_id  = $db_conn->lastInsertId();
		return array('result'=>true,'id'=>$_id,'message'=>'Driver review saved. ');
	}
	else{
		$errors = $stmt->errorInfo();
		return array('result'=>false,'message'=>'Driver review not saved','error'=>$errors[2]);
	}
}

function get_driver_review($id, $dbh, $dbn, $dbu, $dbp) {
	$sql       = "SELECT * FROM tbl_driver_review WHERE id=$id ";

	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		return $rows[0];
	}
	return array();
}

function get_driver_reviews($groupid, $dbh, $dbn, $dbu, $dbp)
{	
	$params    	= $groupid!=0 ? " d.groupid=$groupid AND " : "";
	$sql       	= "SELECT d.*, g.name AS group, u.name AS user     
					FROM tbl_driver_review d, tblgroup g, tbl_user u   
					WHERE $params d.userid=u.id AND d.groupid=g.groupid AND d.status<>'DELETED' AND d.review_end > (CURRENT_DATE - INTERVAL '91 days')    
					ORDER BY created DESC";
	$db_conn   = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	$rows      = pg_fetch_all($db_result);

	if ($rows) {
		return $rows;
	}
	return array();
}

function delete_driver_review($id, $dbh, $dbn, $dbu, $dbp)
{
	$sql 	  = "UPDATE tbl_driver_review SET status='DELETED' WHERE id=? ";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);

	$stmt     ->bindParam(1,  $id);

	if($stmt->execute()){
        return array('result'=>true,'rows'=>$stmt->rowCount());
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }
}



function gcp_query($sql, $dbh, $dbn, $dbu, $dbp){
	$database  = new Database();
	$db_conn   = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt      = $db_conn->prepare($sql);

	$stmt->execute();
	
	return $stmt->rowCount();
}

function gcp_multi_query($sql, $dbh, $dbn, $dbu, $dbp){
	$db_conn   = pg_connect("host=$dbh dbname=$dbn user=$dbu password=$dbp");
	$db_result = pg_query($db_conn, $sql);
	
	return pg_affected_rows($db_result);
}

function gcp_select_query($sql, $dbh, $dbn, $dbu, $dbp){
	$db        = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$result    = pg_query($db,$sql);
	$response  = array();
	
	while($row=pg_fetch_assoc($result)){
			$response[] = ['id' => $row['driverid']];
	}
	return $response;
}

function show_consumption($kml, $default_cons){
	if (!$default_cons) {
		if ($kml==0) {
			return number_format($kml, 2);
		}
		return number_format(100/$kml, 2);
	}
	else{
		return number_format($kml, 2);
	}
}

function date_interval_to_seconds($delta){
	return ($delta->s)
         + ($delta->i * 60)
         + ($delta->h * 60 * 60)
         + ($delta->d * 60 * 60 * 24)
         + ($delta->m * 60 * 60 * 24 * 30)
         + ($delta->y * 60 * 60 * 24 * 365);
}

function seconds_to_time_min($seconds){
	$neg   = "";
	if ($seconds<0) {
		$neg 	 = "-";
		$seconds = $seconds*-1;
	}
	$hours = floor($seconds / 3600);
	$mins  = floor($seconds / 60 % 60);
	$secs  = floor($seconds % 60);
	
	// $days  = floor($hours/24);
	// $hours = ($hours%24);

	if($hours<10){ $hours = "0". $hours; }
	if($mins<10){  $mins  = "0". $mins;   }
	if($secs<10){  $secs  = "0". $secs;   }

	// if($days>0){
	//  	return $neg . $days ."d, $hours:$mins:$secs";
	// }
		
	return $neg ."$hours:$mins:$secs";
}

function seconds_to_time($seconds){
	$hours = floor($seconds / 3600);
	$mins  = floor($seconds / 60 % 60);
	$secs  = floor($seconds % 60);
	
	$days  = floor($hours/24);
	$hours = ($hours%24);

	if($hours<10){ $hours = "0". $hours; }
	if($mins<10){  $mins  = "0". $mins;   }
	if($secs<10){  $secs  = "0". $secs;   }

	return $days ."d, $hours:$mins:$secs";
}

function to_seconds($dt) { 
	return  ($dt->y * 365 * 24 * 60 * 60) + 
			($dt->m * 30 * 24 * 60 * 60) + 
			($dt->d * 24 * 60 * 60) + 
			($dt->h * 60 * 60) + 
			($dt->i * 60) + 
			$dt->s; 
}

function filter_array($group_id, $data_array){
	if(is_array($data_array)){
		foreach ( $data_array as $entity => $info ) {
			if ($info['groupid'] != $group_id) {
				unset($data_array[$entity]);
			}
		}
		return $data_array;
	}	
}

function search_array($array, $key, $value){
    $results = array();

    if (is_array($array)) {
        if (isset($array[$key]) && $array[$key] == $value) {
            $results[] = $array;
        }
        foreach ($array as $subarray) {
            $results = array_merge($results, search_array($subarray, $key, $value));
        }
    }

    return $results;
}

function array_value_exists($array, $val) {
    foreach ($array as $item){
		if (isset($item['description']) && $item['description'] == $val){
           	return true;
		}
    }
    return false;
}

function get_event_key($array, $val) {
    foreach ($array as $item)
        if (isset($item['description']) && $item['description'] == $val)
            return $item['name'];
    return false;
}

function driver_exists_in_array($array, $drivername){
    if (is_array($array)) {
		foreach($array as $driver){
			if($driver['driver_name'] == $drivername)
				return true;
		}
    }

    return false;
}

function event_exists_in_array($array, $drivername, $event){
    if (is_array($array)) {
		$key  = strtolower(str_replace(" ","_", $drivername));
		if(isset($array[$key])){
			if($array[$key]['driver_name'] == $drivername){
				foreach($array[$key]['violations'] as $e){
					// print_r($e);
					// echo $e['event'] ."<br>";
					if($e['event'] == $event)
						return true;
				}
			}
		}
    }

    return false;
}

function fetch_fm_token($token_url, $mix_user, $mix_pwd, $mix_scope, $mix_cid, $mix_csec){
	$headers        = ["Content-Type"=>"application/x-www-form-urlencoded"];
	$post_data 		= array(
		'grant_type'    => "password",
		'username'      => $mix_user,
		'password'      => $mix_pwd,
		'scope'         => $mix_scope,
		'client_id'     => $mix_cid,
		'client_secret' => $mix_csec
	);
	$post_params    = http_build_query($post_data, null, '&');
	$token_response = json_decode(fetchURL($token_url, $post_params, $headers), true);

	return isset($token_response["access_token"]) ? $token_response["access_token"] : "bad-token";
}

function fetch_fm_driver_scores($token, $api_base_url, $driver_id, $start, $end){
	$start 	  	  = str_replace(":","", $start);
	$start 	  	  = str_replace("-","", $start);
	$start 	  	  = str_replace(" ","", $start);
	$end 	  	  = str_replace(":","", $end);
	$end 	  	  = str_replace("-","", $end);
	$end 	  	  = str_replace(" ","", $end);

	$score_url    = "/api/trips/driverscore/standard/from/$start/to/$end";
	$post_data    = '['.$driver_id.']';
	$rest_client  = new RestClient([
			'base_url'=>$api_base_url, 
			'headers' =>['Authorization' => 'Bearer '.$token], 
		]);
	$response  = $rest_client->post(
			$score_url, $post_data, array('Content-Type' => 'application/json')
		);
	
	return $response;
}

function fetch_fm_service_info($token, $api_base_url, $group_id, $vehicle_id){
	$serviceinfo  = array("next_odo"=>0,"last"=>"","next"=>"");
	$reminder_url = "/api/reminders/organisation/$group_id/asset/$vehicle_id";
	$rest_client  = new RestClient([
			'base_url'=>$api_base_url, 
			'headers' =>['Authorization' => 'Bearer '.$token], 
		]);
	$response  = json_decode($rest_client->get($reminder_url)->response);

	$serviceinfo["next_odo"]  = isset($response->ServiceReminder->NextServiceDueAtKm) ? $response->ServiceReminder->NextServiceDueAtKm : 0;
	$serviceinfo["last"] 	  = isset($response->ServiceReminder->LastServiceRecordedAt) ? $response->ServiceReminder->LastServiceRecordedAt : "";
	$serviceinfo["next"] 	  = isset($response->ServiceReminder->NextServiceDueAt) ? $response->ServiceReminder->NextServiceDueAt : "";

	return $serviceinfo;
}

function get_fm_service_info($group_id, $asset_id, $end_date, $dbh, $dbn, $dbu, $dbp ){
	$sql 	   = "SELECT lastservicerecordedkm,lastservicerecordedat, nextservicedueatkm, nextservicedueat FROM tblservicereminder 
					WHERE groupid=$group_id AND assetid=$asset_id AND DATE(lastservicerecordedat)<=DATE('$end_date') LIMIT 1 ";
	$db        = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$result    = pg_query($db,$sql);
	$row 	   = pg_fetch_assoc($result);
	$svcInfo   = array();

	if ($row) {
		$svcInfo["last_odo"] = $row['lastservicerecordedkm'];
		$svcInfo["next_odo"] = $row['nextservicedueatkm'];
		$svcInfo["last"]	 = $row['lastservicerecordedat'];
		$svcInfo["next"]	 = $row['nextservicedueat'];
	}

	return $svcInfo;
}

function get_fm_transits($group_id, $asset_id, $driver_id, $start, $end, $dbh, $dbn, $dbu, $dbp ){
	$params    = intval($group_id)!=0 ?  " AND p.groupid=$group_id " : "";
	$params   .= intval($asset_id)!=0 ?  " AND p.assetid=$asset_id " : "";
	$params   .= intval($driver_id)!=0 ? " AND p.driverid=$driver_id " : "";

	$sql 	   = "SELECT p.groupid, g.name AS grp, p.driverid, d.name AS driver, p.assetid, a.description AS asset, position_a,position_b, 
						transit_start, transit_end, start_odo, end_odo, target_distance, target_duration   
					FROM tbl_transits_processor p, tbldriver d, tblasset a, tblgroup g       
					WHERE p.groupid=g.groupid AND p.driverid=d.driverid AND p.assetid=a.assetid AND transit_end>transit_start AND transit_start>='$start' AND transit_end<='$end' $params  
					ORDER BY transit_start, transit_end";
	$db        = pg_connect("host=$dbh port=5432 dbname=$dbn user=$dbu password=$dbp");
	$result    = pg_query($db,$sql);
	$rows 	   = pg_fetch_all($result);

	$transits  = array();
	if (is_array($rows)) {
		foreach ($rows as $row) {
			$r 	   = array(
				'group' 	=> $row['grp'],
				'groupid' 	=> $row['groupid'],
				'driver' 	=> $row['driver'],
				'driverid' 	=> $row['driverid'],
				'asset' 	=> $row['asset'],
				'assetid' 	=> $row['assetid'],
				'start_pos' => $row['position_a'],
				'end_pos' 	=> $row['position_b'],
				'start_dt' 	=> $row['transit_start'],
				'end_dt' 	=> $row['transit_end'],
				'start_odo' => $row['start_odo'],
				'end_odo' 	=> $row['end_odo'],
				'target_distance' 	=> $row['target_distance'],
				'target_duration' 	=> $row['target_duration']
			);
			$transits[] = $r;
		}
	}

	return $transits;
}

function utf8_encode_array($array){
    foreach($array as $key => $value)
    {
        if(is_array($value))
        {
            $array[$key] = utf8_encode_array($value);
        }
        else
        {
            $array[$key] = mb_convert_encoding($value, 'Windows-1252', 'UTF-8');
        }
    }

    return $array;
}

function dateDiffInDays($date1, $date2)  { 
    // Calulating the difference in timestamps 
    $diff = strtotime($date2) - strtotime($date1); 
      
    // 1 day = 24 hours 
    // 24 * 60 * 60 = 86400 seconds 
    return abs(round($diff / 86400)); 
} 

function fetchURL($url, $post_body = null,$headers = array()) {
	// OK cool - then let's create a new cURL resource handle
	$ch = curl_init();

	// Determine whether this is a GET or POST
	if ($post_body != null) {
		// curl_setopt($ch, CURLOPT_POST, 1);
		// Alows to keep the POST method even after redirect
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_body);

		// Default content type is form encoded
		$content_type = 'application/x-www-form-urlencoded';

		// Determine if this is a JSON payload and add the appropriate content type
		if (is_object(json_decode($post_body))) {
			$content_type = 'application/json';
		}

		// Add POST-specific headers
		$headers[] = "Content-Type: {$content_type}";
		$headers[] = 'Content-Length: ' . strlen($post_body);

	}

	// If we set some heaers include them
	if(count($headers) > 0) {
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	}

	// Set URL to download
	curl_setopt($ch, CURLOPT_URL, $url);

	// if (isset($this->httpProxy)) {
	//     curl_setopt($ch, CURLOPT_PROXY, $this->httpProxy);
	// }

	// Include header in result? (0 = yes, 1 = no)
	curl_setopt($ch, CURLOPT_HEADER, 0);

	// Allows to follow redirect
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

	/**
	 * Set cert
	 * Otherwise ignore SSL peer verification
	 */
	// if (isset($this->certPath)) {
	//     curl_setopt($ch, CURLOPT_CAINFO, $this->certPath);
	// }

	// if($this->verifyHost) {
	//     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	// } else {
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	// }

	// if($this->verifyPeer) {
	//     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	// } else {
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	// }

	// Should cURL return or print out the data? (true = return, false = print)
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	// Timeout in seconds
	curl_setopt($ch, CURLOPT_TIMEOUT, 500);

	// Download the given URL, and return output
	$output = curl_exec($ch);

	// HTTP Response code from server may be required from subclass
	$info = curl_getinfo($ch);
	// $this->responseCode = $info['http_code'];

	if ($output === false) {
		throw new Exception('Curl error: ' . curl_error($ch));
	}

	// Close the cURL resource, and free system resources
	curl_close($ch);

	return $output;
}


/// PRINTING FUNCTIONS
function get_logo($company){
	if (stristr(strtolower($company), "sereth")) {
		return "../img/logo/sereth.png";
	}
	elseif (stristr(strtolower($company), "dharwizi")) {
		return "../img/logo/dharwizi.png";
	}
	elseif (stristr(strtolower($company), "total")) {
		return "../img/logo/total.png";
	}
	elseif (stristr(strtolower($company), "bluestar")) {
		return "../img/logo/bluestar.png";
	}
	elseif (stristr(strtolower($company), "cold chain")) {
		return "../img/logo/cold-chain.png";
	}
	elseif (stristr(strtolower($company), "proton")) {
		return "../img/logo/proton.png";
	}
	elseif (stristr(strtolower($company), "maxillaria")) {
		return "../img/logo/maxillaria.png";
	}
	elseif (stristr(strtolower($company), "sg africa")) {
		return "../img/logo/supergroup.png";
	}
	elseif (stristr(strtolower($company), "sabot")) {
		return "../img/logo/sabot.png";
	}
	elseif (stristr(strtolower($company), "leopack")) {
		return "../img/logo/leopack.png";
	}
	elseif (stristr(strtolower($company), "redan")) {
		return "../img/logo/redan.png";
	}
	elseif (stristr(strtolower($company), "amoza")) {
		return "../img/logo/amozalogix.png";
	}
	elseif (stristr(strtolower($company), "lesiza")) {
		return "../img/logo/lesiza.png";
	}
	elseif (stristr(strtolower($company), "concrete")) {
		return "../img/logo/cc.png";
	}
	elseif (stristr(strtolower($company), "zambulk")) {
		return "../img/logo/zambulk.png";
	}
	elseif (stristr(strtolower($company), "tac")) {
		return "../img/logo/tac.png";
	}

	return "../img/logo/blank.png";
}

function get_print_trip_info($type, $targets, $distances, $fuel, $durations, $configs, $group_id){
	$dynamicDist 	= 0;
	$dynamicFuel 	= 0; 
	$dynamicDurn 	= 0;

	$defaultCons 	= true;
    $showTargets 	= true;
    $showProbeFuel  = false;
    $showGt      	= true;
    $shuntFuel      = true;
    foreach ($configs AS $cnf) {
        if ($cnf['id']==$group_id) {
            $defaultCons 	= $cnf['consumption']=='km/l' ? true : false;
            $showTargets 	= $cnf['showtargets'];
            $showProbeFuel 	= $cnf['probefuel'];
            $showGt      	= $cnf['showgt'];
            $shuntFuel      = $cnf['shuntFuel'];
        }
    }
	
    $dynamicDist = 0; 
    $distTarget  = 0;
    $edit 		 = "";
    if ($type=="GT") {
    	$dynamicDurn = $durations['gt'];
        $dynamicFuel = $fuel['gt'];
        $distTarget  = $targets['distance'];
        $dynamicDist = ($distances['gt_edit']!=$distances['gt'] && $distances['gt_edit']>0) ? $distances['gt_edit'] : $distances['gt'];
    }
    elseif ($type=="FM") {
    	$dynamicDurn = $durations['fm'];
    	$dynamicFuel = $fuel['fm']; 
        $dynamicDist = ($distances['fm_edit']!=$distances['fm'] && $distances['fm_edit']>0) ? $distances['fm_edit'] : $distances['fm'];
        if ($group_id==-3273034392599683215 && $distances['fm']!=$distances['fm_edit'] && $distances['fm_edit']>0) { 
            $distTarget = $dynamicDist;
        }
        else {
            $distTarget = $targets['distance'];
        }
        if($distances['fm_edit']>0 && $distances['fm_edit']!=$distances['fm']){ $edit = "* "; }
    }
    $distVar 	 = $showTargets ? $dynamicDist-$distTarget : $distances['actual']-$dynamicDist;
    $dynCons 	 = ($dynamicFuel>0) ? $dynamicDist/$dynamicFuel : 0;
    $consumption = ($fuel['actual']>0) ? $distances['actual']/$fuel['actual'] : 0;

	$infoData[]  = array(
	    'Distance (Km)',
	    number_format($distances['actual'],1),
	    number_format($distTarget,1),
	    $edit . number_format($dynamicDist,1),
	    $edit . number_format($dynamicDist,1),
	    $edit . number_format($distVar,1),
	    number_format($distances['gt'],1)
	);

	$shunt 		 = $dynamicDist>$targets['distance'] ? $dynamicDist-$targets['distance'] : 0;
	if($group_id==5432009351795875573){ $shunt = 0; }
	
	$fuelTarget  = $targets['fuel'] + ($shuntFuel ? (0.5*$shunt) : 0);
	if($group_id==-3273034392599683215){
		$fuelTarget  = (($targets['consumption']>0) ? $dynamicDist/$targets['consumption'] : 0);
	} 
	elseif($group_id==-5511162494231442644){
		$fuelTarget  = $targets['fuel'] + ($shuntFuel ? ($shunt/0.5) : 0);
	}

	$fuelVar 	 = 0;
	$pfFuel  	 = $fuel['actual'];;
	if ($showProbeFuel && isset($fuel['probe']) && $fuel['probe']>0) {
		$pfFuel  = $fuel['probe'];
        $fuelVar = number_format($fuel['probe']-$fuel['actual'],1);
    }
    elseif($group_id==-5511162494231442644){
        $fuelVar = number_format($fuelTarget-$fuel['actual'],1);
    } 
    elseif ($group_id==7047838304188674589) {
        $fuelVar = number_format($fuel['fm']-$fuelTarget,1);
    }
    else{
        $fuelVar = $showTargets ? number_format($fuelTarget-$fuel['actual'],1) : number_format($fuel['fm']-$fuel['actual'],1);
    }
    
    $infoData[]  = array(
	    'Fuel (L)',
	    number_format($fuel['actual'],1),
	    number_format($fuelTarget,1),
	    number_format($dynamicFuel,1),
	    number_format($pfFuel,1), // Here
	    number_format($fuelVar,1),
	    number_format($fuel['actual'],1)
	);
	$infoData[] = array(
	    'Cons. ('. ($defaultCons ? "Km/L" : "L/100km") .')',
	    show_consumption($consumption,$defaultCons),
	    show_consumption($targets['consumption'],$defaultCons),
	    show_consumption((($dynamicFuel>0) ? $dynamicDist/$dynamicFuel : 0),$defaultCons),
	    show_consumption(($pfFuel>0 ? $dynamicDist/$pfFuel : 0),$defaultCons),
	    "", //show_consumption($targets['consumption']-$consumption,$defaultCons),
	    show_consumption((($fuel['actual']>0) ? $distances['gt']/($fuel['actual']) : 0),$defaultCons)
	);
	$infoData[] = array(
	    'Duration',
	    seconds_to_time_min($durations['actual']),
	    seconds_to_time_min($targets['duration']),
	    seconds_to_time_min($dynamicDurn),
	    "", //seconds_to_time_min($dynamicDurn),
	    seconds_to_time_min($targets['duration']-$durations['actual']),
	    "" //seconds_to_time_min($durations['gt'])
	);

	return $infoData;
}

function update_approval_status($ts_id, $user_id, $dbh, $dbn, $dbu, $dbp){

	$created  = Date('Y-M-d H:i:s');
	$user = $_SESSION['user']['name'];
	$sql 	  = "UPDATE tbltripsheet SET approved = TRUE, verificationdate='$created', verificationuser='$user', verification_userid='$user_id' WHERE tripsheetid=:tid RETURNING tripsheetid AS id;";
	$database = new Database();
	$db_conn  = $database->getConnection($dbh, $dbn, $dbu, $dbp);
	$stmt     = $db_conn->prepare($sql);
	$stmt     ->bindParam(":tid", $ts_id);
	
	if($stmt->execute()){
		$_id  = $stmt->fetchAll();
		$_id = $_id[0]['id'];
        return array('result'=>true,'id'=>$_id,'message'=>'This tripsheet has been approved');
    }
    else {
        $errors = $stmt->errorInfo();
        return array('result'=>false,'error'=>$errors[2]);
    }

}

?>